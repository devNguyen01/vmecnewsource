
<footer id="footer" class="clearfix">
    <section class="footer-body">
        <div class="row row15">
            <?php
            if(!empty($contactInfo)){
                foreach ($contactInfo as $key => $value) {
                    ?>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                         <h4 class="title-foot arr_sq">
                             <?= $value->company_contact_position;?>
                        </h4>
                        <address>
                            <?= dia_chi; ?>: <?= $value->company_contact_address;?><br>
                            <?= dien_thoai; ?>: <?= $value->company_contact_phone;?>
                        </address>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </section>
    <section class="footer-bottom font-2">
        <ul class="list-inline">
            <li><a href="http://www.mitsubishielectric.com/copyright/terms.html"><?= dieu_khoan_su_dung?> <img src="<?= base_img?>open-blank.png" alt="icon_blank" class="icon_blank"></a></li>
                    <li><a href="http://www.mitsubishielectric.com/copyright/privacy.html"><?= chinh_sach_bao_mat?> <img src="<?= base_img?>open-blank.png" alt="icon_blank" class="icon_blank"></a></li>
            <?php
            if(!empty($menuFooter))
            {
                foreach ($menuFooter as $key => $value) {
                    $link = base_url().$value['menu_alias'].'.html';
                    echo '<li><a href="'.$link.'">'.$value["menu_name"].'</a></li>';
                }
            }
            ?>
        </ul>
        <p class="copyright">
            Copyright © 2015 Mitsubishi Elevator Vietnam Co., Ltd. All rights reserved.
        </p>
    </section>
</footer>
</div>
<aside class="visible-lg aside_menu scrollspy">
    <div id="nav_menu" data-spy="affix">
        <div class="icon-nav_menu">
            <div id="arrow-sidebar">
                <i class="fa fa-angle-double-up"></i>
            </div>
            <span id="number_elevator">G</span>
        </div>
        <ul class="list-unstyled row row4 nav" >
            <li class="col-xs-12"><a id="" title="" href="javascript:void(0);" data-container="body" data-toggle="popover" data-placement="left" data-content="<img src='<?= base_img?>popover-bell.jpg' alt=''>"data-original-title=""><i class="fa fa-bell-o"></i></a></li>
            <?php
            if(!empty($menuMain))
            {
                $count = count($menuMain);
                $count = $count > 0 ? ($count-1):0;
                foreach ($menuMain as $key => $value) {
                    $number = $count;
                    $name = $count == 0 ? 'G':$count;
                    $link = $menuInfo['menu_com'] == 'home' ? '#'.$menuMain[$number]['menu_hdid']:base_url().'#'.$menuMain[$number]['menu_hdid'];
                    echo '<li class="col-xs-6"><a title="'.$menuMain[$number]["menu_name"].'" class="page-scroll" href="'.$link.'">'.$name.'</a></li>';
                    $count--;
                }
            }
            ?>
        </ul>
    </div>
</aside>
</div>
<div id="topcontrol"><a href="javascript:void(0)" ><img src="<?= base_img?>skins/page-top.png" alt="page-top"></a></div>
<script src="<?= base_js?>jquery.easing.min.js"></script>
<script src="<?= base_js?>main.js"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script src="https://apis.google.com/js/platform.js" ></script>
<script type="text/javascript">
$(document).ready(function(){
    // nav-sidebar
    $('#nav_menu, #nav_service').affix({
        offset: {
            top: $('.main_content').offset().top,
        }
    });
})
</script>

<!-- Chen code GA Mr.Tuyen-->
 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52251421-1', 'www.vmec.vn');
  ga('send', 'pageview');

</script> 
<a href="https://plus.google.com/112813861292456747582" rel="publisher"></a> 
	<!---End--->
	
</body>
</html>