<header id="header" class=" navbar-fixed-top">
    <section class="header-body">
        <div class="container" id="width-fixed">
            <a href="<?= base_url()?>" class="pull-left logo">
                <img src="<?= base_img?>logo.png" alt="Mitsubishi Elevator Vietnam Co., Ltd." title="Mitsubishi Elevator Vietnam Co., Ltd.">
            </a>
            <div class="pull-right">
                <div class="subnav-search">
                    <ul class="list-inline subnav">
                        <li><a target="_blank" href="<?= link_website_toan_cau?>"><?= trang_web_toan_cau?> <img src="<?= base_img?>open-blank.png" alt="icon_blank" class="icon_blank"></a></li>
                        <li><a target="_blank" href="<?= link_website_thai_binh_duong?>"><?= cong_chau_a_thai_binh_duong?> <img src="<?= base_img?>open-blank.png" alt="icon_blank" class="icon_blank"></a></li>
                    </ul>
                     <div class="hidden-xs form-group has-feedback search">
                        <form action="<?= base_url?>tim-kiem/" method="get">
                        <label class="control-label sr-only" for="inputSuccess4">Input with success</label>
                        <input type="text" class="form-control btn-flat input-small check" placeholder="<?= tim_kiem?>" name="q" required="required">
                        <button class="buttonsearch" type="submit"><i class="fa fa-search"></i></button>
                        <span id="inputSuccess4Status" class="sr-only">(success)</span>
                        </form>
                    </div>
                </div>
                <ul class="list-inline lang">
                    <li <?= $lang=="vn" ? 'class="active"':'';?>><a class="lang_vn arr" href="<?= base_url().'language/?redirect='.base64_encode(current_url())?>&amp;lang=vn&amp;com=<?= $hd_cate?>" title="Tiếng Việt"> Tiếng Việt</a></li>
                    <li <?= $lang=="en" ? 'class="active"':'';?>><a class="lang_en arr" href="<?= base_url().'language/?redirect='.base64_encode(current_url())?>&amp;lang=en&amp;com=<?= $hd_cate?>" title="English"> English</a></li>
                </ul>
            </div>
        </div>
    </section>
    <nav id="navigation" class="navbar navbar-default navbar-custom" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand visible-xs" href="<?= base_url();?>"><img src="<?= base_img?>logo-mini.png" alt="logo"></a>
                <!-- <div class="visible-xs form-group has-feedback search">
                    <form action="<?= base_url?>tim-kiem/" method="get">
                    <label class="control-label sr-only" for="inputSuccess4">Input with success</label>
                    <input type="text" class="form-control btn-flat input-small" placeholder="<?= tim_kiem?>" name="q" required="required">
                    <button class="buttonsearch" type="submit"><i class="fa fa-search"></i></button>
                    <span id="inputSuccess4Status" class="sr-only">(success)</span>
                    </form>
                </div> -->
                <div class="visible-xs search">
                    <i class="fa fa-times"></i>
                    <i class="fa fa-search"></i>
                </div>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav" id="menu">
                    <?php
                    $getListIDActive = $this->mmenu->getAllParent($menuInfo['id']);
                    if(!empty($menuMain))
                    {
                        foreach ($menuMain as $key => $value) {
                            $link = $menuInfo['menu_com'] == 'home' ? '#'.$value['menu_hdid']:base_url().'#'.$value['menu_hdid'];
                            $active = '';
                            if(in_array($value['id'], $getListIDActive) && $this->uri->segment(1)!=''){
                                $active = 'text-red';
                            }
                            echo '<li data-menuanchor="'.$value['menu_hdid'].'" ><a class="page-scroll '.$active.'" href="'.$link.'">'.$value['menu_name'].'</a></li>';
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>
</header>