<?php
if(!empty($menuChild))
{
    foreach ($menuChild as $key => $value) {
        $link = base_url().$value['menu_alias'].'.html';
        $menuChildC1 = $this->mmenu->getMenu($value['id'],$lang);
        ?>

<h3 class="title"><a href="<?= $link?>"><?= $value['menu_name'];?></a></h3>
<ul class="list-unstyled">
    <?php
    if(!empty($menuChildC1))
    {
        foreach ($menuChildC1 as $keyC1 => $valueC1) {
            $linkc2 = base_url().$valueC1['menu_alias'].'.html';
            $menuChildC2 = $this->mmenu->getMenu($valueC1['id'],$lang);
            $linkc2 = !empty($menuChildC2) ? '':'href="'.$linkc2.'"';
            echo '<li class="active"><a class="arr_sqg" '.$linkc2.'>'.strip_tags($valueC1['menu_name']).'</a>';
            if(!empty($menuChildC2))
            {
                echo '<ul class="list-unstyled">';
                foreach ($menuChildC2 as $keyC2 => $valueC2) {
                    $linkC2 = base_url().$valueC2['menu_alias'].'.html';
                    $active = $menuInfo['id'] == $valueC2['id'] ? 'style="color: #e60000 !important;"':'';
                    echo ' <li ><a '.$active.' href="'.$linkC2.'" class="arr">'.strip_tags($valueC2['menu_name']).'</a></li>';
                }
                echo '</ul>';
            }
            echo '</li>';
        }
    }
    ?>
</ul>
<?php
    }
}
?>