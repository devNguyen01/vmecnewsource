<?php
if(!empty($menuLeft))
{
    $i=1;
    $getListIDActive = $this->mmenu->getAllParent($menuInfo['id']);
    foreach ($menuLeft as $key => $value) {
        $link = base_url().$value['menu_alias'].'.html';
        $active = $value['menu_alias']==str_replace('.html','', $this->uri->segment(1)) ?'active':'';
        if(in_array($value['id'], $getListIDActive)){
            $active = 'active';
        }
        echo '<li class="'.$active.' list_menu-'.$i++.'"><a href="'.$link.'" title="'.strip_tags($value['menu_name']).'">'.$value["menu_name"].'</a></li>';
    }
}
?>