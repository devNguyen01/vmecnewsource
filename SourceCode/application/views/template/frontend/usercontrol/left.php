<aside class="sidebar">
    <!-- InstanceBeginEditable name="sidebar" -->
    <div class="wrapper-box_question">
        <div class="box_question">
            <h3 class="title">
                <a href="<?= base_url().'hoi-dap.html'?>#form-faq"><?= hoi_dap?></a>
            </h3>
            <p><?= thac_mac_hoi_dap?></p>
            <a href="<?= base_url().'hoi-dap.html'?>#form-faq" class="btn btn-xs btn-default"><span class="arrc"></span> <?= gui_cau_hoi?></a>
        </div>
        <!--END: box_question-->
    </div>
    <div class="submenu <?= !empty($menuInfo) && $menuInfo['menu_view'] == 'product' ? 'submenu2':''; ?>">
        <?php
        if($menuInfo['menu_view']=='product')
        {
            $this->load->view("template/frontend/usercontrol/left_product",$menuChild);
        }
        elseif($menubox5 == 1){
            echo $menuSpecial;
        }else{
            echo '<h3 class="title"><a class="bold" href="'.base_url().'#'.$hd_cate.'">'.strip_tags($title_cate).'</a></h3>';
            echo '<ul class="list-unstyled">';
                if(!empty($menuChild))
                {
                    $getListIDActive = $this->mmenu->getAllParent($menuInfo['id']);
                    foreach ($menuChild as $keyC1 => $valueC1) {
                        $link = base_url().$valueC1['menu_alias'].'.html';
                        $menuChildC1 = $this->mmenu->getMenu($valueC1['id'],$lang);
                        $active = '';
                        if(in_array($valueC1['id'], $getListIDActive) || $menuInfo['menu_alias']==$valueC1['menu_alias']  || (isset($info->news_parent) && $info->news_parent==$valueC1['id'])){
                            $active = 'active';
                        }
                        echo '<li class="'.$active.'"><a class="arr_sqg" href="'.$link.'">'.strip_tags($valueC1['menu_name']).'</a>';
                        if(!empty($menuChildC1) && $valueC1['menu_view'] != 'kinhnghiem' && $valueC1['id'] != 22)
                        {
                            echo '<ul class="list-unstyled">';
                            foreach ($menuChildC1 as $keyC2 => $valueC2) {
                                $linkC2 = base_url().$valueC2['menu_alias'].'.html';
                                if($valueC2['id']==95)
                                {
                                    $linkC2 = '#';
                                }
                                if(in_array($valueC2['id'], $getListIDActive) || in_array($menuInfo['id'], $getListIDActive) || $menuInfo['menu_alias']==$valueC2['menu_alias']){
                                    $active = 'active';
                                }
                                echo ' <li class="'.$active.'"><a href="'.$linkC2.'" class="arr">'.strip_tags($valueC2['menu_name']).'</a></li>';
                            }
                            echo '</ul>';
                        }
                        echo '</li>';
                    }
                }
            echo '</ul>';
        }
        ?>
    </div>
    <!--END: submenu-->
    <?php
    if(!empty($banner_left_page))
    {
        foreach ($banner_left_page as $key => $value) {
            $picture = base_file.'banner/'.$value->banner_picture;
            $link = !empty($value->banner_link) ? 'target="_blank" download href="'.$value->banner_link.'"':'';
            ?>
            <div class="slide-banner">
                <a <?= $link?> title="<?= $value->banner_title?>" class="image">
                    <img src="<?= $picture?>" alt="<?= $value->banner_title?>">
                </a>
                <?php if(!empty($link)) { ?><p><a <?= $link?>><img src="<?= base_img?>sub-page/pdf.png" alt="<?= nhan_tai_xuong?>"><?= nhan_tai_xuong?></a></p><?php } ?>
            </div>
            <?php
        }
    }
    ?>
    <?php
    if(!empty($banner_left))
    {
        foreach ($banner_left as $key => $value) {
            $picture = base_file.'banner/'.$value->banner_picture;
            $link = !empty($value->banner_link) ? 'target="_blank" href="'.$value->banner_link.'"':'';
            ?>
            <div class="slide-banner">
                <a <?= $link?> title="<?= $value->banner_title?>" class="image">
                    <img src="<?= $picture?>" alt="<?= $value->banner_title?>">
                </a>
            </div>
            <?php
        }
    }
    ?>
</aside>