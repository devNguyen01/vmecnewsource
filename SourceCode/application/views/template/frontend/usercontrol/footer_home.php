<footer id="footer">
    <?php if(!empty($mobi) && $mobi=='mobi'){ ?>
    <section class="footer-body">
        <div class="row row15">
            <?php
            if(!empty($contactInfo)){
                foreach ($contactInfo as $key => $value) {
            ?>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                     <h4 class="title-foot arr_sq">
                         <?= $value->company_contact_position;?>
                    </h4>
                    <address>
                        <?= dia_chi; ?>: <?= $value->company_contact_address;?><br>
                        <?= dien_thoai; ?>: <?= $value->company_contact_phone;?>
                    </address>
                </div>
            <?php
                }
            }
            ?>
        </div>
    </section>
    <?php }else{ ?>
    <section class="footer-body order_home">
        <div class="row row15">
            <?php
            if(!empty($contactInfo)){
                foreach ($contactInfo as $key => $value) {
            ?>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                     <h4 class="title-foot arr_sq">
                         <?= $value->company_contact_position;?>
                    </h4>
                    <address>
                        <?= dia_chi; ?>: <?= $value->company_contact_address;?><br>
                        <?= dien_thoai; ?>: <?= $value->company_contact_phone;?>
                    </address>
                </div>
            <?php
                }
            }
            ?>
        </div>
    </section>
    <section class="footer-body it_home">
        <div class="s_box s_banner">
            <div class="row row10">
                <?php
                if(!empty($banner_home))
                {
                    foreach ($banner_home as $key => $value) {
                        $picture = base_file.'banner/'.$value->banner_picture;
                        $link = !empty($value->banner_link) ? 'href="'.$value->banner_link.'"':'';
                        ?>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <a <?= $link?> class="image imghome"><img src="<?= $picture?>" alt="<?= $value->banner_title?>"></a>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
       </div>
    </section>
       <!--END: .s_banner-->
       <?php } ?>
    
    <section class="footer-bottom font-2">
        <ul class="list-inline">
        <li><a href="http://www.mitsubishielectric.com/copyright/terms.html"><?= dieu_khoan_su_dung?> <img src="<?= base_img?>open-blank.png" alt="icon_blank" class="icon_blank"></a></li>
                    <li><a href="http://www.mitsubishielectric.com/copyright/privacy.html"><?= chinh_sach_bao_mat?> <img src="<?= base_img?>open-blank.png" alt="icon_blank" class="icon_blank"></a></li>
        <?php
        if(!empty($menuFooter))
        {
            foreach ($menuFooter as $key => $value) {
                $link = base_url().$value['menu_alias'].'.html';
                echo '<li><a href="'.$link.'" title="'.$value["menu_name"].'" >'.$value["menu_name"].'</a></li>';
            }
        }
        ?>
        </ul>
        <br>
        <p class="copyright">
            Copyright © 2015 Mitsubishi Elevator Vietnam Co., Ltd. All rights reserved.
        </p>
    </section>
</footer>
<!--END: Footer-->
<div class="sidebar-fixed visible-lg" id="sidebar-fixed">
    <aside class="aside_service hidden-xs hidden-sm" id="aside_service">
        <ul id="nav_service" class="list-unstyled">
            <?php $this->load->view("template/frontend/usercontrol/menu_left");?>
        </ul>
    </aside>
    <!--END: aside_service-->
    <aside class="aside_menu hidden-xs hidden-sm " id="aside_menu">
       <div id="nav_menu">
         <div class="icon-nav_menu" >
             <div id="arrow-sidebar" >
                 <i class="fa fa-angle-double-up"></i>
             </div>
            <span id="number_elevator">G</span>
         </div>
         <!--END: icon-nav_menu-->
         <ul class="list-unstyled row row4 nav" id="number-scroll" >
            <li class="col-xs-12">
                <a href="javascript:void(0);" data-container="body" data-toggle="popover" data-placement="left" data-content="<img src='<?= base_img?>popover-bell.jpg' alt=''>"data-original-title="">
                    <i class="fa fa-bell-o"></i>
                </a>
            </li>
            <?php
            if(!empty($menuMain))
            {
                $count = count($menuMain);
                $count = $count > 0 ? ($count-1):0;
                foreach ($menuMain as $key => $value) {
                    $number = $count;
                    $name = $count == 0 ? 'G':$count;
                    $link = $menuInfo['menu_com'] == 'home' ? '#'.$menuMain[$number]['menu_hdid']:base_url().'#'.$menuMain[$number]['menu_hdid'];
                    echo '<li class="col-xs-6"><a title="'.$menuMain[$number]["menu_name"].'" class="page-scroll" href="'.$link.'">'.$name.'</a></li>';
                    $count--;
                }
            }
            ?>
        </ul>
    </div>
   </aside>
</div>
<!--END: sidebar-fixed-->
<div class="main_content main_content-body"></div>
<div id="topcontrol"><a href="javascript:void(0)"><img src="<?= base_img?>skins/page-top.png" alt="page-top"></a></div>
<script src="<?= base_js?>jquery.easing.min.js"></script>
<!--Homeycombs-->
<script src="<?= base_js?>jquery.homeycombs.js"></script>
<!--random-background-->
<script src="<?= base_js?>random-background.js"></script>
<!--Javascript Main -->
<?php
if(empty($mobi))
{
    ?>
    <script src="<?= base_js?>main-index.js"></script>
<?php }else{ ?>
<script type="text/javascript">
        $(document).ready(function(e) {            
            // jQuery for page scrolling feature - requires jQuery Easing plugin
              $(function() {
                    $('a.page-scroll').bind('click', function(event) {
                        var $anchor = $(this);
                        $('html, body').stop().animate({
                            scrollTop: $($anchor.attr('href')).offset().top
                        }, 1500, 'easeInOutExpo');
                        event.preventDefault();
                    });
                });
              // Closes the Responsive Menu on Menu Item Click
              $('.navbar-collapse ul li a').click(function() {
                  $('.navbar-toggle:visible').click();
              });
            /*honeycombs*/
            $('.honeycombs').honeycombs().random_model();
            
            $('.recruit_parallelogram').random_model();
        });
    </script>
    <?php } ?>
<!--Javascript Style -->
<script type="text/javascript">
    $(document).ready(function(e) {
        /*popover*/
        $('[data-toggle="popover"]').popover({ html : true});
        $('body').on('click', function (e) {
            $('[data-toggle="popover"]').each(function () {
                //the 'is' for buttons that trigger popups
                //the 'has' for icons within a button that triggers a popup
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
    });
</script>
 <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <script src="https://apis.google.com/js/platform.js" async defer ></script>
	
	<!-- Chen code GA Mr.Tuyen-->
 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52251421-1', 'www.vmec.vn');
  ga('send', 'pageview');

</script> 
<a href="https://plus.google.com/112813861292456747582" rel="publisher"></a> 
	<!---End--->
	
	
</body>
</html>