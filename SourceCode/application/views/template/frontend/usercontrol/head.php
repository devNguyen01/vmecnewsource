<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="<?= company?>">
    <title><?= strip_tags($title) ?></title>
    <meta name="description" content="<?= strip_tags($description); ?>"/>
    <meta name="keywords" content="<?= strip_tags($keywords);?>"/>
    <link rel="shortcut icon" href="<?= base_url()?>favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?= base_url()?>favicon.ico" type="image/x-icon">
    <!-- Bootstrap -->
    <meta property="og:image" content="<?= trim($picture);?>"/>
    <meta property="og:title" content="<?= strip_tags($title)?>"/>
    <meta property="og:site_name" content="<?= company;?>"/>
    <meta property="og:url" content="<?= current_url()?>"/>
    <meta property="og:description" content="<?= strip_tags($description); ?>" />
    <meta property="og:type" content="website" />

    <!-- Bootstrap -->
    <link href="<?= base_css?>bootstrap.min.css" rel="stylesheet">
    <?php if(!empty($menuInfo['menu_com']) && $menuInfo['menu_com'] == 'home'){ ?>
    <link rel="stylesheet" type="text/css" href="<?= base_css?>homeycombs.css">
    <?php } ?>
    <link rel="stylesheet" type="text/css" href="<?= base_css?>style.css">
    <?php if(!empty($menuInfo['menu_com']) && $menuInfo['menu_com'] == 'home'){ ?>
    <link rel="stylesheet" type="text/css" href="<?= base_css?>home.css">
    <?php } ?>
    <?php if(!empty($menuInfo['menu_view'])){ ?>
    <link rel="stylesheet" type="text/css" href="<?= base_css?>select.css">
    <?php } ?>
    <link rel="stylesheet" type="text/css" href="<?= base_css?>media.css">
    <?php
    if(!empty($mobi) && $mobi=='mobi' && $this->uri->segment(1)=='')
    {
        ?>
        <link rel="stylesheet" type="text/css" href="<?= base_css?>style-mobile.css">
        <?php
    }
    ?>
    <link rel="stylesheet" type="text/css" href="<?= base_css?>mystyle.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	  <?php if(!empty($mobi) && $mobi=='mobi') { ?>
    <link rel="stylesheet" type="text/css" href="<?= base_css?>style-mobile.css">
	  <?php } ?>
    <link rel="stylesheet" type="text/css" href="<?= base_css?>fixie.css">
    <style type="text/css">
            .sh_home .item .image .img_hover, #products .image span, #products .image span strong, .carousel-indicators li{
                behavior: url(public/frontend/css/PIE.htc);
            }
            #about{
                padding-bottom: 5000px;
            }
        </style>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?= base_js?>jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>    -->
    <script src="<?= base_js?>bootstrap.min.js"></script>
    <script src="<?= base_js?>myscript.js"></script>
    <!--jQuery Easing-->
    <script type="text/javascript">
        var configs = {
            base_url: '<?= base_url()?>',
            curren_url: '<?= current_url();?>',
            theight: '<?= $theight;?>',
        }
    </script>
    <style type="text/css">
        .borpie{
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            behavior: url(public/frontend/css/PIE.htc);
        }
    </style>
</head>
<?php
    $background ='';
    if(!empty($menuInfo['menu_com']) && $menuInfo['menu_com'] != 'home' || isset($_REQUEST['q']))
    {
        $background ='data-spy="scroll" data-target=".scrollspy" style="background: none; padding-top: 80px;"';
    }
    if(!empty($menuInfo['menu_view']) && $menuInfo['menu_view']!='download'  && empty($info) && $menuInfo['menu_view'] != 'product' && $menuInfo['menu_view'] != 'tuyendung' && $menuInfo['menu_view'] != 'kinhnghiem'){
        $background ='data-spy="scroll" data-target=".scrollspy"';
    }
?>
<body <?= $background; ?>>
<input type="hidden" value="<?= $hd_cate; ?>" id="defaultcom">