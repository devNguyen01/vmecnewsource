<footer class="subfooter">      
        <section class="footer-bottom font-2">  
            <div class="container">
                <ul class="list-inline">
                    <li><a href="http://www.mitsubishielectric.com/copyright/terms.html"><?= dieu_khoan_su_dung?> <img src="<?= base_img?>open-blank.png" alt="icon_blank" class="icon_blank"></a></li>
                    <li><a href="http://www.mitsubishielectric.com/copyright/privacy.html"><?= chinh_sach_bao_mat?> <img src="<?= base_img?>open-blank.png" alt="icon_blank" class="icon_blank"></a></li>
                    <?php
                    if(!empty($menuFooter))
                    {
                        foreach ($menuFooter as $key => $value) {
                            $link = base_url().$value['menu_alias'].'.html';
                            echo '<li><a href="'.$link.'">'.$value["menu_name"].'</a></li>';
                        }
                    }
                    ?>
                </ul>
                <p class="copyright">
                    Copyright &copy; 2015 Mitsubishi Elevator Vietnam Co., Ltd. All rights reserved.
                </p>
            </div>
        </section>
    </footer>
    <div id="topcontrol"><a href="javascript:void(0)"><img src="<?= base_img?>skins/page-top.png" alt="page-top"></a></div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    
    <!--jQuery Easing-->
    <script src="<?= base_js?>jquery.easing.min.js"></script>
    <!-- InstanceBeginEditable name="footinsert2" -->    
    <!-- InstanceEndEditable -->
    <!--Javascript custom -->
    <script src="<?= base_js?>main.js"></script>        
    <!-- InstanceBeginEditable name="footerstyle2" -->    
    <!-- InstanceEndEditable -->
	<!-- Chen code GA Mr.Tuyen-->
 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52251421-1', 'www.vmec.vn');
  ga('send', 'pageview');

</script> 
<a href="https://plus.google.com/112813861292456747582" rel="publisher"></a> 
	<!---End--->
	
  </body>
<!-- InstanceEnd --></html>