<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= isset($title)?$title:""?> | CMS</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="shortcut icon" href="<?= base_url()?>favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= base_url()?>favicon.ico" type="image/x-icon">
        <link href="<?= admin_css_no?>bootstrap.min.css" rel="stylesheet" type="text/css" />        
        <link href="<?= admin_css_no?>font-awesome.min.css" rel="stylesheet" type="text/css" />        
        <link href="<?= admin_css_no?>ionicons.min.css" rel="stylesheet" type="text/css" />        
        <link href="<?= admin_css?>morris.css" rel="stylesheet" type="text/css" />        
        <link href="<?= admin_css?>jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />        
        <link href="<?= admin_css?>fullcalendar.css" rel="stylesheet" type="text/css" />        
        <link href="<?= admin_css?>daterangepicker-bs3.css" rel="stylesheet" type="text/css" />        
        <link href="<?= admin_css?>bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />        
        <link href="<?= admin_css?>AdminLTE.css" rel="stylesheet" type="text/css" />        
        <link href="<?= admin_css?>my_style_admin.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?= admin_css?>colorbox.css" />
        <link rel="stylesheet" href="<?= admin_css?>select2.css" />
        <link href="<?= admin_css?>tinhnguyenvan.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript">
            var configs = {
            base_url: '<?= base_url()?>',
            base_public: '<?= base_url()?>public/',
            admin_url:'<?= admin_url?>',
            admin_img:'<?= admin_img?>',
            base_js: '<?= base_url()?>public/backend/js/',
            admin_name: '<?=$this->uri->segment(1)?>',
            base_component: '<?=$this->uri->segment(2)?>',
            task:'<?= $this->uri->segment(3)?>',
            lang:'<?= $lang?>',
            page:'<?= $this->uri->segment(4)?>',
            curren_url: '<?= current_url();?>',
            }
        </script>

        <script src="<?= admin_js_no?>jquery.min.js"></script>
        <script type="text/javascript" src="<?= base_url()?>public/backend/library/editor/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="<?= base_url()?>public/backend/js/TextExt.js"></script>
        <script type="text/javascript" src="<?= base_url()?>public/backend/js/jwplayer.js"></script>
        <script type="text/javascript" src="<?= base_url()?>public/backend/js/maps.js"></script>
        
         
    </head>
       <body class="skin-blue">
        <header class="header">
            <a href="<?= admin_url?>" class="logo">
                CMS Admin
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav ajLang"></ul>
                    <ul class="nav navbar-nav">                        
                        <li class="dropdown messages-menu ajLoadBirthday"></li>
                        <li class="dropdown messages-menu ajLoadMailHead"></li>


                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i>
                                <span><?= $s_info["s_user_username"]?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header bg-light-blue">
                                    <img src="<?=base_file.'/user/'.$s_info["s_user_logo"];?>" class="img-circle" alt="<?= $s_info["s_user_first_name"]?>" />
                                    <p>
                                        <?= $s_info["s_user_last_name"].' ';?>
                                        <?= $s_info["s_user_first_name"]?>
                                        <small><?= isset($s_info['s_user_birthday'])?date("d/m/Y",$s_info['s_user_birthday']):""?></small>
                                    </p>
                                </li>
                                <li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <a href="<?= admin_url?>user/profile/?profile=update&redirect=<?= base64_encode(current_url())?>" class="">Profile</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="<?= admin_url?>user/lockscreen/?redirect=<?= base64_encode(current_url())?>" class="">Lock</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="<?= admin_url?>index/logout/" class="">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar">
                    <div class="user-panel row">
                        <div class="col-xs-3 image">
                            <img src="<?=base_file.'/user/'.$s_info["s_user_logo"];?>" class="img-circle" alt="<?= $s_info["s_user_first_name"]?>" />
                        </div>
                        <div class="col-xs-9 info">
                            <span><?= xin_chao?>,</span><br>
                            <small><?= $s_info["s_user_last_name"].' ';?> <?= $s_info["s_user_first_name"]?></small><br>
                            <a target="_blank" href="<?= base_url?>"><i class="fa fa-home text-success"></i> <?= xem?> Website</a>
                        </div>
                    </div>
                   <?php echo $this->mmenu->menu_left($lang); ?>
                </section>
            </aside>
