<div class="clearfix"></div>
<div class="content subcontent" style="min-height:600px;padding-top:110px">
<div class="container">
    <div class="row">
            <h1 class="title-main content_404"><span class="h1">Sorry, we can't find that page.</span></h1>
            <div class="content_404">
                <p>The page you are looking for is no longer available or has been moved.</p>
                <p>If you believe that this is an error please contact the Webmaster or your systems administrator, or double check your login data/Internet access point for errors.</p>

                <p>If you reached this page by clicking a link within <a href="http://www.vmec.vn">www.vmec.vn</a>,please contact the <a href="mailto:info@vmec.vn">Webmaster</a>.</p>

                <p>Thank you.</p>
            </div>
            <div class="text-center"><a href="<?= base_url()?>">Back</a></div>
        </div>
    </div>
</div>
<!-- <main id="content" class="content" style="margin-top:120px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                <div class="jumbotron mt30 text-center font-2">
                    <h1 style="font-size: 3.1em;">
                        <span class="text-danger"><i class="fa fa-warning"></i> <?= loi_404?>!</span><br/>
                        <small class="text-warning"><?= khong_tim_thay_trang_ban_can_tim?>.</small>
                    </h1>
                    <div class="alert alert-info" role="alert">
                        <?= ban_co_the_truy_cap_vao?>
                        <a href="<?= base_url()?>" class="alert-link"><?= trang_chu?></a> 
                        <?= hoac_su_dung_o_duoi_de_tim_kiem?>
                    </div>
                    <div class="form-group">
                    <form method="get" action="<?= base_url()?>tim-kiem/">
                        <div class="input-group">
                            <input type="text" class="form-control input-lg" required="required"  name="fkey" placeholder="Nhập từ khóa cần tìm...">
                            <span class="input-group-btn">
                                <button class="btn btn-lg btn-success" type="submit" data-toggle="tooltip" data-placement="top" data-original-title="Bấm để tìm" title="Bấm để tìm"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    </div>
                    <a class="btn btn-primary btn-lg" href="<?= base_url()?>" role="button" data-toggle="tooltip" data-placement="top" data-original-title="Trang chủ" title="Trang chủ"><i class="fa fa-2x fa-home"></i></a>
                </div>
            </div>
        </div>
    </div>
</main> -->
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function(){
        $('body').css({
            background: 'transparent',
        });
    });
</script>