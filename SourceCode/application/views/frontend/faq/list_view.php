<div class="clearfix"></div>
<div class="content subcontent">
    <!-- InstanceBeginEditable name="slideshow" -->
    <div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
              <?php
                if(!empty($banner_page))
                {
                    $link = !empty($banner_page[0]->banner_link) ? 'href="'.$banner_page[0]->banner_link.'"':'';
                    echo '<a '.$link.'>';
                        echo '<img alt="First slide" src="'.base_file.'banner/'.$banner_page[0]->banner_picture.'">';
                    echo '</a>';
                }
                else{
            ?>
                <a>
                    <img alt="First slide" src="<?= base_img?>sub-page/skill-banner.jpg">
                </a>
            <?php
                }
            ?>
            </div>
        </div>
     </div>
     <!--END: carousel-->
     <!-- InstanceEndEditable -->
     <div class="container">
        <div class="box-main">
            <?php $this->load->view("template/frontend/usercontrol/left.php"); ?>
            <!--End .sidebar-->
            <div class="primary">
                <!-- InstanceBeginEditable name="EditRegion1" -->
                <div class="breadcrumb-container">
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?= base_url(); ?>"><?= trang_chu?></a>
                        </li>
                        <li>
                            <a href="<?= base_url(); ?>#support"><?= ho_tro?></a>
                        </li>
                        <li class="active"><?= $title;?></li>
                    </ol>
                </div>
                <div class="clearfix">
                    <div class="title-news">
                        <div class="title-news_bg"></div>
                        <ul class="list-unstyled list-title-news">
                            <li class="<?= $ftype==1 ? 'active':'';?>"><a  href="<?= current_url().'/?ftype=1'?>"><?= cau_hoi_san_pham?></a></li>
                            <li class="<?= $ftype==2 ? 'active':'';?>"><a  href="<?= current_url().'/?ftype=2'?>"><?= cau_hoi_dich_vu?></a></li>
                        </ul>
                    </div>
                </div>
                <h1 class="title-main"><span><?= cau_hoi_thuong_gap; ?></span></h1>
                <!-- <div class="title-main-tab">
                    <h1 class="title-main"><span><?= cau_hoi_thuong_gap?></span></h1>
                    <ul class="list-inline list-title-main">
                        <li><a class="<?= $ftype==1 ? 'color_red':'';?>" href="<?= current_url().'/?ftype=1'?>"><?= cau_hoi_san_pham?></a></li>
                        <li><a class="<?= $ftype==2 ? 'color_red':'';?>" href="<?= current_url().'/?ftype=2'?>"><?= cau_hoi_dich_vu?></a></li>
                    </ul>
                </div> -->
                <div class="faq_list">
                    <div class="highlight">
                        <?= $menuInfo['menu_detail']?>
                    </div>
                    <!--END: highlight-->
                    <div class="faq_list-content">
                        <?php
                        if(!empty($list))
                        {
                          $i=1;
                            foreach ($list as $key => $value) {
                                $myReply = $this->mfaq_reply->getData('reply_content_'.$lang,array('faq_id'=>$value['id'],'reply_content_'.$lang.' !=' => ''));
                                if(!empty($myReply) && $value['faq_title_'.$lang]){
                                ?>
                                <div class="item">
                                    <h2 class="title">
                                        <strong class="ffq"><?= hoi?>:</strong> 
                                        <p class="pl15"><strong><?= $value['faq_title_'.$lang]; ?></strong></p>
                                    </h2>
                                    <div style="margin-top:5px;"><strong class="ffq"><?= dap?>:</strong> 
                                      <div class="pl15"><?= $myReply['reply_content_'.$lang]?></div>
                                    </div>
                                </div>
                                <hr />
                                <?php
                                }
                            }
                        }
                        ?>
                    </div>
                    <!--END: faq_list-content-->
                    <div class="clearfix">
                        <nav class="pull-right">
                          <ul class="pagination pagination-sm">
                            <?= $pagination; ?>
                          </ul>
                        </nav>
                    </div>
                    <!--/ .pagination-->
                </div>
                <!--END: faq_list-->
                <div class="form-faq" id="form-faq">
                    <div class="note"><?= tieu_de_them_moi_hoi_dap?></div>
                    <?php if(isset($_REQUEST['success']) && $_REQUEST['success']=='ok' && !isset($error)) { ?>
                        <div class="alert alert-success text-left"><?= gui_yeu_cau_thanh_cong?> !</div>
                    <?php } ?>
                    <?php
                    if(isset($error) && $error && count($error) >0 ){
                        echo '<div class="alert alert-danger">';
                            echo '<ul>';
                            foreach ($error as $key => $value) {
                                echo '<li>'.$value.'</li>';
                            }
                            echo '</ul>';
                        echo '</div>';
                    }
                    ?>
                    <form class="form-horizontal" method="post">
                      <div class="form-group">
                        <label for="faq_fullname" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label"><i class="fa fa-user"></i> <?= ho_va_ten?></label>
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                          <input type="text" name="faq_fullname" value="<?= $formData['faq_fullname']?>" id="faq_fullname" class="form-control flat" required="required"></div>
                      </div>
                      <div class="form-group">
                        <label for="faq_phone" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label"><i class="fa fa-phone"></i> <?= dien_thoai?></label>
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                          <input type="text" name="faq_phone" value="<?= $formData['faq_phone']?>" id="faq_phone" class="form-control flat"></div>
                      </div>
                      <div class="form-group">
                        <label for="faq_email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label"><i class="fa fa-envelope"></i> Email</label>
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                          <input type="email" name="faq_email" value="<?= $formData['faq_email']?>" id="faq_email" class="form-control flat" required="required"></div>
                      </div>
                      
                      <div class="form-group">
                        <label for="faq_title" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label"><i class="fa fa-edit"></i> <?= noi_dung?></label>
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                          <textarea class="form-control flat" name="faq_title" id="faq_title" rows="5"><?= $formData['faq_title']?></textarea>
                      </div>
                      </div>
                      <div class="form-group">
                        <label for="faq_captcha" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label"><i class="fa fa-key"></i> <?= ma_xac_nhan?></label>
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                          <input type="text" name="faq_captcha" id="faq_captcha" class="form-control flat input-list" required="required" minlength="5" maxlength="5">
                          <div id="captcha" style="float:left">
                            <span class="btn btn-primary"><?= $_SESSION['captcha']?></span>
                          </div>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-xs-12 col-sm-9 col-md-9 col-lg-9">
                          <button class="btn btn-main" name="fsubmit" type="submit"><i class="fa fa-send"></i> <?= gui?></button>
                        </div>
                      </div>
                    </form>
                </div>
                <!-- InstanceEndEditable -->
            </div>
         </div>
     </div>
</div>
