<div class="container">
    <div class="clearfix main_content">
        <?php if(!empty($mobi) && $mobi=='mobi') { ?>
             <h2 class="arr_sq title-mobile"><?= $categoryInfo['menu_name']?></h2>
            <h3 class="title-main"><span><?= cong_trinh_tieu_bieu?></span></h3>
        <?php }else{ ?>
        <h3 class="title-main"><span><?= $categoryInfo['menu_name']?></span></h3>
      <?php } ?>
      <div class="s_box s_banner">
        <div class="row row10">
            <?php
            if(!empty($category))
            {
                $i=1;
                foreach ($category as $key => $value) {
                    $link = base_url().$value['menu_alias'].'.html';
                    $picture = $value['menu_picture'] ? base_file.'menu/'.$value['menu_picture'] : base_img.'Construction-1.jpg';
                    ?>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <div class="item">
                            <a href="<?= $link?>" class="image">
                                <img src="<?= $picture?>" alt="<?= $value['menu_name']?>">
                                <span class="img_hover"><img src="<?= base_img?>Construction-icon-<?= $i++; ?>.png" alt="<?= $value['menu_name']?>"></span>
                            </a>
                            <div class="caption">
                                <h3 class="title">
                                    <a href="<?= $link?>"><span class="arrc"></span><?= $value['menu_name']?></a>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>
</div>