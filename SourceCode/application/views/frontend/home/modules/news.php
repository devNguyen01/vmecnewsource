<div class="container">
    <div class="clearfix main_content">
        <?php if(!empty($mobi) && $mobi=='mobi') { ?>
            <h2 class="arr_sq title-mobile"><?= $categoryInfo['menu_name']?></h2>
        <?php }else{?>
        <div class="clearfix">
            <div class="title-news">
                <div class="title-news_bg"></div>
                <!-- <ul class="list-unstyled list-title-news">
                    <li><a href="#" title="">Tin từ tập đoàn</a></li>
                    <li><a href="#" title="">Tin từ VMEC</a></li>
                </ul> -->
            </div>
        </div>
        <?php } ?>
        <div class="s_box">
            <div class="row new_tier1">
                <?php
                if(!empty($category))
                {
                    $i=1;
                    foreach ($category as $key => $value) {
                        if($i++ < 4 )
                        {
                            $link = base_url().$value['menu_alias'].'.html';
                            $picture = $value['menu_picture'] ? base_file.'menu/'.$value['menu_picture'] : base_img.'item-news-1.jpg';
                        ?>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="item">
                                    <a class="image" href="<?= $link?>">
                                        <img alt="<?= $value['menu_name']?>" src="<?= $picture?>" class="img_fixn">
                                    </a>
                                    <div class="caption">
                                        <h3 class="title">
                                            <a href="<?= $link?>"><span class="arrc"></span><?= $value['menu_name']?></a>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                    }
                }
                ?>
            </div>
            <div class="row new_tier2">
                <?php
                if(!empty($category))
                {
                    $k=1;
                    foreach ($category as $key => $value) {
                        if($k++ > 3 )
                        {
                            $link = base_url().$value['menu_alias'].'.html';
                            $picture = $value['menu_picture'] ? base_file.'menu/'.$value['menu_picture'] : base_img.'item-news-1.jpg';
                        ?>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="item">
                                    <a class="image" title="<?= $value['menu_name']?>" href="<?= $link?>">
                                        <img alt="<?= $value['menu_name']?>" src="<?= $picture?>" class="img_fixn">
                                    </a>
                                    <div class="caption">
                                        <h3 class="title">
                                            <a title="<?= $value['menu_name']?>" href="<?= $link?>"><span class="arrc"></span><?= $value['menu_name']?></a>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>