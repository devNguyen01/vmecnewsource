<div class="container">
    <div class="clearfix main_content">
        <?php if(!empty($mobi) && $mobi=='mobi') { ?>
            <h2 class="arr_sq title-mobile"><?= $categoryInfo['menu_name']?></h2>
        <?php }?>
        <div class="honeycombs">
          <?php
            if(!empty($category))
            {
                foreach ($category as $key => $value) {
                    $link = base_url().$value['menu_alias'].'.html';
                    $picture = $value['menu_picture'] ? base_file.'menu/'.$value['menu_picture'] : base_img.'item-service-1.jpg';
                    ?>
                    <a href="<?= $link?>" class="comb">
                        <img src="<?= $picture?>" /> <span><?= $value['menu_name']?></span> <strong class="service_bg"></strong>
                    </a>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>