<div class="container">
    <div class="clearfix main_content">
        <?php if(!empty($mobi) && $mobi=='mobi') { ?>
            <h2 class="arr_sq title-mobile"><?= $categoryInfo['menu_name']?></h2>
        <?php }?>
        <div class="s_box">
            <div class="row row15">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <?php
                    if(!empty($category))
                    {
                        $news_link = $category[0]->news_link == 0 ? ' <a href="'.$link.'" class="text-danger"><i>'.xem_chi_tiet.'</i></a>':'<a href="'.$link.'" target="_blank"><img src="'.base_img.'open-blank.png" alt="icon_blank" class="icon_blank"></a>';
                    ?>
                    <div class="item_sp item_sp-large">
                        <h3 class="title"><a href="<?= base_url().$category[0]['menu_alias'].'.html';?>"><span class="arrc"></span><?= $category[0]['menu_name']?></a></h3>
                        <div class="item_sp-body clearfix">
                            <a href="<?= base_url().$category[0]['menu_alias'].'.html';?>" class="image">
                                <img src="<?= base_file.'menu/'.$category[0]['menu_picture']?>" alt="<?= $category[0]['menu_name']?>">
                            </a>
                            <p><?= $category[0]['menu_detail']?>
                                <?= $news_link?>
                            </p>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="row row15" c="">
                        <?php
                        if(!empty($category))
                        {
                            $i=1;
                            foreach ($category as $key => $value) {
                                if($i>1){
                                    $link = base_url().$value['menu_alias'].'.html';
                                    $picture = $value['menu_picture'] ? base_file.'menu/'.$value['menu_picture'] : base_img.'support-1.jpg';
                                    $news_link = $value->news_link == 0 ? ' <a href="'.$link.'" class="text-danger"><i>'.xem_chi_tiet.'</i></a>':'<a href="'.$link.'" target="_blank"><img src="'.base_img.'open-blank.png" alt="icon_blank" class="icon_blank"></a>';
                                ?>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="item_sp item_sp-small">
                                            <h3 class="title"><a href="<?= $link?>"><span class="arrc"></span><?= $value['menu_name']?></a></h3>
                                            <div class="item_sp-body item_sp_4 clearfix">
                                                <a class="image" href="<?= $link?>">
                                                    <img alt="<?= $value['menu_name']?>" src="<?= $picture?>">
                                                </a>
                                                <p><?= $value['menu_detail']?> <?= $news_link?></p>
                                            </div>
                                        </div> 
                                    </div>
                                <?php
                                }
                                $i++;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>