<div class="container">
  <div class="clearfix main_content">
  <?php if(!empty($mobi) && $mobi=='mobi') { ?> <h2 class="arr_sq title-mobile"><?= $menuAb['menu_name']?></h2><?php } ?>
   <div class="s_box s_intro">
      <div class="row row10">
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
              <h3 class="title-main"><span><?= $aboutus->news_lang_name; ?></span></h3>
              <div class="intro">
                  <img alt="" src="<?= base_file.'news/'.$aboutus->news_picture; ?>">
                  <?= $aboutus->news_lang_detail; ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
              <h2 class="title-main"><span>Video</span></h2>
              <div class="box_video">
                  <?php if($aboutus->news_video) { ?><iframe width="100%" height="213" src="<?= $aboutus->news_video; ?>" frameborder="0" allowfullscreen></iframe><?php }else{ echo '<i>Video '.strtolower(dang_cap_nhat).'</i>'; } ?>

              </div>
              <!--END: box_video-->
          </div>
      </div>
  </div>
  <!--END: s_intro-->
  <div class="s_box s_banner">
      <div class="row row10">
        <?php
            if(!empty($category))
            {
                foreach ($category as $key => $value) {
                    $link = base_url().$value['menu_alias'].'.html';
                    $picture = $value['menu_picture'] ? base_file.'menu/'.$value['menu_picture'] : base_img.'intro-1.jpg';
                    ?>
                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                        <div class="item">
                            <a href="<?= $link?>" class="image">
                                <img src="<?= $picture?>" alt="<?= $value['menu_name']?>">
                            </a>
                            <div class="caption">
                                <h3 class="title">
                                    <a href="<?= $link?>"><span class="arrc"></span><?= $value['menu_name']?></a>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
        ?>
      </div>
  </div>
  <!--END: s_banner-->
</div>
</div>