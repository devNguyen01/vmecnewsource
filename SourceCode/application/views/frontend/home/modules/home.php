<?php if(empty($mobi) || !empty($ipad)) { ?><div class="clearfix main_content"></div><?php } ?>
<div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php
        if(!empty($banner_page))
        {
            $h=0;
            foreach ($banner_page as $key => $value) {
                $active = $h == 0 ? 'active':'';
                ?>
                    <li data-target="#carousel-home" data-slide-to="<?= $h; ?>" class="<?= $active;?> borpie"></li>
                <?php
                $h++;
            }
        }
        ?>
    </ol>
    <div class="carousel-inner">
        <?php
        if(!empty($banner_page))
        {
            $j=1;
            foreach ($banner_page as $key => $value) {
                $picture = base_file.'banner/'.$value->banner_picture;
                $link = !empty($value->banner_link) ? 'href="'.$value->banner_link.'"':'';
                $active = $j == 1 ? 'active':'';
                ?>
                    <div class="item <?= $active?>" data-number="<?= $j; ?>"><a <?= $link?>><img src="<?= $picture?>"  alt="<?= $value->banner_title?>" /></a></div>
                <?php
                $j++;
            }
        }
        ?>
    </div>
</div> 
<!-- END: Slideshow -->
<div class="clearfix main_content main_content2" style="padding-top:14px !important">  
    <div class="s_box">
        <div class="row row15">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="welcome">
                    <h2 class="title-main mb7"><a>Welcome</a></h2>
                    <p>
                    <?= $menuInfo['menu_detail']?>
                    </p>
                </div>
                <!--END: s_box-->
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="news">            
                    <h2 class="title-main mb7 mt5"><a><?= tin_tuc?></a></h2>
                    <ul class="list-unstyled list-news">
                        <?php
                        if(!empty($listNewHome))
                        {
                            foreach ($listNewHome as $key => $value) {
                                $myMenu = $this->mmenu->getInfoID($value->news_parent,$lang);
                                $link= base_url.$myMenu['menu_alias'].'/'.$value->news_lang_alias.'-news'.$value->id.'.html';
                                $news_link = $value->news_link == 0 ? '':'<img src="'.base_img.'open-blank.png" alt="icon_blank" class="icon_blank">';
                                $date = $lang == 'vn' ? 'Ngày '.date('d/m/Y',strtotime($value->news_create_date)): date('M d, Y',strtotime($value->news_create_date));
                                echo '<li><a href="'.$link.'" class="arr"> <strong>'.$date.'</strong>, '.$value->news_lang_name.' '.$news_link.'</a></li>';
                            }
                        }
                        ?>
                    </ul>
					<div class="clr"></div>
                </div>
				
                <!--END: s_box-->
            </div>
        </div>
    </div>
    <!--END: s_box-->
</div>