<div class="container">
    <div class="clearfix main_content">
        <?php if(!empty($mobi) && $mobi=='mobi') { ?>
            <h2 class="arr_sq title-mobile"><?= $categoryInfo['menu_name']?></h2>
        <?php }?>
        <div class="recruit_parallelogram">
            <?php
            if(!empty($category))
            {
                foreach ($category as $key => $value) {
                    $link = base_url().$value['menu_alias'].'.html';
                    $picture = $value['menu_picture'] ? base_file.'menu/'.$value['menu_picture'] : base_img.'item-recruit-1.jpg';
                    ?>
                    <div class="parallelogram">
                        <div class="parallelogram-1">
                            <div class="parallelogram-2" style="background-image: url(<?= $picture?>);">
                                <a href="<?= $link?>"><?= $value['menu_name']?></a>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>