<div class="container">
    <div class="clearfix main_content">
        <?php if(!empty($mobi) && $mobi=='mobi') { ?>
            <h2 class="arr_sq title-mobile"><?= $categoryInfo['menu_name']?></h2>
        <?php }?>
        <div class="s_box">
            <div class="row row0">
                <?php
                if(!empty($category))
                {
                    foreach ($category as $key => $value) {
                        $link = base_url().$value['menu_alias'].'.html';
                        $picture = $value['menu_picture'] ? base_file.'menu/'.$value['menu_picture'] : base_img.'produc-1.jpg';
                        ?>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <a href="<?= $link?>" class="image">
                                <img src="<?= $picture?>" alt="<?= $value['menu_name']?>">
                                <span><strong><?= $value['menu_name']?></strong></span>
                            </a>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>