<div id="fullpage" class="content"> 
    <section class="section sh_home" id="recruit">
        <?php $this->load->view("frontend/home/modules/recruit",$recruit);?>
    </section>
    <!--END: #recruit-->
    <section class="section sh_home" id="support">
        <?php $this->load->view("frontend/home/modules/support",$support);?>
    </section>
    <!--END: #support-->
    <section class="section sh_home" id="news">
        <?php $this->load->view("frontend/home/modules/news",$news);?>
    </section>
    <!--END: #news-->
    <section class="section sh_home" id="service">
        <?php $this->load->view("frontend/home/modules/service",$service);?>
    </section>
    <!--END: #service-->
    <section class="section sh_home" id="products">
        <?php $this->load->view("frontend/home/modules/products",$products);?>
    </section>
    <!--END: #products-->
    <section class="section sh_home" id="customer">
        <?php $this->load->view("frontend/home/modules/customer",$customer);?>
    </section>
    <!--END: #customer-->
    <section class="section sh_home" id="about">
        <?php $this->load->view("frontend/home/modules/about",$about);?>
    </section>
    <!--END: #about-->
    <section class="section" id="home">
        <?php $this->load->view("frontend/home/modules/home",$home);?>
    </section>
    <!--END: #home-->
</div>
<!--END: #fullpage-->