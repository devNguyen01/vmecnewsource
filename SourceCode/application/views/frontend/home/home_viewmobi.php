<div id="fullpage" class="content"> 
    <section class="section" id="home">
        <?php $this->load->view("frontend/home/modules/home",$home);?>
        <div class="clearfix main_content main_content2">
            <div class="s_box s_banner">
                <div class="row row15">
                    <?php
                    if(!empty($home['banner_home']))
                    {
                        foreach ($home['banner_home'] as $key => $value) {
                            $picture = base_file.'banner/'.$value->banner_picture;
                            $link = !empty($value->banner_link) ? 'href="'.$value->banner_link.'"':'';
                            ?>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <a <?= $link?> class="image imghome"><img src="<?= $picture?>" alt="<?= $value->banner_title?>"></a>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
           </div>
       </div>
    </section>
    <!--END: #home-->
    <section class="section sh_home" id="about">
        <?php $this->load->view("frontend/home/modules/about",$about);?>
    </section>
    <!--END: #about-->
    <section class="section sh_home" id="customer">
        <?php $this->load->view("frontend/home/modules/customer",$customer);?>
    </section>
    <!--END: #customer-->
    <section class="section sh_home" id="products">
        <?php $this->load->view("frontend/home/modules/products",$products);?>
    </section>
    <!--END: #products-->
    <section class="section sh_home" id="service">
        <?php $this->load->view("frontend/home/modules/service",$service);?>
    </section>
    <!--END: #service-->
    <section class="section sh_home" id="news">
        <?php $this->load->view("frontend/home/modules/news",$news);?>
    </section>
    <!--END: #news-->
    <section class="section sh_home" id="support">
        <?php $this->load->view("frontend/home/modules/support",$support);?>
    </section>
    <!--END: #support-->
    <section class="section sh_home" id="recruit">
        <?php $this->load->view("frontend/home/modules/recruit",$recruit);?>
    </section>
    <!--END: #recruit-->
</div>
<!--END: #fullpage-->
<section class="hidden-lg">
    <div class="container">
        <div class="clearfix main_content">
            <div class="wrapper-plink">
                <h3 class="title-main"><span>5 Core Value</span></h3>
                <ul class="plink">
                    <?php $this->load->view("template/frontend/usercontrol/menu_left");?>
                </ul>
            </div>
        </div>
    </div>
</section>
<style type="text/css" media="screen">
    /*#footer{
        width:980px !important;
    }*/
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('#topcontrol').css({bottom:"15px"});
        } else {
            $('#topcontrol').css({bottom:"-100px"});
        }
    });
    $('#topcontrol').click(function(){
        $('html, body').animate({scrollTop: '0px'}, 800);
        return false;
    });
})
    
</script>