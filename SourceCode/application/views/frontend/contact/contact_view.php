<div class="clearfix"></div>
    <div class="content subcontent">
        <!-- InstanceBeginEditable name="slideshow" -->
        <div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">            
            <div class="carousel-inner">
                <div class="item active">
                    <?php
                        if(!empty($banner_page))
                        {
                            $link = !empty($banner_page[0]->banner_link) ? 'href="'.$banner_page[0]->banner_link.'"':'';
                            echo '<a '.$link.'>';
                                echo '<img alt="First slide" src="'.base_file.'banner/'.$banner_page[0]->banner_picture.'">';
                            echo '</a>';
                        }
                        else{
                    ?>
                        <a>
                            <img alt="First slide" src="<?= base_img?>sub-page/skill-banner.jpg">
                        </a>
                    <?php
                        }
                    ?>
                </div>                        
            </div>                
         </div>
         <!--END: carousel-->
         <!-- InstanceEndEditable -->
         <div class="container">
            <div class="box-main">
                <?php $this->load->view("template/frontend/usercontrol/left.php"); ?>
                <!--End .sidebar-->
                <div class="primary">
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <div class="breadcrumb-container">
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?= base_url()?>"><?= trang_chu?></a>
                            </li>
                            <?php if($hd_cate && strtolower($menuInfo['menu_name']) != strtolower($title_cate)) { ?>
                            <li>
                                <a href="<?= base_url()?>#<?= $hd_cate?>"><?= $title_cate?></a>
                            </li>
                            <?php } ?>
                            <li class="active"><?= $title;?></li>
                        </ol>                        
                    </div>
                    <h1 class="title-main"><span><?= lien_he_voi_chung_toi?></span></h1>
                    <section class="sb_map">
                        <?php
                        if(!empty($contactInfo)){
                            $i=1;
                            foreach ($contactInfo as $key => $value) {
                                ?>
                                <div class="box_map">
                                    <h2 class="title">
                                        <?php
                                        if($i++==1)
                                        {
                                            echo company;
                                        }
                                        else{
                                            echo $value->company_contact_position;
                                        }
                                        ?>
                                    </h2>
                                    <address>
                                        <?= dia_chi?>: <?= $value->company_contact_address;?><br>
                                        <?= dien_thoai?>: <?= $value->company_contact_phone;?><br>
										Fax: <?= $value->company_contact_fax;?><br>
                                        Call center: <?= $value->company_contact_hotline;?><br>
                                        
                                        Email: <a href="mailto://<?= $value->company_contact_email;?>" title=""><?= $value->company_contact_email;?></a><br>
                                        Website: <a href="http://<?= $value->company_contact_website;?>" title=""><?= $value->company_contact_website;?></a><br>
										Facebook: <a href="https://<?= $value->company_contact_facebook;?>" title=""><?= $value->company_contact_facebook;?></a><br>
                                        <a href="#" title="" class="arr" data-toggle="modal" data-target=".modal-address-<?= $i;?>"><?= xem_ban_do?></a>
                                        <div class="modal fade modal-address-<?= $i;?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                          <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="map">
                                                    <?= $value->company_contact_maps;?>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                    </address>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </section>
                </div>
             </div>
         </div>         
    </div> 