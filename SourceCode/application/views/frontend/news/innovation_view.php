<!--[if lt IE 9]>
<style type="text/css">
        #footer{
        width: 100% !important;
        }
    </style>
<![endif]-->
<div class="clearfix"></div>
<div class="content">
    <!-- InstanceBeginEditable name="slideshow" -->
    <div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">          
        <div class="carousel-inner">
            <div class="item active">
                <?php
                    if(!empty($banner_page))
                    {
                        $link = !empty($banner_page[0]->banner_link) ? 'href="'.$banner_page[0]->banner_link.'"':'';
                        echo '<a '.$link.'>';
                            echo '<img alt="First slide" src="'.base_file.'banner/'.$banner_page[0]->banner_picture.'">';
                        echo '</a>';
                    }
                    else{
                ?>
                    <a>
                        <img alt="First slide" src="<?= base_img?>sl5/sl5-banner.jpg">
                    </a>
                <?php
                    }
                ?>
            </div>               
        </div>                
    </div>
    <!-- END: Slideshow -->  
    <!-- InstanceEndEditable -->          
    <div class="container container-1 container-body">
        <aside class="visible-lg aside_service"> 
            <!-- InstanceBeginEditable name="aside1" -->
            <ul id="nav_service" class="list-unstyled" data-spy="affix">
                <?php $this->load->view("template/frontend/usercontrol/menu_left");?>
            </ul>
            <!-- InstanceEndEditable --> 
        </aside>           
        <!--END: aside_service-->
        <div class="main_content">
         <!-- InstanceBeginEditable name="Template1" -->

         <!-- InstanceBeginEditable name="Template1" -->
         <section class="innovation">
            <h1 class="title-main"><span><?= strip_tags($innovation['box1_gioithieu']->news_lang_name); ?></span></h1>
            <div class="innov_tier1">
                <a href="<?= base_url.$menuInfo['menu_alias'].'/'.$innovation['box1_gioithieu']->news_lang_alias.'-news'.$innovation['box1_gioithieu']->id.'.html'?>" class="image">
                    <img src="<?=  base_file.'news/'.$innovation['box1_gioithieu']->news_picture?>" alt="" />
                </a>
                <div class="innov_desc text-justify">
                    <?= strip_tags($innovation['box1_gioithieu']->news_lang_detail);?>
                </div>
            </div>
            <!--END: innov_tier1-->
            <div class="innov_slogan">
                <?= $menuInfo['menu_detail']?>
            </div>
            <div class="innov_tier2" id="id109">
                <div class="row row15">
                    <?php
                    if(!empty($innovation['box2']))
                    {
                        $i=1;
                        foreach ($innovation['box2'] as $key => $value) {
                            $picture = !empty($value->news_picture)  ? base_file.'news/'.$value->news_picture : base_img.'no_image.png';
                            $myMenu = $this->mmenu->getInfoID($value->news_parent,$lang);
                            $link= base_url.$myMenu['menu_alias'].'/'.$value->news_lang_alias.'-news'.$value->id.'.html';
                        ?>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="cbox cbox3">
                                <div class="cbox_body" <?= $lang=='en' ? 'style="height:350px;"':''; ?>>
                                    <h3 class="title-foot">
                                        <a href="<?= $link; ?>" class="arrc"><?= $value->news_lang_name?></a>
                                    </h3>
                                    <div class="cbox-item3">
                                        <a href="<?= $link; ?>" class="image">
                                            <img src="<?= $picture?>" alt="<?= $value->news_lang_name?>">
                                        </a>
                                        <div class="caption">
                                            <p><?= strip_tags($value->news_lang_detail);?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END: cbox-->
                        </div>
                    <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <!--END: innov_tier2-->
            <h2 class="title-main"  id="id110"><span><?= cot_moc_sang_tao_cua_tap_doan?></span></h2>
            <div class="innov_tier3">
                <?php
                if(!empty($banner_innovation))
                {
                    foreach ($banner_innovation as $key => $value) {
                        $title = strip_tags($value->banner_title);
                        $picture = base_file.'banner/'.$value->banner_picture;
                        $link = !empty($value->banner_link) ? 'href="'.$value->banner_link.'"':'';
                        ?>
                        <a <?= $link?> class="image">
                            <img src="<?= $picture?>" alt="<?= $title?>">
                            <span>
                                <?= $value->banner_title?>
                            </span>
                        </a>
                        <?php
                    }
                }
                ?>
            </div>
        </section>
        <?php if(!empty($mobi) && $mobi == 'mobi') { ?>
               <section class="hidden-lg">
                    <div class="wrapper-plink">
                        <h3 class="title-main"><span>5 Core Value</span></h3>
                        <ul class="plink">
                            <?php $this->load->view("template/frontend/usercontrol/menu_left");?>
                        </ul>
                    </div>
                </section>
                <?php } ?>