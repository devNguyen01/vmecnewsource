<div class="clearfix"></div>
    <div class="content subcontent">
        <div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <?php
                        if(!empty($banner_page))
                        {
                            $link = !empty($banner_page[0]->banner_link) ? 'href="'.$banner_page[0]->banner_link.'"':'';
                            echo '<a '.$link.'>';
                                echo '<img alt="First slide" src="'.base_file.'banner/'.$banner_page[0]->banner_picture.'">';
                            echo '</a>';
                        }
                        else{
                    ?>
                        <a>
                            <img alt="First slide" src="<?= base_img?>sub-page/news-detail-banner.jpg">
                        </a>
                    <?php
                        }
                    ?>
                </div>
            </div>
         </div>
         <div class="container">
            <div class="box-main clearfix">
                <?php $this->load->view("template/frontend/usercontrol/left.php"); ?>
                <div class="primary">
                    <div class="breadcrumb-container">
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?= base_url()?>"><?= trang_chu?></a>
                            </li>
                            <?php if($hd_cate && strtolower($menuInfo['menu_name']) != strtolower($title_cate)) { ?>
                            <li>
                                <a href="<?= base_url()?>#<?= $hd_cate?>"><?= $title_cate?></a>
                            </li>
                            <?php } ?>
                            <li class="active"><?= strip_tags($title); ?></li>
                        </ol>
                    </div>
                    <h1 class="title-main"><span><?= $title; ?></span></h1>
                    <div class="sp_skill">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <ul class="list-unstyled arr">
                                    <?php
                                    if(!empty($list))
                                    {
                                        foreach ($list as $key => $value) {
                                            if($value->news_file){
                                                $linkdownload = base_file.'news/'.$value->news_file;
                                                $filesize = filesize(dir_root.'/'.str_replace(base_url, '', $linkdownload));
                                                echo '<li><a target="_blank" download href="'.$linkdownload.'">'.$value->news_lang_name.' <strong>(ZIP.'.sizeFilter($filesize).') <img src="'.base_img.'sub-page/icon-folder.jpg" alt="'.$value->news_lang_name.'"></strong></a></li>';
                                            }
                                        }
                                    }
                                    ?>
                                </ul>
                                <div class="clearfix mt20">
                                    <nav class="pull-right">
                                      <ul class="pagination pagination-sm">
                                        <?= $pagination; ?>
                                      </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
         </div>
    </div>