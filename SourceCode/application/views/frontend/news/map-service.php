<!DOCTYPE html>
<html lang="vi">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home | Vmec Mitsubishi</title>
    <!-- Bootstrap -->
    <link href="<?= base_css?>bootstrap.min.css" rel="stylesheet">         
    <style type="text/css">
        .map_service{
            width: 596px;
            height: 842px;
            position: relative;         
        }
        .map_service > .image{
            border: 1px solid #e5e5e5;
            display: block; 
        }
        .point{
            position: absolute; 
            width: 17px;
            height: 17px;
            display: inline-block;
            background: #ff0000;
            opacity: 0;filter: alpha(opacity=0);
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            -ms-border-radius: 50%;
            -o-border-radius: 50%;
            border-radius: 50%;     
            -webkit-transform: translateY(0%) translateX(0%) scale(1);
            transform: translateY(0%) translateX(0%) scale(1);
            -webkit-transition: all 0.86s;
            -moz-transition: all 0.86s;
            -ms-transition: all 0.86s;
            -o-transition: all 0.86s;
            transition: all 0.86s;
            behavior: url(public/frontend/css/PIE.htc);
        }
        .point-hanoi{
            top: 143px;
            left: 207px;
        }
        .point-dn{
            top: 404px;
            left: 316px;
        }
        .point-hcm{
            top: 678px;
            left: 243px;
        }
        .point:hover, .point:focus{
            opacity: 1;filter: alpha(opacity=100);
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            -ms-border-radius: 50%;
            -o-border-radius: 50%;
            border-radius: 50%; 
            opacity: 1;filter: alpha(opacity=100);  
            -webkit-transform: translateY(0%) translateX(0%) scale(2);
            transform: translateY(0%) translateX(0%) scale(2);  
            behavior: url(public/frontend/css/PIE.htc);
        }   
        .point:focus{
            outline: none;
        }
        .popover{
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            -ms-border-radius: 0;
            -o-border-radius: 0;
            border-radius: 0;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
        }
        .popover-content{
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 12px;
            font-weight: 300;
            line-height: 15px;
            max-width: 230px;           
        }
        .popover-content h2{
            font-size: 12px;
            font-weight: 500;
            margin: 0 0 5px 0;
        }
        .popover-content p{
            margin-bottom: 0;
        }
    </style>    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>   
</head>
<body>  
    <div class="map_service">
        <div class="image">
            <img src="<?= base_img?>sub-page/map-service.jpg" alt="Map Service">
        </div>
        <div class="btn-point">
            <?php
            $arr = array(1=>'hcm',2=>'hanoi',3=>'dn');
            if(!empty($contactInfo)){
                foreach ($contactInfo as $key => $value) {
                    $area = $arr[$key+1];
            ?>
                <a href="javascript:void(0);" tabindex="0" class="point point-<?= $area?>" data-placement="top" data-toggle="popover" data-trigger="focus" data-content="<h2><?= $value->company_contact_position;?></h2><p><?= dia_chi?>: <?= $value->company_contact_address;?><br><?= dien_thoai?>: <?= $value->company_contact_phone;?></p>"><span></span></a>
            <?php
                }
            }
            ?>
        </div>
    </div>
    <!--End map_service-->
    <!--End footer-->
    <!--[if IE]>
        <script type="text/javascript">
             var console = { log: function() {} };
        </script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?= base_js?>jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>    -->
    <script src="<?= base_js?>bootstrap.min.js"></script>
    <!--jQuery Easing-->
    <!--<script src="<?= base_js?>jquery.easing.min.js"></script>-->
    
     <script>
        $(document).ready(function(e) {
            /*popover*/
            $('[data-toggle="popover"]').popover({ html : true});
        });
    </script>    
</body>
</html>