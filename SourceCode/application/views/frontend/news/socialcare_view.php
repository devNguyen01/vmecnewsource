<!--[if lt IE 9]>
<style type="text/css">
        #footer{
        width: 100% !important;
        }
    </style>
<![endif]-->
<div class="clearfix"></div>
<div class="content">
    <!-- InstanceBeginEditable name="slideshow" -->
    <div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">           
        <div class="carousel-inner">
            <div class="item active">
                <?php
                    if(!empty($banner_page))
                    {
                        $link = !empty($banner_page[0]->banner_link) ? 'href="'.$banner_page[0]->banner_link.'"':'';
                        echo '<a '.$link.'>';
                            echo '<img alt="First slide" src="'.base_file.'banner/'.$banner_page[0]->banner_picture.'">';
                        echo '</a>';
                    }
                    else{
                ?>
                    <a>
                        <img alt="First slide" src="<?= base_img?>sl4/sl4-banner.jpg">
                    </a>
                <?php
                    }
                ?>
            </div>                       
        </div>                
    </div>
    <!-- END: Slideshow -->  
    <!-- InstanceEndEditable -->          
    <div class="container container-1 container-body">
        <aside class="visible-lg aside_service"> 
            <!-- InstanceBeginEditable name="aside1" -->
            <ul id="nav_service" class="list-unstyled" data-spy="affix">
                <?php $this->load->view("template/frontend/usercontrol/menu_left");?>
            </ul>
            <!-- InstanceEndEditable --> 
        </aside>           
        <!--END: aside_service-->
        <div class="main_content">
         <!-- InstanceBeginEditable name="Template1" -->               
         <section class="sscare">
            <div class="sscare1">                   
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                        <div class="sscare-tier1">
                            <?php if(!empty($social['box1'])) { ?>
                            <h1 class="title-main"><span><?= $social['box1']->news_lang_name?></span></h1>
                            <a class="image">
                            <img src="<?= base_file.'news/'.$social['box1']->news_picture?>" alt="<?= $social['box1']->news_lang_name?>">
                            </a>
                            <div class="text-justify">
                                <?= $social['box1']->news_lang_detail; ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <!--END .sscare-tier1-->
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                        <h2 class="title-main"><span><?= $social['menubox2']['menu_name']?></span></h2>
                        <div class="row row15 sscare-tier2">
                            <?php
                            if(!empty($myCate))
                            {
                                $i = 1;
                                foreach ($myCate as $key => $value) {
                                    if($i++ < 3) {
                                        $picture = base_file.'menu/'.$value['menu_picture'];
                                        $link = base_url().$value['menu_alias'].'.html';
                                    ?>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="rules">
                                            <a href="<?= $link?>" class="image">
                                            <img src="<?= $picture?>" alt="<?= $value['menu_name']?>">
                                            </a>
                                            <h3 class="title">
                                                <a href="<?= $link?>"><?= $value['menu_name']?></a>
                                            </h3>
                                        </div>
                                    </div> 
                                    <?php
                                    }
                                }
                            }
                            ?>
                        </div>

                        <div class="row row15 sscare-tier3">
                            <?php
                            if(!empty($myCate))
                            {
                                $i = 1;
                                foreach ($myCate as $key => $value) {
                                    if($i++ > 2) {
                                        $picture = base_file.'menu/'.$value['menu_picture'];
                                        $link = base_url().$value['menu_alias'].'.html';
                                    ?>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                        <div class="rules">
                                            <a href="<?= $link?>" class="image">
                                            <img src="<?= $picture?>" alt="<?= $value['menu_name']?>">
                                            </a>
                                            <h3 class="title">
                                                <a href="<?= $link?>"><?= $value['menu_name']?></a>
                                            </h3>
                                        </div>
                                    </div> 
                                    <?php
                                    }
                                }
                            }
                            ?>
                        </div>

                    </div>  
                </div>
            </div>
            <!--END: sscare1-->
            <div class="sscare2">
                <h2 class="title-main"><span><?= $social['menubox3']['menu_name']?></span></h2>
                <div class="wrapper-owl">
                    <div id="owl-demo" class="owl-carousel owl-theme-nav">
                        <?php
                        if(!empty($listNews))
                        {
                            foreach ($listNews as $key => $value) {
                                $picture = !empty($value->news_picture)  ? base_file.'news/'.$value->news_picture : base_img.'no_image.png';
                                $myMenu = $this->mmenu->getInfoID($value->news_parent,$lang);
                                $link= base_url.$myMenu['menu_alias'].'/'.$value->news_lang_alias.'-news'.$value->id.'.html';
                                ?>
                                    <div class="item">
                                        <a href="<?= $link;?>" class="image">
                                        <img src="<?= $picture; ?>" alt="<?= $value->news_lang_name; ?>" class="img_social">
                                        </a>
                                        <h3 class="title">
                                            <a href="<?= $link;?>"><?= substring($value->news_lang_name,150); ?></a>
                                        </h3>
                                    </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!--END: sscare2-->
        </section>
        <?php if(!empty($mobi) && $mobi == 'mobi') { ?>
               <section class="hidden-lg">
                    <div class="wrapper-plink">
                        <h3 class="title-main"><span>5 Core Value</span></h3>
                        <ul class="plink">
                            <?php $this->load->view("template/frontend/usercontrol/menu_left");?>
                        </ul>
                    </div>
                </section>
                <?php } ?>
         <link rel="stylesheet" type="text/css" href="<?= base_css?>owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="<?= base_css?>owl.theme.css">
        <script src="<?= base_js?>owl.carousel.min.js"></script>
        <script type="text/javascript">
        $(document).ready(function(e) {
            $('.owl-carousel').owlCarousel({
                items : 4,
                itemsCustom : false,
                itemsDesktop : [1199,4],
                itemsDesktopSmall : [980,3],
                itemsTablet: [768,2],
                itemsTabletSmall: false,
                itemsMobile : [479,1],
                singleItem : false,
                itemsScaleUp : false,
                pagination : false,
                navigation: true,
                navigationText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
                autoPlay: 3000,             
            })
        });     
    </script>   