<!--[if lt IE 9]>
<style type="text/css">
        #footer{
        width: 100% !important;
        }
    </style>
<![endif]-->
<div class="clearfix"></div>
    <div class="content">
        <!-- InstanceBeginEditable name="slideshow" -->
        <div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <?php
                        if(!empty($banner_page))
                        {
                            $link = !empty($banner_page[0]->banner_link) ? 'href="'.$banner_page[0]->banner_link.'"':'';
                            echo '<a '.$link.'>';
                                echo '<img alt="First slide" src="'.base_file.'banner/'.$banner_page[0]->banner_picture.'">';
                            echo '</a>';
                        }
                        else{
                    ?>
                        <a>
                            <img alt="First slide" src="<?= base_img?>sl3/sl3-banner.jpg">
                        </a>
                    <?php
                        }
                    ?>
                </div>                
            </div>                
        </div>
        <!-- END: Slideshow -->  
        <!-- InstanceEndEditable -->          
        <div class="container container-1 container-body">
            <aside class="visible-lg aside_service"> 
                <!-- InstanceBeginEditable name="aside1" -->
                <ul id="nav_service" class="list-unstyled" data-spy="affix">
                    <?php $this->load->view("template/frontend/usercontrol/menu_left");?>
                </ul>
                <!-- InstanceEndEditable --> 
            </aside>           
            <!--END: aside_service-->
            <div class="main_content">
               <!-- InstanceBeginEditable name="Template1" -->
               <section class="mainten">
                    <h1 class="title-main"><span><?= dinh_huong_hoat_dong?></span></h1>
                    <div class="row">
                        <?php
                        if(!empty($main['box1']))
                        {
                            $i=1;
                            foreach ($main['box1'] as $key => $value) {
                                $picture = !empty($value->news_picture)  ? base_file.'news/'.$value->news_picture : base_img.'no_image.png';
                                $link= base_url.$menuInfo['menu_alias'].'/'.$value->news_lang_alias.'-news'.$value->id.'.html';
                                $news_link = '<a href="'.$link.'"><img src="'.base_img.'open-blank.png" alt="icon_blank" class="icon_blank"></a>';
                            ?>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="cbox2">
                                    <a href="<?= $link; ?>" class="image">
                                        <img src="<?= $picture?>" alt="<?= $value->news_lang_name?>">
                                    </a>
                                    <div class="caption">
                                        <h2 class="title"><a href="<?= $link; ?>" class="arrc"><?= $value->news_lang_name?> </a></h2>
                                        <p>
                                            <?= substring($value->news_lang_summary,120); ?>
                                            <?php if(!empty($value->news_lang_summary)) { ?>
                                            <?= $news_link?>
                                            <?php } ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php
                            }
                        }
                        ?>
                    </div>
               </section>

                <?php if(!empty($mobi) && $mobi == 'mobi') { ?>
               <section class="hidden-lg">
                    <div class="wrapper-plink">
                        <h3 class="title-main"><span>5 Core Value</span></h3>
                        <ul class="plink">
                            <?php $this->load->view("template/frontend/usercontrol/menu_left");?>
                        </ul>
                    </div>
                </section>
                <?php } ?>