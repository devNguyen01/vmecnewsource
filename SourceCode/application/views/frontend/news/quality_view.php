<!--[if lt IE 9]>
<style type="text/css">
        #footer{
        width: 100% !important;
        }
    </style>
<![endif]-->
<div class="clearfix"></div>
    <div class="content">
        <!-- InstanceBeginEditable name="slideshow" -->        
        <div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">           
            <div class="carousel-inner">
                <div class="item active">
                <?php
                    if(!empty($banner_page))
                    {
                        $link = !empty($banner_page[0]->banner_link) ? 'href="'.$banner_page[0]->banner_link.'"':'';
                        echo '<a '.$link.'>';
                            echo '<img alt="First slide" src="'.base_file.'banner/'.$banner_page[0]->banner_picture.'">';
                        echo '</a>';
                    }
                    else{
                ?>
                    <a>
                        <img alt="First slide" src="<?= base_img?>sl1/sl1-banner.jpg">
                    </a>
                <?php
                    }
                ?>
                </div>                
            </div>                
        </div>        
        <!-- END: Slideshow -->  
        <!-- InstanceEndEditable -->          
        <div class="container container-1 container-body">
            <aside class="visible-lg aside_service"> 
                <!-- InstanceBeginEditable name="aside1" -->
                <ul id="nav_service" class="list-unstyled" data-spy="affix">
                    <?php $this->load->view("template/frontend/usercontrol/menu_left");?>
                </ul>
                <!-- InstanceEndEditable --> 
            </aside>           
            <!--END: aside_service-->
            <div class="main_content">
               <!-- InstanceBeginEditable name="Template1" -->
               <section class="quality">
                    <h1 class="title-main"><span><?= strip_tags($quality['menubox1']['menu_name'])?></span></h1>
                    <p class="italic">
                        <?php echo $quality['box1'][0]->news_lang_summary?>
                    </p>
                    <div class="quality-text">
                        <?php echo $quality['box1'][0]->news_lang_detail?>
                    </div>
                    <!--END: sb_quality-text-->
                    <div class="quality-service">
                        <div class="row row22">
                            <?php
                            if(!empty($quality['box1']))
                            {
                                $i=1;
                                foreach ($quality['box1'] as $key => $value) {
                                    if($i++ > 1)
                                    {
                                    ?>
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                        <div class="item">
                                            <h3 class="title arr_sq"><?= $value->news_lang_name; ?></h3>
                                            <div class="text-justify">
                                                <?= $value->news_lang_summary; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <!--END: sb_quality-service-->
                    <div class="ecosystem" id="id95">
                        <h2 class="title-main"><span><?= $quality['menubox2']['menu_name']?></span></h2>
                        <?= $quality['menubox2']['menu_detail']?>
                        <div class="clearfix"></div>
                        <div class="quality-avatar">
                            <img src="<?= base_file?>menu/<?= $quality['menubox2']['menu_picture']?>" alt="<?= $quality['menubox2']['menu_name']?>">
                        </div>
                        <!--END: quality-avatar-->
                        <div class="quality-service-2">
                            <div class="wrapper-owl">
                                <div class="owl-carousel">
                                    <?php 
                                    if(!empty($quality['box2']))
                                    {
                                        foreach ($quality['box2'] as $key => $value) {
                                            $myMenu = $this->mmenu->getInfoID($value->news_parent,$lang);
                                            $link= base_url.$myMenu['menu_alias'].'/'.$value->news_lang_alias.'-news'.$value->id.'.html';
                                            $news_link = $value->news_link == 0 ? '<Br /><a href="'.$link.'" class="text-danger rm_qua"><i>'.xem_chi_tiet.'</i></a>':'<a href="'.$link.'" target="_blank"><img src="'.base_img.'open-blank.png" alt="icon_blank" class="icon_blank"></a>';
                                            $picture = !empty($value->news_picture)  ? base_file.'news/'.$value->news_picture : base_img.'no_image.png';
                                            ?>
                                            <div class="item">
                                                <div class="img_item">
                                                    <a href="<?= $link?>">
                                                        <span class="img_item-pro" style="background-image:url(<?= $picture?>)"></span>
                                                    </a>
                                                    <h3 class="title"><a href="<?= $link?>"><?= $value->news_lang_name; ?></a></h3>
                                                </div>
                                                <p >
                                                    <?= substring(strip_tags($value->news_lang_summary),190); ?>
                                                    <?= $news_link; ?>
                                                </p>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--END: ecosystem-->
               </section>
               <?php if(!empty($mobi) && $mobi == 'mobi') { ?>
               <section class="hidden-lg">
                    <div class="wrapper-plink">
                        <h3 class="title-main"><span>5 Core Value</span></h3>
                        <ul class="plink">
                            <?php $this->load->view("template/frontend/usercontrol/menu_left");?>
                        </ul>
                    </div>
                </section>
                <?php } ?>
               <!--END: quality-->
<link rel="stylesheet" type="text/css" href="<?= base_css?>owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?= base_css?>owl.theme.css">
<script src="<?= base_js?>owl.carousel.min.js"></script>
<script type="text/javascript">
$(document).ready(function(e) {
    $('.owl-carousel').owlCarousel({
        items : 5,
        itemsCustom : false,
        itemsDesktop : [1199,5],
        itemsDesktopSmall : [980,3],
        itemsTablet: [768,2],
        itemsTabletSmall: false,
        itemsMobile : [479,1],
        singleItem : false,
        itemsScaleUp : false,
        pagination : true,
    })                      
});     
</script>   