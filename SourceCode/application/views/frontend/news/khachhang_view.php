<div class="clearfix"></div>
    <div class="content subcontent">
        <!-- InstanceBeginEditable name="slideshow" -->
        <div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <?php
                        if(!empty($banner_page))
                        {
                            $link = !empty($banner_page[0]->banner_link) ? 'href="'.$banner_page[0]->banner_link.'"':'';
                            echo '<a '.$link.'>';
                                echo '<img alt="First slide" src="'.base_file.'banner/'.$banner_page[0]->banner_picture.'">';
                            echo '</a>';
                        }
                        else{
                    ?>
                        <a>
                            <img alt="First slide" src="<?= base_img?>sub-page/news-detail-banner.jpg">
                        </a>
                    <?php
                        }
                    ?>
                </div>
            </div>
         </div>
         <!--END: carousel-->
         <!-- InstanceEndEditable -->
         <div class="container">
            <div class="box-main clearfix">
                <?php $this->load->view("template/frontend/usercontrol/left.php"); ?>
                <!--End .sidebar-->
                <div class="primary">
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <div class="breadcrumb-container">
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?= base_url()?>"><?= trang_chu?></a>
                            </li>
                            <li>
                                <a href="<?= base_url()?>#customer"><?= khach_hang?></a>
                            </li>
                            <li>
                                <a href="<?= base_url().$menuInfo['menu_alias']?>.html"><?= $menuInfo['menu_name']?></a>
                            </li>               
                            <li class="active"><?= $title; ?></li>
                        </ol>                        
                    </div>
                    <h1 class="title-main"><span><?= $title; ?></span></h1>
                    
                    <section class="s_box custom-gallery">
                        <div class="image image-large">
                            <a class="item">
                                <img src="<?= base_file.'news/'.$info->news_picture; ?>" alt="<?= $title; ?>" width="100%">
                            </a>
                        </div>
                        <div class="row row15">
                            <?php
                            if(!empty($mulImg))
                            {
                                foreach ($mulImg as $key => $value) {
                                    if($value){
                                        $picture = base_file.'news/'.$value;
                                        echo '<div class="col5"><div class="image">';
                                            echo '<img src="'.$picture.'" alt="'.$title.'" width="100%">';
                                        echo '</div>';
                                        echo '</div>';
                                    }
                                }
                            }
                            ?>
                        </div>
                    </section>
                    <!--END: cs-gallery-->
                    <section class="article-content">
                        <h2><?= thong_tin_du_an?></h2>
                        <?= !empty($info->news_lang_summary) ? $info->news_lang_summary:''; ?>
                        <?= !empty($info->news_lang_detail) ? $info->news_lang_detail:''; ?>
                    </section>
                    <!--END: article-content-->
                    <div class="sscare2">
                        <h2 class="title-main">
                            <span><?= cong_trinh_khac?></span>
                        </h2>
                        <div class="wrapper-owl">
                            <div id="owl-demo" class="owl-carousel owl-theme-nav">
                                <?php
                                if(!empty($same))
                                {
                                    foreach ($same as $key => $value) {
                                        $picture = !empty($value->news_picture)  ? base_file.'news/'.$value->news_picture : base_img.'no_image.png';
                                        $link= base_url.$menuInfo['menu_alias'].'/'.$value->news_lang_alias.'-news'.$value->id.'.html';
                                        ?>
                                        <div class="item">
                                            <a href="<?= $link; ?>" class="image">
                                                <img src="<?= $picture?>" alt="<?= $value->news_lang_name?>">
                                            </a>
                                            <h3 class="title">
                                                <a href="<?= $link; ?>"><?= $value->news_lang_name?></a>
                                            </h3>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- InstanceEndEditable -->
                </div>
             </div>
         </div>         
    </div>    
    <!-- InstanceBeginEditable name="footinsert2" -->    
       <link rel="stylesheet" type="text/css" href="<?= base_css?>owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?= base_css?>owl.theme.css">
    <link rel="stylesheet" type="text/css" href="<?= base_css?>owl.transitions.css">

    <script src="<?= base_js?>owl.carousel.min.js"></script>

<script>
$(document).ready(function(e) {       
  $('.sscare2 .owl-carousel').owlCarousel({
        items : 4,
        itemsCustom : false,
        itemsDesktop : [1199,4],
        itemsDesktopSmall : [980,3],
        itemsTablet: [768,2],
        itemsTabletSmall: false,
        itemsMobile : [479,1],
        singleItem : false,
        itemsScaleUp : false,
        pagination : false,
        navigation: true,
        navigationText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
        autoPlay: 3000,             
    })
});
</script>
<!-- InstanceEndEditable -->
