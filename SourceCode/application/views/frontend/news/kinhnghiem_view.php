<div class="clearfix"></div>
    <div class="content subcontent">
        <!-- InstanceBeginEditable name="slideshow" -->
        <div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">            
            <div class="carousel-inner">
                <div class="item active">
                    <?php
                        if(!empty($banner_page))
                        {
                            $link = !empty($banner_page[0]->banner_link) ? 'href="'.$banner_page[0]->banner_link.'"':'';
                            echo '<a '.$link.'>';
                                echo '<img alt="First slide" src="'.base_file.'banner/'.$banner_page[0]->banner_picture.'">';
                            echo '</a>';
                        }
                        else{
                    ?>
                        <a>
                            <img alt="First slide" src="<?= base_img?>sub-page/banner-intro.jpg">
                        </a>
                    <?php
                        }
                    ?>
                </div>                        
            </div>                
         </div>
         <!--END: carousel-->
         <!-- InstanceEndEditable -->
         <div class="container">
            <div class="box-main clearfix">
                <?php $this->load->view("template/frontend/usercontrol/left.php"); ?>
                <!--End .sidebar-->
                <div class="primary">
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <div class="breadcrumb-container">                        
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?= base_url()?>"><?= trang_chu?></a>
                            </li>
                            <li>
                                <a href="<?= base_url()?>#support"><?= ho_tro?></a>
                            </li>                   
                            <li class="active"><?= strip_tags($title); ?></li>
                        </ol>                        
                    </div>
                    <h1 class="title-main"><span><?= strip_tags($title); ?></span></h1>
                    <div class="sp_skill">
                        <div class="row">
                            <?php
                            if(!empty($menuCate))
                            {
                                foreach ($menuCate as $key => $value) {
                                    $condition_news=" news_lang='".$lang."' and news_status = 1 and  n.news_parent=".$value['id'];
                                    $object_news = 'n.id,n.news_parent';
                                    $object_news .= ',nl.news_lang_name,nl.news_lang_alias';
                                    $listNews = $this->mnews->getNews($object_news, $condition_news, 'n.id desc', 100);

                                    ?>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        <h3 class="title arr_sq"><?= $value['menu_name']?></h3>
                                        <a class="image">
                                         <img src="<?= base_file.'menu/'.$value['menu_picture']?>" alt="<?= $value['menu_name']?>">
                                        </a>
                                        <ul class="list-unstyled arr scroll_kn">
                                            <?php
                                            if(!empty($listNews))
                                            {
                                                foreach ($listNews as $k => $val) {
                                                    if($val->news_lang_name) { 
                                                    $link= base_url.$value['menu_alias'].'/'.$val->news_lang_alias.'-news'.$val->id.'.html';
                                                    ?>
                                                        <li><a href="<?= $link?>"><?= $val->news_lang_name?></a></li>
                                                    <?php
                                                    }
                                                }
                                            }
                                            ?>
                                            
                                        </ul>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <!--END: sp_skill-->
                    <!-- InstanceEndEditable -->
                </div>
             </div>
         </div>         
    </div>   