<div class="clearfix"></div>
    <div class="content subcontent">
        <!-- InstanceBeginEditable name="slideshow" -->
        <div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <?php
                        if(!empty($banner_page))
                        {
                            $link = !empty($banner_page[0]->banner_link) ? 'href="'.$banner_page[0]->banner_link.'"':'';
                            echo '<a '.$link.'>';
                                echo '<img alt="First slide" src="'.base_file.'banner/'.$banner_page[0]->banner_picture.'">';
                            echo '</a>';
                        }
                        else{
                    ?>
                        <a>
                            <img alt="First slide" src="<?= base_img?>sub-page/tuyen-dung-banner.jpg">
                        </a>
                    <?php
                        }
                    ?>
                </div>
            </div>
         </div>
         <!--END: carousel-->
         <!-- InstanceEndEditable -->
         <div class="container">
            <div class="box-main clearfix">
                <?php $this->load->view("template/frontend/usercontrol/left.php"); ?>
                <!--End .sidebar-->
                <div class="primary">
                    <!-- InstanceBeginEditable name="EditRegion1" -->
                    <div class="breadcrumb-container">
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?= base_url()?>"><?= trang_chu?></a>
                            </li>
                            <?php if($hd_cate  && strtolower($menuInfo['menu_name']) != strtolower($title_cate)) { ?>
                            <li>
                                <a href="<?= base_url()?>#<?= $hd_cate?>"><?= $title_cate?></a>
                            </li>
                            <?php } ?>
                            <li class="active"><?= strip_tags($title); ?></li>
                        </ol>
                    </div>
                    <?php if($fillter==1) { ?>
                    <div class="clearfix">
                        <div class="title-news">
                            <div class="title-news_bg"></div>
                            <ul class="list-unstyled list-title-news">
                                <li <?= $ntype==2 ? 'class="active"':''; ?>><a href="<?= current_url().'/?ntype=2&amp;page='.$page?>"><?= tin_tu_tap_doan?></a></li>
                                <li <?= $ntype==1 ? 'class="active"':''; ?>><a href="<?= current_url().'/?ntype=1&amp;page='.$page?>"><?= tin_tu_vmec?></a></li>
                            </ul>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if((!empty($menuInfo) && $menuInfo['id']== 22 || $menuInfo['menu_parent']==22) && $mobi != 'mobi') { ?>
                    <div class="clearfix">
                        <div class="title-news">
                            <div class="title-news_bg"></div>
                            <ul class="list-unstyled list-title-news">
                        <?php
                            if(!empty($menuHoatDong))
                            {
                                foreach ($menuHoatDong as $key => $value) {
                                    $link = base_url().$value['menu_alias'].'.html';
                                    $active = $value['menu_alias'] == str_replace('.html', '', $this->uri->segment(1)) ? 'class="active"':'';
                                    echo '<li '.$active.'><a '.$active.' href="'.$link.'"> '.$value['menu_name'].' </a></li>';
                                }
                            }
                        ?>
                        </ul>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <?php if(!empty($menuInfo) && $menuInfo['id']== 22 || $menuInfo['menu_parent']==22) { ?>
                    <div class="title-filter">
                    <?php } ?>
                    <h1 class="title-main"><span><?= strip_tags($title); ?></span></h1>
                    <?php
                    if(!empty($mobi) && $mobi == 'mobi')
                    {
                        echo '<ul class="list-inline">';
                            if(!empty($menuHoatDong))
                            {
                                foreach ($menuHoatDong as $key => $value) {
                                    $link = base_url().$value['menu_alias'].'.html';
                                    $active = $value['menu_alias'] == str_replace('.html', '', $this->uri->segment(1)) ? 'class="active"':'';
                                    echo '<li '.$active.'><a '.$active.' href="'.$link.'"> '.$value['menu_name'].' </a></li>';
                                }
                            }
                        echo '</ul>';
                    }
                    ?>
                    <div class="list-news">
                        <div class="row row22 minhe">
                            <?php
                            if(!empty($list))
                            {
                                $i=1;
                                foreach ($list as $key => $value) {
                                    $picture = !empty($value->news_picture)  ? base_file.'news/'.$value->news_picture : base_img.'no_image.png';
                                    if(empty($fkey)){
                                        $link= base_url.$menuInfo['menu_alias'].'/'.$value->news_lang_alias.'-news'.$value->id.'.html';
                                    }
                                    else{
                                        $myMenu = $this->mmenu->getInfoID($value->news_parent,$lang);
                                        $link= base_url.$myMenu['menu_alias'].'/'.$value->news_lang_alias.'-news'.$value->id.'.html';
                                    }
                                    $news_link = $value->news_link == 0 ? ' <a href="'.$link.'" class="text-danger"><i>'.xem_chi_tiet.'</i></a>':'<a href="'.$link.'" target="_blank"><img src="'.base_img.'open-blank.png" alt="icon_blank" class="icon_blank"></a>';
                                    $center = $menuInfo['id'] == 39 || $menuInfo['menu_parent'] == 39 ? 'textcenter':'';
                                    if($value->news_lang_name) {
                                ?>
                                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                        <div class="item">
                                            <a href="<?= $link; ?>" class="image">
                                                <img src="<?= $picture?>" alt="<?= $value->news_lang_name?>" class="<?= $fix_img; ?>">
                                            </a>
                                            <div class="caption">
                                                <h2 class="title <?= $center?>" title="<?= $value->news_lang_name?>"><a href="<?= $link; ?>"><?= $value->news_lang_name?></a></h2>
                                                <!-- <p><?= substring(strip_tags($value->news_lang_summary),120)?><?= $news_link?></p> -->
                                            </div>
                                        </div>
                                    </div>
                            <?php
                                    }
                                }
                            }else{
                                echo '<div class="col-xs-12">';
                                echo du_lieu_dang_cap_nhat;
                                echo '</div>';
                            }
                            ?>
                        </div>
                        <div class="clearclrfix"></div>
                        <div class="mt20">
                            <nav class="text-center">
                              <ul class="pagination pagination-sm">
                                <?php echo $pagination;?> 
                              </ul>
                            </nav>
                        </div>
                        <!--END /.pagination-->
                    </div>
                    <!-- InstanceEndEditable -->
                </div>
             </div>
         </div>
    </div>