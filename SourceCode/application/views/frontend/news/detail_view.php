<div class="clearfix"></div>
    <div class="content subcontent">
        <div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active">
                    <?php
                        if(!empty($banner_page))
                        {
                            $link = !empty($banner_page[0]->banner_link) ? 'href="'.$banner_page[0]->banner_link.'"':'';
                            echo '<a '.$link.'>';
                                echo '<img alt="First slide" src="'.base_file.'banner/'.$banner_page[0]->banner_picture.'">';
                            echo '</a>';
                        }
                        else{
                    ?>
                        <a>
                            <img alt="First slide" src="<?= base_img?>sub-page/news-detail-banner.jpg">
                        </a>
                    <?php
                        }
                    ?>
                </div>
            </div>
         </div>
         <div class="container">
            <div class="box-main clearfix">
                <?php $this->load->view("template/frontend/usercontrol/left.php"); ?>
                <div class="primary">
                    <div class="breadcrumb-container">
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?= base_url()?>"><?= trang_chu?></a>
                            </li>
                            <?php if($hd_cate && strtolower($menuInfo['menu_name']) != strtolower($title_cate)) { ?>
                            <li>
                                <a href="<?= base_url()?>#<?= $hd_cate?>"><?= $title_cate?></a>
                            </li>
                            <?php } ?>
                            <?php if(!empty($menuInfo) && strtolower($menuInfo['menu_name']) != strtolower($title)) { ?>
                            <li>
                                <a href="<?= base_url.$menuInfo['menu_alias'].'.html'?>"><?= strip_tags($menuInfo['menu_name'])?></a>
                            </li>
                            <?php } ?>
                            <li class="active"><?= $title; ?></li>
                        </ol>
                    </div>
                    <h1 class="title-main"><span><?= $title; ?></span></h1>
                    <div class="article-content">
                        <?= !empty($info->news_lang_summary) ? $info->news_lang_summary:''; ?>
                        <?= !empty($info->news_lang_detail) ? $info->news_lang_detail:''; ?>
                        <?php
                        if($info->news_parent == 84 && !empty($info->news_picture))
                        {
                            echo '<p style="text-align:center"><img src="'.base_file.'news/'.$info->news_picture.'" alt="'.$info->news_lang_name.'" style="width:500px"></p>';
                        }
                        ?>

                        <?php if($menuInfo['id']==6 || $menuInfo['menu_parent']==6) { ?>
                        <!-- begin ban do dich vu -->
                        <div class="hidden-xs hidden-sm" style="text-align: center; margin: 0 auto;">
                            <iframe width="596" height="842" frameBorder="0" src="<?= base_url().'news/map/' ?>"></iframe>
                        </div>
                        <!-- end ban do dich vu -->
                        <?php } ?>
                        <?php if(!empty($info->news_video)) { ?><iframe width="100%" height="400" src="<?= $info->news_video?>" frameborder="0" allowfullscreen></iframe><?php } ?>
                    </div>
                    <!--END: article-content-->

                    <?php
                        $menuGioiThieu = 3;
                        if(!empty($mobi) && $mobi=='mobi'){
                    ?>
                    <!-- danh cho goi thieu trang mobi -->
                    <aside class="visible-xs sidebar">
                        <div class="submenu">
                            <h3 class="title"><a href="<?= base_url().'#'.$hd_cate?>"><?= strip_tags($title_cate); ?></a></h3>
                            <ul class="list-unstyled">
                                <?php
                                if(!empty($menuChild))
                                {
                                    foreach ($menuChild as $key => $value) {
                                        $link = base_url().$value['menu_alias'].'.html';
                                        $active = $menuInfo['menu_alias'] == $value['menu_alias'] ? 'active':'';
                                        $menuChild2 = $this->mmenu->getMenu($value['id'],$lang);
                                        ?>
                                            <li class="<?= $active?>"><a href="<?= $link; ?>" class="arr_sqg"><?= $value['menu_name']?></a>
                                            <?php
                                            if(!empty($menuChild2))
                                            {
                                                echo '<ul class="list-unstyled">';
                                                foreach ($menuChild2 as $key2 => $value2) {
                                                    $link2 = base_url().$value2['menu_alias'].'.html';
                                                    $active = $menuInfo['menu_alias'] == $value2['menu_alias'] ? 'active':'';
                                                    $menuChild3 = $this->mmenu->getMenu($value2['id'],$lang);
                                                    if(!empty($menuChild3))
                                                    {
                                                        $link2 = '#';
                                                    }
                                                    echo '<li class="'.$active.'"><a class="arr" href="'.$link2.'">'.$value2['menu_name'].'</a>';
                                                    if(!empty($menuChild3))
                                                    {
                                                        echo '<ul class="list-unstyled pl10">';
                                                        foreach ($menuChild3 as $key3 => $value3) {
                                                            $link3 = base_url().$value3['menu_alias'].'.html';
                                                            $active = $menuInfo['menu_alias'] == $value3['menu_alias'] ? 'active':'';
                                                            echo '<li class="'.$active.'"><a class="arr" href="'.$link3.'">'.$value3['menu_name'].'</a></li>';
                                                        }
                                                        echo '</ul>';
                                                    }
                                                    echo '</li>';
                                                }
                                                echo '</ul>';
                                            }
                                            ?>
                                            </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </aside>
                    <!-- danh cho goi thieu trang mobi -->
                    <?php } ?>

                    <?php
                    if(!empty($same))
                    {
                    ?>
                    <div class="news">
                        <h2 class="title-main"><span><?= tin_tuc_lien_quan?></span></h2>
                            <ul class="list-unstyled list-news">
                                <?php
                                foreach ($same as $key => $value) {
                                    $link= base_url.$menuInfo['menu_alias'].'/'.$value->news_lang_alias.'-news'.$value->id.'.html';
                                    $news_link = $value->news_link == 0 ? '':' <img src="'.base_img.'open-blank.png" alt="icon_blank" class="icon_blank">';
                                    echo '<li><a href="'.$link.'" class="arr"> <strong>Ngày '.date('d/m/Y',strtotime($value->news_create_date)).',</strong> '.$value->news_lang_name.' '.$news_link.'</a></li>';
                                }
                                ?>
                            </ul>
                    </div>
                    <?php
                    }
                    ?>
                </div>
             </div>
         </div>
    </div>