<!--[if lt IE 9]>
<style type="text/css">
        #footer{
        width: 100% !important;
        }
    </style>
<![endif]-->
<div class="clearfix"></div>
<div class="content">
    <div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">          
        <div class="carousel-inner">
            <div class="item active">
                <?php
                    if(!empty($banner_page))
                    {
                        $link = !empty($banner_page[0]->banner_link) ? 'href="'.$banner_page[0]->banner_link.'"':'';
                        echo '<a '.$link.'>';
                            echo '<img alt="First slide" src="'.base_file.'banner/'.$banner_page[0]->banner_picture.'">';
                        echo '</a>';
                    }
                    else{
                ?>
                    <a>
                        <img alt="First slide" src="<?= base_img?>sl2/sl2-banner.jpg">
                    </a>
                <?php
                    }
                ?>
            </div>
        </div>
    </div>
    <div class="container container-1 container-body">
        <aside class="visible-lg aside_service"> 
            <ul id="nav_service" class="list-unstyled" data-spy="affix">
                <?php $this->load->view("template/frontend/usercontrol/menu_left");?>
            </ul>
        </aside>
        <div class="main_content">
            <section class="eco">
                <h1 class="title-main"><span><?= thong_diep_moi_truong?></span></h1>
                <?php if(!empty($ecochanges['box1'])) { ?>
                <h2 class="title-slogan"><?= $ecochanges['box1']->news_lang_name?></h2>
                <div class="eco-text">
                    <?= $ecochanges['box1']->news_lang_detail?>
                </div>
                <?php } ?>
                <div class="eco-action">
                    <div class="row row15">
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            <h2 class="title-main"><span>Video</span></h2>
                            <div class="box_video">
                                <?php if(!empty($ecochanges['video'])) { ?>
                                <iframe width="100%" height="240" src="<?= $ecochanges['video']->news_video;?>" frameborder="0" allowfullscreen></iframe>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                            <?php if(!empty($ecochanges['menubox2'])) { ?>
                            <h2 class="title-main"><span><?= $ecochanges['menubox2']['menu_name']?></span></h2>
                            <div class="row row15">
                                <?php
                                if(!empty($ecochanges['box2']))
                                {
                                    foreach ($ecochanges['box2'] as $key => $value) {
                                        $picture = !empty($value->news_picture)  ? base_file.'news/'.$value->news_picture : base_img.'no_image.png';
                                        $link= base_url.$ecochanges['menubox2']['menu_alias'].'/'.$value->news_lang_alias.'-news'.$value->id.'.html';
                                        ?>
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                            <div class="item">
                                                <div class="image">
                                                    <img src="<?= $picture?>" alt="<?= $value->news_lang_name?>">
                                                </div>
                                                <div class="caption">
                                                    <h3 class="title"><?= $value->news_lang_name?></h3>
                                                    <p><?= $value->news_lang_summary; ?></p>
                                                </div>
                                                <span class="icon_linkc"></span>
                                                <div class="wrapper-link"></div>
                                                <a href="<?= $link?>" class="link-full"><?= $value->news_lang_name?></a>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="sb_banner" id="id111">
                    <a href="http://www.mitsubishielectric.com/eco/index.html" class="image">
                        <img src="<?= base_img?>sl2/sl2-banner-1.jpg" alt="">
                    </a>
                </div>
                <div class="eco_power">
                    <?php if(!empty($ecochanges['menubox3'])) { ?><h2 class="title-slogan"><?= $ecochanges['menubox3']['menu_name']?></h2><?php } ?>
                    <div class="row row15">
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            <div class="box_video">
                                <?php if(!empty($ecochanges['box3']->news_video)) { ?><iframe width="100%" height="184" src="<?= $ecochanges['box3']->news_video?>" frameborder="0" allowfullscreen></iframe><?php } ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                            <?= !empty($ecochanges['box3']) ? $ecochanges['box3']->news_lang_detail:''; ?>
                            ...<a href="<?= base_url.$ecochanges['menubox3']['menu_alias'].'/'.$ecochanges['box3']->news_lang_alias.'-news'.$ecochanges['box3']->id.'.html'?>" class="icon_link"></a>
                        </div>
                    </div>
                    <div class="row row15" style="margin-top:10px;">
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            <div class="cbox">
                                <div class="wtop"></div>
                                <div class="cbox_body">
                                    <?php if(!empty($ecochanges['menubox4'])) { ?><h3 class="title-foot"><a href="<?= base_url.$ecochanges['menubox4']['menu_alias'].'.html'?>" class="arr_sqgr"> <?= $ecochanges['menubox4']['menu_name']?></a></h3><?php } ?>
                                    <?php if(!empty($ecochanges['box4'])) { 
                                        $link4 = base_url.$ecochanges['menubox4']['menu_alias'].'/'.$ecochanges['box4']->news_lang_alias.'-news'.$ecochanges['box4']->id.'.html';
                                        $news_link = $ecochanges['box4']->news_link == 0 ? ' <a href="'.$link4.'" class="text-danger"><i>'.xem_chi_tiet.'</i></a>':'<a href="'.$link4.'" class="icon_link"></a>';
                                        ?>
                                        <div class="cbox-item1">
                                            <a href="<?= $link4; ?>" class="image">
                                                <img src="<?= base_img?>sl2/sl2-app-1.png" alt="">
                                            </a>
                                            <div class="caption">
                                                <h4 class="highlight"><a href="<?= $link4; ?>"><?= $ecochanges['box4']->news_lang_name; ?></a></h4>
                                                <p class=""><?= $ecochanges['box4']->news_lang_summary ?>
                                                <?= $news_link?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="cbox-item-caption">
                                            <?= that_don_gian_ma_van_sieu_tiet_kiem?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                                <div class="cbox">
                                    <div class="cbox_body">
                                        <?php if(!empty($ecochanges['menubox5'])) { ?>
                                        <h3 class="title-foot ml51"><a class="arr_sqgr"> <?= $ecochanges['menubox5']['menu_name']?></a></h3>
                                        <div class="row row22">
                                            <?php
                                            if(!empty($ecochanges['box5']))
                                            {
                                                foreach ($ecochanges['box5'] as $key => $value) {
                                                    $picture = !empty($value->news_picture)  ? base_file.'news/'.$value->news_picture : base_img.'no_image.png';
                                                    $link= base_url.$ecochanges['menubox5']['menu_alias'].'/'.$value->news_lang_alias.'-news'.$value->id.'.html';
                                                    $news_link = $value->news_link == 0 ? ' <a href="'.$link.'" class="text-danger"><i>'.xem_chi_tiet.'</i></a>':'<a href="'.$link.'" class="icon_link"></a>';
                                                    ?>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="cbox-item2">
                                                            <a href="<?= $link?>"  class="image">
                                                                <img src="<?= $picture?>" alt="<?= $value->news_lang_name?>">
                                                            </a>
                                                            <h4 class="highlight"><a href="<?= $link?>"><?= $value->news_lang_name?></a></h4>
                                                            <p><?= $value->news_lang_summary?><?= $news_link; ?></p>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php if(!empty($mobi) && $mobi == 'mobi') { ?>
               <section class="hidden-lg">
                    <div class="wrapper-plink">
                        <h3 class="title-main"><span>5 Core Value</span></h3>
                        <ul class="plink">
                            <?php $this->load->view("template/frontend/usercontrol/menu_left");?>
                        </ul>
                    </div>
                </section>
                <?php } ?>