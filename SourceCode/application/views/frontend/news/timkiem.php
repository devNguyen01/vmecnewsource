<div class="clearfix"></div>
    <div class="content subcontent">
         <div class="container">
            <div class="box-main clearfix" style="min-height:470px; padding-left:0">
                <h1 id="page_title_s">Search Results</h1>
                <form class="form-inline seek" action="<?= base_url()?>tim-kiem/" method="get" id="seek2">
                  <div class="form-group">
                    <input type="text" class="form-control w500" name="q" id="q2">
                  </div>
                  <button type="submit" class="btn btn-default search_btn btn-primary"><i class="fa fa-search"></i> <?= tim_kiem?></button>
                </form>
                <p id="dispRange"></p>
                <div id="searchResults">Loading</div>
             </div>
         </div>
    </div>

<script type="text/javascript" src="http://www.google.com/jsapi"></script> 
<script type="text/javascript">
    function parseQueryFromUrl () {
        var queryParamName = "q";
        var search = window.location.search.substr(1);
        var parts = search.split('&');
        for (var i = 0; i < parts.length; i++) {
            var keyvaluepair = parts[i].split('=');
            if (decodeURIComponent(keyvaluepair[0]) == queryParamName) {
                return decodeURIComponent(keyvaluepair[1].replace(/\+/g, ' '));
            }
        }
        return '';
    }
    var queryString;
    google.load('search', '1', {language : 'en'});
    google.setOnLoadCallback(function() {
        var customSearchControl = new google.search.CustomSearchControl('008825074411651552352:slbvaburf-m');
        customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
        var options = new google.search.DrawOptions();
        options.enableSearchResultsOnly(); 
        customSearchControl.draw('searchResults', options);
        customSearchControl.setSearchCompleteCallback(null, onSearchComplete);
        var queryFromUrl = parseQueryFromUrl();
        if (queryFromUrl) {
            document.getElementById("q2").value = queryFromUrl;
            customSearchControl.execute(queryFromUrl);
            queryString = queryFromUrl;
        }
    }, true);
    function onSearchComplete(){
        //count image
        var img = new Image();
        img.src = "/search/img/count.gif?" + new Date().getTime();
        
        //display range
        try{
            var currentPageIndex = $(".gsc-cursor-current-page").html();
            if(currentPageIndex){
                currentPageIndex--;
                var start = currentPageIndex * 10;
                var end = start + $(".gsc-result").length;
                $("#dispRange").html("Results " + (start + 1) + " - " + end + " for <em>" + queryString + "</em>")
            }
        }
        catch(e){}
    }
</script>

<script>
  // (function() {
  //   var cx = '007260703708949770062:ic2aaadc3fm';
  //   var gcse = document.createElement('script');
  //   gcse.type = 'text/javascript';
  //   gcse.async = true;
  //   gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
  //       '//cse.google.com/cse.js?cx=' + cx;
  //   var s = document.getElementsByTagName('script')[0];
  //   s.parentNode.insertBefore(gcse, s);
  // })();
</script>
<!-- <gcse:search></gcse:search> -->