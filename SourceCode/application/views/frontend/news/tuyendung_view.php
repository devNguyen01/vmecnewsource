 <div class="clearfix"></div>
 <div class="content subcontent">
    <!-- InstanceBeginEditable name="slideshow" -->
    <div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">            
        <div class="carousel-inner">
            <div class="item active">
                <?php
                    if(!empty($banner_page))
                    {
                        $link = !empty($banner_page[0]->banner_link) ? 'href="'.$banner_page[0]->banner_link.'"':'';
                        echo '<a '.$link.'>';
                            echo '<img alt="First slide" src="'.base_file.'banner/'.$banner_page[0]->banner_picture.'">';
                        echo '</a>';
                    }
                    else{
                ?>
                    <a>
                        <img alt="First slide" src="<?= base_img?>sub-page/tuyen-dung-banner.jpg">
                    </a>
                <?php
                    }
                ?>
            </div>                        
        </div>                
    </div>
    <!--END: carousel-->
    <!-- InstanceEndEditable -->
    <div class="container">
        <div class="box-main clearfix">
            <?php $this->load->view("template/frontend/usercontrol/left.php"); ?>
            <!--End .sidebar-->
            <div class="primary">
                <div class="breadcrumb-container">
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?= base_url?>"><?= trang_chu?></a>
                        </li>
                        <li>
                            <a href="<?= base_url?>#recruit"><?= tuyen_dung?></a>
                        </li>                   
                        <li class="active"><?= strip_tags($menuInfo['menu_name'])?></li>
                    </ol>                        
                </div>
                <h1 class="title-main"><span><?= strip_tags($menuInfo['menu_name'])?></span></h1>
                <div class="info-point">
                    <?= $menuInfo['menu_detail']?>
                </div>


                <div class="recruit-accordion">
                    <div aria-multiselectable="true" id="accordion" class="panel-group">

                    <?php
                    if(!empty($list))
                    {
                        $i=1;
                        foreach ($list as $key => $value) {
                            $link= base_url.$menuInfo['menu_alias'].'/'.$value->news_lang_alias.'-news'.$value->id.'.html';
                            $icon = $i == 1 ? 'fa fa-minus-circle':'fa fa-plus-circle';
                            ?>
                            <div class="panel panel-default">
                                <div id="headingOne<?= $value->id;?>" class="panel-heading">
                                      <h2 class="panel-title">
                                        <a aria-controls="collapseOne<?= $value->id;?>" aria-expanded="true" href="#collapseOne<?= $value->id;?>" data-parent="#accordion" data-toggle="collapse" class="arrc">
                                          <?= $value->news_lang_name;?> <i class="<?= $icon?>"></i>
                                      </a>
                                      <span><a  aria-controls="collapseOne<?= $value->id;?>" aria-expanded="true" href="#collapseOne<?= $value->id;?>" data-parent="#accordion" data-toggle="collapse"><?= xem_chi_tiet?></a></span>
                                  </h2>
                                </div>
                                <div style="" id="collapseOne<?= $value->id;?>" class="panel-collapse collapse <?= $i==1 ? 'in':'';?>" aria-labelledby="headingOne<?= $value->id;?>">
                                      <div class="panel-body">
                                        <?= $value->news_lang_summary; ?>
                                        <?= $value->news_lang_detail; ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(e) {
        $('.collapse').on('shown.bs.collapse', function(){
            $(this).parent().find(".fa-plus-circle").removeClass("fa-plus-circle").addClass("fa-minus-circle");
            }).on('hidden.bs.collapse', function(){
            $(this).parent().find(".fa-minus-circle").removeClass("fa-minus-circle").addClass("fa-plus-circle");
        });
    });
</script>