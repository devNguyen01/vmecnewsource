<div class="clearfix"></div>
<div class="content subcontent">
    <div id="carousel-home" class="carousel slide carou_scrool" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <?php
                    if(!empty($banner_page))
                    {
                        $link = !empty($banner_page[0]->banner_link) ? 'href="'.$banner_page[0]->banner_link.'"':'';
                        echo '<a '.$link.'>';
                            echo '<img alt="First slide" src="'.base_file.'banner/'.$banner_page[0]->banner_picture.'">';
                        echo '</a>';
                    }
                    else{
                ?>
                    <a>
                        <img alt="First slide" src="<?= base_img?>sub-page/banner-product.jpg">
                    </a>
                <?php
                    }
                ?>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="box-main clearfix">
            <?php $this->load->view("template/frontend/usercontrol/left.php"); ?>
            <div class="primary">
                <div class="breadcrumb-container">
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?= base_url()?>"><?= trang_chu?></a>
                        </li>
                        <li>
                            <a href="<?= base_url()?>#products"><?= san_pham?></a>
                        </li>
                        <li class="active"><?= strip_tags($title)?></li>
                    </ol>
                </div>
                <div class="product">
                    <h1 class="title-main">
                    <span><?= !empty($info->news_lang_name) ? $info->news_lang_name : ''?></span>
                    </h1>
                    <section class="s_box pro-info content_pro">
                        <?= !empty($info->news_lang_detail) ? $info->news_lang_detail:''; ?>
                    </section>
                    <?php if(!empty($info->news_file)) { ?>
                    <section class="s_box sb-catalo">
                        <p>
                            <strong><?= catalogue_tai_ve?>:</strong>
                        </p>
                        <a href="<?= base_file.'news/'.$info->news_file; ?>" class="image" download>
                            <img src="<?= base_file.'news/'.$info->news_picture; ?>" alt="Download <?= $info->news_lang_name?>" style="width:195px; height:275px;">
                        </a>
                        <?php if(!empty($info->news_file2)) { ?>
                        <a href="<?= base_file.'news/'.$info->news_file2; ?>" class="image" download>
                            <img src="<?= base_file.'news/'.$info->news_picture; ?>" alt="Download <?= $info->news_lang_name?>" style="width:195px; height:275px;">
                        </a>
                        <?php } ?>
                    </section>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>