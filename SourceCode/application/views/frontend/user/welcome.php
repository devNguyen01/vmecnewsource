<main id="content" class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
                <div class="jumbotron mt30 text-center font-2">
                    <div class="alert alert-success">
                        Welcome: <b> <?= $s_member['s_user_username']?></b>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>