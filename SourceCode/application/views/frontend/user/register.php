<form role="form"  class="frm-right" method="post"  action="<?= base_url.'user/register/'?>">
<div class="modal-dialog">
    <div class="modal-content text-left">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">×</button>
            <h4 class="modal-title font-vip color-vip" id="myModalLabel">
                <i class="fa fa-unlock-alt"></i>
                Đăng ký
            </h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="input-group margintb-10">
                        <span class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </span>
                        <input type="text" placeholder="Tài khoản" required class="form-control" id="user_username" name="user_username"></div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="input-group margintb-10">
                        <span class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </span>
                        <input type="password" placeholder="Mật khẩu" class="form-control" required id="user_password" name="user_password"></div>
                </div>
            </div>
            <div class="input-group margintb-10">
                <span class="input-group-addon">
                    <i class="fa fa-user"></i>
                </span>
                <input type="text" placeholder="Họ và Tên" required class="form-control" id="user_first_name" name="user_first_name"></div>
            <div class="input-group margintb-10">
                <span class="input-group-addon">
                    <i class="fa fa-home"></i>
                </span>
                <input type="text" placeholder="Địa chỉ" required class="form-control" id="user_address" name="user_address"></div>
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="input-group margintb-10">
                        <span class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </span>
                        <input type="text" placeholder="Điện thoại" required class="form-control" id="user_phone" name="user_phone"></div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="input-group margintb-10">
                        <span class="input-group-addon">
                            <i class="fa fa-envelope"></i>
                        </span>
                        <input type="email" placeholder="Email" required class="form-control" id="user_email" name="user_email"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-main2 pull-left" id="xndangky">
                <i class="fa fa-send-o"></i>
                Đăng ký
            </button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">
                <i class="fa fa-times"></i>
                Đóng
            </button>
        </div>
    </div>
</div>
</form>