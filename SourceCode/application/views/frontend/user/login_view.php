<form role="form" class="frm-left" method="post" action="<?= base_url.'user/login/'?>">
<div class="modal-content text-left">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">×</button>
        <h4 class="modal-title font-vip color-vip" id="myModalLabel"> <i class="fa fa-user"></i>
            Đăng nhập
        </h4>
    </div>
    <div class="modal-body">
        <div class="input-group margintb-10">
            <span class="input-group-addon">
                <i class="fa fa-user"></i>
            </span>
            <input type="text" placeholder="Tài khoản" class="form-control" required="required" id="xnuser_acc" name="username"></div>
        <div class="input-group margintb-10">
            <span class="input-group-addon">
                <i class="fa fa-key"></i>
            </span>
            <input type="password" placeholder="Mật khẩu" class="form-control" required="required" id="xnuser_password" name="password"></div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-main2 pull-left" id="xndangnhap">
            <i class="fa fa-user"></i>
            Đăng nhập
        </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">
            <i class="fa fa-times"></i>
            Đóng
        </button>
    </div>
</div>
</form>