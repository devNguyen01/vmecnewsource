<?php
if($info){
    $config_video = $info["config_video"];
    $info_config_title = $info["config_title"];
    $info_config_keyword = $info["config_keyword"];
    $info_config_description = $info["config_description"];
}else{
    $info_config_description = $info_config_keyword = $info_config_title = "";
    $config_video = '';
}
$form = array("name"=>"basic_validate","method"=>"post","class"=>"form-control","id"=>"basic_validate","novalidate"=>"novalidate");
$config_title = array("name"=>"config_title","id"=>"config_title","value"=>$info_config_title,"class"=>"form-control");
$config_keyword = array("name"=>"config_keyword","id"=>"config_keyword","placeholder"=>"","value"=>$info_config_keyword,"class"=>"form-control");
$config_description = array("name"=>"config_description","id"=>"config_description","placeholder"=>"","value"=>$info_config_description,"class"=>"form-control");
$config_goole_analytics = array("name"=>"config_goole_analytics","id"=>"config_goole_analytics","placeholder"=>"","value"=>$config_goole_analytics,"class"=>"form-control");
$config_feeds = array("name"=>"config_feed","id"=>"config_feed","value"=>$config_feeds,"class"=>"form-control");

?>

<form accept-charset="utf-8" method="post" enctype="multipart/form-data">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <small><?= $title ?></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= admin_url ?>home/"><i class="fa fa-dashboard"></i> <?= trang_chu ?></a></li>
                <li class="active"><?= $title?></li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid box-primary">
                        <div class="box-header">
                            <i class="fa fa-info"></i>
                            <h3 class="box-title"><?= $title;?></h3>
                        </div>
                        <div class="box-body">
                            <!-- <div class="control-group">
                                <label class="control-label">Menu footer</label>
                                <div class="controls">
                                    <textarea name="config_video" class="form-control" rows="5"><?= $config_video?></textarea>
                                </div>
                            </div> -->

                            <div class="control-group">
                                <label class="control-label">Title</label>
                                <div class="controls">
                                    <?= form_input($config_title)?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Keyword</label>
                                <div class="controls">
                                    <?= form_input($config_keyword)?>
                                    <div class="help-ts">
                                        <i class="fa fa-info-circle"></i>
                                        <b class="text-danger"><?= tu_khoa?> SEO</b>
                                        <small>(<?= vi_du?>: thiết kế website chuyên nghiệp, thiết kế pro)</small>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Description</label>
                                <div class="controls">
                                    <?= form_input($config_description)?>
                                    <div class="help-ts">
                                        <i class="fa fa-info-circle"></i>
                                        <b class="text-danger"><?= mo_ta_ngan?> website</b>
                                        <small>(<?= vi_du?>: Công ty thiết kế website chuyên nghiệp với đội ngũ không ngừng học hỏi và phát triển năng lực bản thân...)</small>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Thông tin footer</label>
                                <div class="controls">
                                    <textarea name="config_footer" id="config_footer">
                                        <?= $info['config_footer']; ?>
                                    </textarea>
                                </div>
                            </div>
                           <!--  <div class="control-group">
                                <label class="control-label">Thông tin liên hệ</label>
                                <div class="controls">
                                    <textarea name="config_contact" id="config_contact">
                                        <?= $info['config_contact']; ?>
                                    </textarea>
                                </div>
                            </div> -->
                            <!-- <div class="control-group">
                                <label class="control-label">Background</label>
                                <div class="controls">
                                    <input type="file" name="config_background" id="config_background" class="form-control">
                                </div>
                                <?php
                                if($info['config_background'])
                                {
                                    echo '<span class="btn btn-xs btn-danger delete_config_background">Delete</span>';
                                    echo '<div class="config_background"><img src="'.base_img.$info['config_background'].'" /></div>';
                                }
                                ?>
                            </div> -->
                        
                        </div>
                    
                        <div class="box-footer text-center">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-save"></i>
                                <span><?= luu ?></span>
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </aside>
</form>
<script type="text/javascript" charset="utf-8" async defer>
    // CKEDITOR.replace("config_contact", {
    //         toolbar: [{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'FontSize', 'TextColor','Strike','Styles','Image', 'Format', 'Source', 'RemoveFormat' ] }], //makes all editors use this toolbar
    //         toolbarStartupExpanded : false,
    //         toolbarCanCollapse  : false,
    //         toolbar_Custom: [], //define an empty array or whatever buttons you want.
    //         enterMode: CKEDITOR.ENTER_BR
    //     } );
    CKEDITOR.replace("config_footer", {
            toolbar: [{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'FontSize', 'TextColor','Strike','Styles', 'Format', 'Source', 'RemoveFormat' ] }], //makes all editors use this toolbar
            toolbarStartupExpanded : false,
            toolbarCanCollapse  : false,
            toolbar_Custom: [], //define an empty array or whatever buttons you want.
            enterMode: CKEDITOR.ENTER_BR
        } );
</script>