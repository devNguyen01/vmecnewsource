<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&language=vi"></script>
<form accept-charset="utf-8" method="post" enctype="multipart/form-data">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <small><?= $title ?></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= admin_url ?>home/"><i class="fa fa-dashboard"></i> <?= trang_chu ?></a></li>
                <li class="active"><?= $title?></li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="box box-solid box-primary">
                        <div class="box-header">
                            <i class="fa fa-info"></i>
                            <h3 class="box-title"><?= thong_tin_doanh_nghiep?></h3>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="control-group">
                                        <label class="control-label"><?= ten_cong_ty?></label>
                                        <div class="controls">
                                            <?= $company["company_name"]; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="control-group">
                                        <label class="control-label"><?= ten_viet_tat?> <?= cong_ty?></label>
                                        <div class="controls">
                                            <?= $company["company_name_short"]; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="control-group">
                                        <label class="control-label">Hành động</label>
                                        <div class="controls">
                                            <a href="<?= admin_url?>config/company_update/1/" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="box box-success">
                        <div class="box-header">
                            <i class="fa fa-info-circle"></i>
                            <h3 class="box-title"><?= thong_tin_lien_he?></h3>

                            <div class="pull-right box-tools">
                                <a href="<?= admin_url?>config/add_contact/" class="btn btn-success btn-sm add_contact pointer" title="<?= them_moi?>">
                                    <i class="fa fa-plus"></i>
                                    <span><?= them?></span>
                                </a>
                            </div>
                        </div>

                        <div class="box-body table-responsive no-padding">
                            <table class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr>
                                        <th><?= dia_chi?></th>
                                        <th class="text-center"><?= dien_thoai?></th>
                                        <th class="text-center"><?= hotline?></th>
                                        <th class="text-center">Fax</th>
                                        <th class="text-center">Website</th>
                                        <th class="text-center">Facebook</th>
                                        <th class="text-center" style="min-width: 100px;"><?= hanh_dong?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if($company_contact){
                                        foreach($company_contact as $item){
                                            if($item["company_contact_facebook"]){
                                                $face = '<i class="fa fa-facebook"></i>';
                                                $link_face = $item["company_contact_facebook"];
                                            }else{
                                                $link_face = $face = "";
                                            }
                                            $link_delete = admin_url.'config/delete_contact/'.$item["company_id"].'/?redirect='.base64_encode(curPageURL());
                                            $link_update = admin_url.'config/update_contact/'.$item["company_id"].'/?redirect='.base64_encode(curPageURL());
                                            ?>
                                            <tr>
                                                <td><?= $item["company_contact_address"]?></td>
                                                <td class="text-center"><?= $item["company_contact_phone"]?></td>
                                                <td class="text-center"><?= $item["company_contact_hotline"]?></td>
                                                <td class="text-center"><?= $item["company_contact_fax"]?></td>
                                                <td class="text-center"><a href="http://<?= $item["company_contact_website"]?>" target="_blank" title="<?= bam_vao_de_mo?>"><?= $item["company_contact_website"]?></a></td>
                                                <td class="text-center">
                                                    <?php if($link_face) { ?>
                                                    <a href="<?= $link_face?>" target="_blank" title="<?= bam_vao_de_mo?>" class="btn btn-xs btn-social-icon btn-facebook"><?= $face?></a>
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center">
                                                    <a href="<?= $link_update?>" class="btn btn-xs btn-primary edit_company_contact" data-id="<?= $item["id"]?>"><i class="fa fa-edit"></i></a>
                                                    <a href="<?= $link_delete?>" onclick="return Delete();" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="box box-danger">
                        <div class="box-header">
                            <i class="fa fa-support"></i>
                            <h3 class="box-title"><?= ho_tro?></h3>

                            <div class="pull-right box-tools">
                                <a href="<?= admin_url?>config/add_support/" class="btn btn-success btn-sm add_support pointer" title="<?= them_moi?>">
                                    <i class="fa fa-plus"></i>
                                    <span><?= them?></span>
                                </a>
                            </div>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr>
                                        <th><?= ten_nhom?></th>
                                        <th class="text-center"><?= hotline?></th>
                                        <th class="text-center">Yahoo</th>
                                        <th class="text-center">Spyke</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center"><?= hanh_dong?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($company_support) {
                                        foreach($company_support as $item){
                                            $link_delete = admin_url.'config/delete_support/'.$item["id"].'/?redirect='.base64_encode(curPageURL());
                                            $link_update = admin_url.'config/update_support/'.$item["id"].'/?redirect='.base64_encode(curPageURL());
                                            ?>
                                            <tr>
                                                <td><?= $item["company_support_name"]?></td>
                                                <td class="text-center"><?= $item["company_support_hotline"]?></td>
                                                <td class="text-center"><?= $item["company_support_yahoo"]?></td>
                                                <td class="text-center"><?= $item["company_support_skype"]?></td>
                                                <td class="text-center"><?= $item["company_support_email"]?></td>
                                                <td class="text-center">
                                                    <a href="<?= $link_update?>" class="btn btn-xs btn-primary edit_company_contact" data-id="<?= $item["id"]?>"><i class="fa fa-edit"></i></a>
                                                    <a href="<?= $link_delete?>" onclick="return Delete();"  class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</form>