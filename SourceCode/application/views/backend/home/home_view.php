<aside class="right-side">
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<?php if($list_news){ ?>
<section class="content">
    <div class="row">
        <div class="col-lg-4 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><font><font>
                        <?= $sum_aritce?>
                    </font></font></h3>
                    <p><font><font>
                        Article
                    </font></font></p>
                </div>
                <div class="icon">
                    <i class="fa fa-list-alt "></i>
                </div>
                <a href="<?= admin_url?>news/index/" class="small-box-footer"><font><font class="">
                    More info </font></font><i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><font><font>
                        <?= $sum_mailto?></font></font>
                    </h3>
                    <p><font><font>
                        Email
                    </font></font></p>
                </div>
                <div class="icon">
                    <i class="fa fa-envelope"></i>
                </div>
                <a href="<?= admin_url?>mailto/index/" class="small-box-footer"><font><font>
                    More info </font></font><i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><font><font>
                        <?= $sum_user?>
                    </font></font></h3>
                    <p><font><font>
                        User Registrations
                    </font></font></p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="<?= admin_url?>user/index/" class="small-box-footer"><font><font>
                    More info </font></font><i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?= bai_viet?></h3>
            <small class="badge pull-right bg-yellow">Top 5/<?=$record_news?></small>
        </div>
        <div class="box-body table-responsive">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <table id="example1" class="table table-bordered table-striped dataTable" aria-describedby="example1_info">
                    <thead>
                        <tr role="row">
                            <th>Title</th>
                            <th>Author</th>
                            <th>Categories</th>
                            <th>View</th>
                            <th>Date</th>
                        </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php
                        foreach($list_news as $item){
                            $name_parent = $this->mmenu_lang->getData('',array("menu_lang"=>$lang,"menu_id"=>$item["news_parent"]));
                            if($name_parent){
                                $name_parent = $name_parent["menu_lang_name"];
                            }else{
                                $name_parent = "Chờ cập nhật";
                            }
                            $view = $item["news_view"];
                            /**bang news_lang*/
                            $data_lang = $this->mnews_lang->getData('',array("news_id"=>$item["id"],"news_lang"=>$lang));
                            if($data_lang){
                                $news_name = $data_lang["news_lang_name"];
                            }else{
                                $news_name = "Chờ cập nhật";
                            }
                            /**begin tac gia*/
                            $author = $this->muser->getOnceAnd(array("id"=>$item["user"]));
                            $authorName = (isset($author['user_username']) && $author['user_username'])?$author['user_username']:'--';
                            /**end tac gia*/
                            ?>
                            <tr class="odd">
                                <td><?= $news_name?></td>
                                <td><i><?= $authorName?></i></td>
                                <td><?= $name_parent?></td>
                                <td><small class="badge pull-right bg-red"><?= $view?></small></td>
                                <td><?= $item["news_update_date"]?></td>
                            </tr>
                            
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?= lich_su_dang_nhap?> </h3>
            <small class="badge pull-right bg-yellow">Top 10/<?=$record_history_login?></small>
        </div>
        <div class="box-body table-responsive">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
                <table id="example1" class="table table-bordered table-striped dataTable" aria-describedby="example1_info">
                    <thead>
                        <tr role="row">
                            <th><?= tai_khoan?></th>
                            <th>Level</th>
                            <th>IP</th>
                            <th><?= trinh_duyet?></th>
                            <th><?= thoi_gian?></th>
                        </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php if($list_history_login) { 
                            foreach ($list_history_login as $key => $value) {
                                $getlevel = $this->mgroup->getData(array("id"=>$value["history_level"]));
                                if($getlevel){
                                    $level_name = $getlevel["group_name"];
                                }else{
                                    $level_name = "Không xác định";
                                }
                                ?>
                                <tr class="gradeA odd">
                                    <td class=" sorting_1"><?= $value["history_username"]?></td>
                                    <td class=" "><?= $level_name?></td>
                                    <td class=" "><?= $value["history_ip"]?></td>
                                    <td class="center "><?= $value["history_user_agent"]?></td>
                                    <td class="center "><?= date("d/m/Y h:i:s a",$value["history_time"])?></td>
                                </tr>
                                <?php } 
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</aside>