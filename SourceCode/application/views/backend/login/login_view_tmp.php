<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title><?= dang_nhap?> | CMS</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="<?= admin_css_no?>bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= admin_css?>AdminLTE.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="login2">
        <form method="post" class="login2_form" id="myForm" autocomplete="off">
            <?php if(!empty($error)) { ?>
            <div class="form-group">
                <div class="alert alert-danger"><?= $error?></div>
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="checkID">ID login</label>
                <input type="text" class="form-control" id="checkID" name="checkID" placeholder="ID" required="required" autocomplete="off" value="">
            </div>
            <div class="form-group">
                <label for="checkPass">Password</label>
                <input type="password" class="form-control" id="checkPass" name="checkPass" placeholder="Password" required="required" autocomplete="off" value="">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</body>
</html>