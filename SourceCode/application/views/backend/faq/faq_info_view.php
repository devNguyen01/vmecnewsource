<form accept-charset="utf-8" method="post" enctype="multipart/form-data">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <small><?= $title ?></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= admin_url ?>home/"><i class="fa fa-dashboard"></i> <?= trang_chu ?></a></li>
                <li><a href="<?= admin_url ?>faq/"><?= danh_sach ?> FAQ</a></li>
                <li class="active"><?= $title ?></li>
            </ol>
        </section>

        <section class="content">
            <ul class="nav nav-tabs" role="tablist">
                <?php 
                if(!empty($language))
                {
                    $i = 1;
                    foreach ($language as $key => $value) {
                        $active = $i++ == 1 ? 'active' : '';
                        echo '<li role="presentation" class="'.$active.'"><a href="#'.$value->alias.'" aria-controls="'.$value->alias.'" role="tab" data-toggle="tab"><img src="'.base_file.'language/'.$value->picture.'" /> '.$value->name.'</a></li>';
                    }
                }
                ?>
              </ul>
              
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-info">
                                <div class="box-header">
                                    <i class="fa fa-info"></i>
                                    <h3 class="box-title"><?= $title; ?></h3>
                                </div>

                                <div class="box-body">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td>Tiêu đề</td>
                                                <td>
                                                    
                                                    <?php
                                                    if(!empty($language))
                                                    {
                                                        $i=1;
                                                        foreach ($language as $key => $value) {
                                                            if($myFaq['faq_title_'.$value->lang]) { 
                                                            ?>
                                                            
                                                                <?= $myFaq['faq_title_'.$value->lang]; ?>
                                                                <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" />
                                                    <?php
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Họ tên</td>
                                                <td><?= $myFaq['faq_fullname']?></td>
                                            </tr>
                                            <tr>
                                                <td>Điện thoại</td>
                                                <td><?= $myFaq['faq_phone']?></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><?= $myFaq['faq_email']?></td>
                                            </tr>
                                            <tr>
                                                <td>Ngày đăng</td>
                                                <td><?= date('d/m/Y',$myFaq['faq_createdate'])?></td>
                                            </tr>
                                            <tr>
                                                <td>Trạng thái</td>
                                                <td>
                                                    <?php
                                                    $statusName = $myFaq['faq_status'] == 1?"Đã duyệt":"Chưa duyệt" ;
                                                    $statusNote = $myFaq['faq_status'] == 1?"#0071cd":"#d85100" ;
                                                    ?>
                                                    <small class="label label-xs" style="background:<?= $statusNote?>;color:#fff"><?= $statusName?></small>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-success">
                                <div class="box-header">
                                    <i class="fa fa-info"></i>
                                    <h3 class="box-title">Dánh sách câu trả lời</h3>
                                </div>

                                <div class="box-body">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Nội dung</th>
                                                <th class="text-center" width="150px">Ngày trả lời</th>
                                                <th class="text-center" width="150px">Nhân viên</th>
                                                <th class="text-center" width="100px">Hành động</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if(!empty($myFaqReply))
                                        {
                                            foreach ($myFaqReply as $key => $value) {
                                                $author = $this->muser->getOnceAnd(array("id"=>$value["user"]));
                                                $authorName = (isset($author['user_username']) && $author['user_username'])?$author['user_username']:'--';
                                                $link_delete = admin_url.'faq/del_reply/'.$value['id'].'/?redirect='.base64_encode(current_url());
                                                    if(!empty($language))
                                                    {
                                                        $i=1;
                                                        foreach ($language as $k => $val) {
                                                            if($value['reply_content_'.$val->lang]) {
                                                            ?>
                                                <tr>
                                                    <td><?= $value['reply_content_'.$val->lang]; ?>
                                                    <img src="<?= base_file.'language/'.$val->picture;?>" width="20px" />
                                                    </td>
                                                    <td class="text-center"><?= date('d/m/Y H:i',$value['reply_create_date'])?></td>
                                                    <td class="text-center"><?= $authorName?></td>
                                                    <td class="text-center">
                                                        <a href="<?= $link_delete?>" onclick="return Delete();" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                </tr>
                                                <?php
                                                    }
                                                    }
                                                }
                                            }
                                        }else{
                                            ?>
                                            <tr>
                                                <td colspan="4" class="text-center">Chưa có câu trả lời</td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <i class="fa fa-info"></i>
                                    <h3 class="box-title">Forum trả lời</h3>
                                </div>

                                <div class="box-body">
                                <div class="tab-content">
                                <?php
                                if(!empty($language))
                                {
                                    $i=1;
                                    foreach ($language as $key => $value) {
                                        $active = $i++ == 1 ? 'active' : '';
                                        ?>
                                        <div role="tabpanel" class="tab-pane <?= $active?>" id="<?= $value->alias?>">
                                            <div class="form-group">
                                                <label class="control-label">Nội dung câu trả lời <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" /></label>
                                                <div class="controls">
                                                    <textarea name="reply_content_<?= $value->lang?>" class="form-control textarea" style="height:200px"><?= $formData['reply_content']?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                     <?php
                                    }
                                }
                                ?>
                                </div>

                                    <div class="box-footer text-center">
                                        <button type="submit" name="fsubmit" class="btn btn-primary" value="Save">
                                            <i class="fa fa-save"></i>
                                            <span>Save</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </section>
    </aside>
</form>
