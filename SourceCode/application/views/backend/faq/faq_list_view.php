<form accept-charset="utf-8" method="get" enctype="multipart/form-data">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <small><?= $title ?></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= admin_url?>home/" ><i class="fa fa-dashboard"></i> <?= trang_chu?></a></li>
                <li class="active"><?= $title ?></li>
            </ol>
        </section>

        <section class="content">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="fa fa-list-alt"></i>
                    <h3 class="box-title"><?= $title ?></h3>
                    <div class="pull-right box-tools">
                        <a href="<?= admin_url ?>faq/add/" class="btn btn-primary btn-sm" title="<?= them?> <?= bai_viet?>">
                            <i class="fa fa-plus"></i>
                            <span><?= them?></span>
                        </a>
                    </div>
                </div>

                <div id="example1_wrapper" class="box-body" role="grid">
                    <div class="row">
                        <div class="form-group col-md-5">
                            <select onchange="this.form.submit()" name="tstatus" class="form-control input-sm">
                                <option <?= $tstatus == 1 ? "selected":"";?> value="1"><?= hien_thi?></option>
                                <option <?= $tstatus == 0 ? "selected":"";?> value="0"><?= an?></option>
                            </select>
                        </div>
                        <div class="form-group col-md-5">
                            <input type="text" name="fkeyword" class="form-control input-sm" placeholder="<?= tim_kiem?>" value="<?= $fkeyword; ?>">
                        </div>
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> <?= tim_kiem?></button>
                        </div>
                    </div>
                </div>

                <div class="box-body table-responsive no-padding">
                    <table id="example1" class="table table-bordered table-striped dataTable table-checkbox" aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                            <th><?= tieu_de?></th>
                            <th><?= ho_va_ten?></th>
                            <th>Email</th>
                            <th><?= dien_thoai?></th>
                            <th class="text-center"><?= ngay_dang?></th>
                            <th class="text-center"><?= trang_thai?></th>
                            <th class="text-center" style="width: 129px;"><?= hanh_dong?></th>
                        </tr>
                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php
                        if(isset($list)){
                            foreach($list as $item){
                                $countFAQReply = $this->mfaq_reply->countQuery('faq_id='.$item["id"]);
                                $statusName = $item['faq_status'] == 1?"Đã duyệt":"Chưa duyệt" ;
                                $statusNote = $item['faq_status'] == 1?"#0071cd":"#d85100" ;
                                $link_edit = admin_url.'faq/update/'.$item["id"].'/?redirect='.base64_encode(curPageURL());
                                $link_delete = admin_url.'faq/delete/'.$item["id"].'/?redirect='.base64_encode(curPageURL());
                                $link_info = admin_url.'faq/info/'.$item["id"].'/?redirect='.base64_encode(curPageURL());
                                ?>
                                <tr class="odd">
                                    <td>
                                        <?= $item["faq_title_".$lang]; ?> <small class="badge bg-aqua"><?= $countFAQReply?></small>
                                    </td>
                                    <td>
                                        <?= $item["faq_fullname"]; ?>
                                    </td>
                                    <td>
                                        <?= $item["faq_email"]?>
                                    </td>
                                    <td>
                                        <?= $item["faq_phone"]?>
                                    </td>
                                    <td class="text-center"><?= date("d/m/Y",$item["faq_updatedate"]);?></td>
                                    <td class="text-center"><small class="label label-xs" style="background:<?= $statusNote?>;color:#fff"><?= $statusName?></small></td>
                                    <td>
                                        <div class="toolbar">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-default">Action</button>
                                                <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="<?= $link_edit?>" class="text-primary"><i class="fa fa-edit"></i> Cập nhật</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="<?= $link_info?>" class="text-success"><i class="fa fa-info"></i> Trả lời</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="<?= $link_delete?>" onclick="return Delete();" class="text-danger"><i class="fa fa-trash-o"></i>Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>

                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php if(isset($pagination)){echo $pagination;};?>
                        <li class="disabled"><a><?= tat_ca?>: <?= $record?></a></li>
                    </ul>
                </div>
            </div>
        </section>
    </aside>
</form>
