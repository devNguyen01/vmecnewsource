
<form accept-charset="utf-8" method="post" enctype="multipart/form-data" novalidate>
<aside class="right-side">
<section class="content-header">
    <h1>
        <small><?= $title ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= admin_url ?>home/"><i class="fa fa-dashboard"></i> <?= trang_chu ?></a></li>
        <li><a href="<?= admin_url ?>faq/"><?= danh_sach ?> FAQ</a></li>
        <li class="active"><?= $title ?></li>
    </ol>
</section>

<section class="content">
<div class="row">
<div class="col-md-12">
    <div class="box box-info">
        <div class="box-header">
            <i class="fa fa-info"></i>
            <h3 class="box-title"><?= $title; ?></h3>
        </div>

        <div class="box-body">
            <div class="row">
            <ul class="nav nav-tabs" role="tablist">
                <?php 
                if(!empty($language))
                {
                    $i = 1;
                    foreach ($language as $key => $value) {
                        $active = $i++ == 1 ? 'active' : '';
                        echo '<li role="presentation" class="'.$active.'"><a href="#'.$value->alias.'" aria-controls="'.$value->alias.'" role="tab" data-toggle="tab"><img src="'.base_file.'language/'.$value->picture.'" /> '.$value->name.'</a></li>';
                    }
                }
                ?>
              </ul>
              <div class="tab-content">
                    <div class="col-md-4 form-group">
                        <label class="control-label">Họ tên</label>
                        <div class="controls">
                            <input type="text" class="form-control faq_fullname" required="required" name="faq_fullname" id="faq_fullname" value="<?= $formData['faq_fullname']?>">
                        </div>
                    </div>
                    <div class="col-md-4 form-group">
                        <label class="control-label">Email</label>
                        <div class="controls">
                            <input type="email" class="form-control faq_email" required="required" name="faq_email" id="faq_email" value="<?= $formData['faq_email']?>">
                        </div>
                    </div>
                    <div class="col-md-4 form-group">
                        <label class="control-label">Điện thoại</label>
                        <div class="controls">
                            <input type="text" class="form-control faq_phone" name="faq_phone" id="faq_phone" value="<?= $formData['faq_phone']?>">
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <label class="control-label">Loại</label>
                        <div class="controls">
                            <select name="faq_type" id="faq_type" class="form-control">
                                <option <?= $formData['faq_type']==1 ? 'selected' : '';?> value="1">Câu hỏi sản phẩm</option>
                                <option <?= $formData['faq_type']==2 ? 'selected' : '';?> value="2">Câu hỏi dịch vụ</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">
                        <label class="control-label">Trạng thái</label>
                        <div class="controls">
                            <select name="faq_status" class="form-control faq_status">
                                <option <?= $formData['faq_status'] == 1 ? 'selected':''; ?> value="1"><?= hien_thi?></option>
                                <option <?= $formData['faq_status'] == 0 ? 'selected':''; ?> value="0">Không <?= hien_thi?></option>
                            </select>
                        </div>
                    </div>

                <?php
                if(!empty($language))
                {
                    $i=1;
                    foreach ($language as $key => $value) {
                        $active = $i++ == 1 ? 'active' : '';
                        ?>
                        <div role="tabpanel" class="tab-pane <?= $active?>" id="<?= $value->alias?>">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label class="control-label">Tiêu đề <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" /></label>
                                    <div class="controls">
                                        <input type="text" class="form-control faq_title" required="required" name="faq_title_<?= $value->lang?>" id="faq_title" value="<?= $formData['faq_title_'.$value->lang]?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
                </div>
        <div class="box-footer text-center">
            <button type="submit" name="fsubmit" class="btn btn-primary" value="Save">
                <i class="fa fa-save"></i>
                <span>Save</span>
            </button>
        </div>

    </div>
</div>
</div>
</section>
</aside>
</form>
