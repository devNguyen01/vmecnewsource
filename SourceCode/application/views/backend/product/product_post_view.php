<form accept-charset="utf-8" method="post" enctype="multipart/form-data">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <small><?= $title ?></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= admin_url ?>home/"><i class="fa fa-dashboard"></i> <?= trang_chu ?></a></li>
                <li><a href="<?= admin_url ?>product/"><?= danh_sach ?></a></li>
                <li class="active"><?= $title ?></li>
            </ol>
        </section>
        <section class="content">
        <div class="row">
            <div class="col-md-12">
              <ul class="nav nav-tabs" role="tablist">
                <?php 
                if(!empty($language))
                {
                    $i = 1;
                    foreach ($language as $key => $value) {
                        $active = $i++ == 1 ? 'active' : '';
                        echo '<li role="presentation" class="'.$active.'"><a href="#'.$value->alias.'" aria-controls="'.$value->alias.'" role="tab" data-toggle="tab"><img src="'.base_file.'language/'.$value->picture.'" /> '.$value->name.'</a></li>';
                    }
                }
                ?>
              </ul>
              <!-- box tag -->
              <div class="tab-content">
              <!-- commont -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label"><?=danh_muc?></label>
                            <div class="controls">
                                <select name="product_parent" class="form-control">
                                    <?php $this->mmenu->dropDownMenu($formData['product']['product_parent'],'product'); ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label"><?=chieu_cao?></label>
                            <div class="controls">
                                <input type="text" class="form-control" name="product_height" id="product_height" value="<?= $formData['product']['product_height']?>" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label"><?= chieu_rong?></label>
                            <div class="controls">
                                <input type="text" class="form-control" name="product_width" id="product_width" value="<?= $formData['product']['product_width']?>" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label"><?=chieu_dai?></label>
                            <div class="controls">
                                <input type="text" class="form-control" name="product_length" id="product_length" value="<?= $formData['product']['product_length']?>" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label"><?=so_thu_tu?></label>
                            <div class="controls">
                                <input type="number" class="form-control" name="product_orderby" id="product_orderby" value="<?= $formData['product']['product_orderby']?>" />
                            </div>
                        </div>
                    </div> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group ">
                                <label class="control-label"><?= trang_thai?></label>
                                <div class="controls">
                                    <input type="checkbox" value="1" <?= $formData['product']['product_status'] == 1 ? "checked" : "" ?> name="product_status" checked> <i class="fa fa-eye"></i> <?= hien_thi?>
                                    <!-- <input type="checkbox" value="1" <?= $formData['product']['product_hot'] == 1 ? "checked" : "" ?> name="product_hot"> <i class="fa fa-trophy"></i> <?= noi_bat?>
                                    <input type="checkbox" value="1" <?= $formData['product']['product_home'] == 1 ? "checked" : "" ?> name="product_home"> <i class="fa fa-home"></i> <?= trang_chu?> -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="text-left block">Hình ảnh đại diện</label>
                            <div class="controls row">
                                <div class="col-lg-12"><input type="file" name="product_picture" class="form-control mb10"></div>
                            </div>

                            <ul class="mulpic_ser">
                            <?php
                            if($formData['product']['product_picture'])
                            {
                                echo '<li><img src="'.base_file.'product/'.$formData['product']['product_picture'].'" width="100"  height="100"/><span class="text-danger"><input type="checkbox" name="removefile" value="1"> <i class="fa fa-trash-o"></i> Trash</span></li>';
                            }
                            ?>
                            </ul>
                        </div>
                        <div class="alert alert-warning">Hình ảnh không vượt quá 2MB. File định dạng GIF | JPG | PNG</div>
                    </div>
                </div>
                <?php
                if(!empty($language))
                {
                    $i=1;
                    foreach ($language as $key => $value) {
                        $active = $i++ == 1 ? 'active' : '';
                        ?>
                        <div role="tabpanel" class="tab-pane <?= $active?>" id="<?= $value->alias?>">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label"><?= ma_san_pham?> <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" /></label>
                                        <div class="controls">
                                            <input type="text" class="form-control product_code_<?= $value->lang?>" name="product_code_<?= $value->lang?>" id="product_code" value="<?= $formData[$value->lang]['product_code'] ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label"><?= ten_san_pham?> <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" /></label>
                                        <div class="controls">
                                            <input type="text" class="form-control product_lang_name_<?= $value->lang?>" name="product_lang_name_<?= $value->lang?>" id="product_lang_name" value="<?= $formData[$value->lang]['product_lang_name'] ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?= gia_ban?> <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" /></label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="product_lang_price_<?= $value->lang?>" id="product_lang_price" value="<?= $formData[$value->lang]['product_lang_price'] ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label"><?= gia_khuyen_mai?> <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" /></label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="product_lang_promotion_<?= $value->lang?>" id="product_lang_promotion" value="<?= $formData[$value->lang]['product_lang_promotion'] ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?=so_luong?> <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" /></label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="product_lang_quality_<?= $value->lang?>" id="product_lang_quality" value="<?= $formData[$value->lang]['product_lang_quality'] ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Made in <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" /></label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="product_lang_madein_<?= $value->lang?>" id="product_lang_madein" value="<?= $formData[$value->lang]['product_lang_madein'] ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?= bao_hanh?> <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" /></label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="product_lang_warranty_<?= $value->lang?>" id="product_lang_warranty" value="<?= $formData[$value->lang]['product_lang_warranty'] ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?= don_vi_tinh?> <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" /></label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="product_lang_unit_<?= $value->lang?>" id="product_lang_unit" value="<?= $formData[$value->lang]['product_lang_unit'] ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><?= chat_luong?> <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" /></label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="product_lang_quantity_<?= $value->lang?>" id="product_lang_quantity" value="<?= $formData[$value->lang]['product_lang_quantity'] ?>"/>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- <div class="col-md-12" style="margin-bottom:30px;">
                                    <?php
                                    if($formData[$value->lang]['product_lang_more'])
                                    {
                                        $i=1;
                                        echo '<div id="sortable">';
                                        foreach ($formData[$value->lang]['product_lang_more'] as $k => $item) {
                                            ?>
                                            <div class="form-inline group<?= $i;?>" >
                                                <label class='w100 move'>Thông số <?= $i;?></label>
                                                <div class="form-group">
                                                    <input type="text"  name="product_lang_more_name[]" value="<?= $k?>" />
                                                </div>
                                                <div class="form-group">
                                                    <input type="text"  name="product_lang_more[]" value="<?= $item?>" />
                                                </div>
                                                <?php
                                                if($i==1)
                                                {
                                                    echo '<button class="btn btn-xs btn-success" id="infoplus"><i class="fa fa-plus"></i></button>';
                                                }else{
                                                    echo '<button class="groupdel btn btn-xs btn-danger" data-id="group'.$i.'" id="infoplus"><i class="fa fa-times"></i></button>';
                                                }
                                                ?>
                                            </div>
                                            <?php
                                            $i++;
                                        }
                                        echo '</div>';
                                    }else{
                                        echo '<div class="form-inline">';
                                            echo '<label class="w100">Thông số 1</label>';
                                            echo '<div class="form-group">';
                                                echo '<input type="text"  name="product_lang_more_name[]"/>';
                                            echo '</div>';
                                            echo '<div class="form-group">';
                                                echo '<input type="text"  name="product_lang_more[]"/>';
                                            echo '</div>';
                                            echo '<button class="btn btn-xs btn-success" id="infoplus"><i class="fa fa-plus"></i></button>';
                                        echo '</div>';
                                    }
                                    ?>
                                    
                                    <div id="infomore" class="form-inline"></div>
                                    <input type="hidden" id="thongsodem" value="<?= count($formData[$value->lang]['product_lang_more'])?>">
                                    <div class="alert alert-danger" style="width:500px; margin-top:20px; ">Click chuột vào text Thông số 1, 2, .... Di chuyển vị trí</div>
                                </div> -->
                                <div class="clr"></div>
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#product_lang_detail" aria-controls="product_lang_detail" role="tab" data-toggle="tab"><?= chi_tiet?></a></li>
                                        <!-- <li role="presentation"><a href="#product_lang_more" aria-controls="product_lang_more" role="tab" data-toggle="tab"><?= thong_tin_them?></a></li>-->
                                        <li role="presentation"><a href="#product_lang_using" aria-controls="product_lang_using" role="tab" data-toggle="tab"><?= huong_dan_su_dung?></a></li>
                                        <li role="presentation"><a href="#product_lang_advantage" aria-controls="product_lang_advantage" role="tab" data-toggle="tab"><?= uu_diem?></a></li>
                                        <li role="presentation"><a href="#product_lang_unadvantage" aria-controls="product_lang_unadvantage" role="tab" data-toggle="tab"><?= nhuoc_diem?></a></li>
                                    </ul>
                                    <div class="form-group">
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="product_lang_detail">
                                                <textarea class="form-control" name="product_lang_detail_<?= $value->lang?>" id="product_lang_detail_<?= $value->lang?>"><?= $formData[$value->lang]['product_lang_detail'] ?></textarea>
                                            </div>
                                            <!-- <div role="tabpanel" class="tab-pane" id="product_lang_more">
                                                <textarea class="form-control" name="product_lang_more_<?= $value->lang?>" id="product_lang_more_<?= $value->lang?>"><?= $formData[$value->lang]['product_lang_more'] ?></textarea>
                                            </div>-->
                                            <div role="tabpanel" class="tab-pane" id="product_lang_using">
                                                <textarea class="form-control" name="product_lang_using_<?= $value->lang?>" id="product_lang_using_<?= $value->lang?>"><?= $formData[$value->lang]['product_lang_using'] ?></textarea>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="product_lang_advantage">
                                                <textarea class="form-control" name="product_lang_advantage_<?= $value->lang?>" id="product_lang_advantage_<?= $value->lang?>"><?= $formData[$value->lang]['product_lang_advantage'] ?></textarea>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="product_lang_unadvantage">
                                                <textarea class="form-control" name="product_lang_unadvantage_<?= $value->lang?>" id="product_lang_unadvantage_<?= $value->lang?>"><?= $formData[$value->lang]['product_lang_advantage'] ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">SEO Title <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" /></label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="product_lang_seo_title_<?= $value->lang?>" id="product_lang_seo_title" value="<?= $formData[$value->lang]['product_lang_seo_title'] ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">SEO Keyword <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" /></label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="product_lang_seo_keyword_<?= $value->lang?>" id="product_lang_seo_keyword" value="<?= $formData[$value->lang]['product_lang_seo_keyword'] ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">SEO Description <img src="<?= base_file.'language/'.$value->picture;?>" width="20px" /></label>
                                        <div class="controls">
                                            <input type="text" class="form-control" name="product_lang_seo_description_<?= $value->lang?>" id="product_lang_seo_description" value="<?= $formData[$value->lang]['product_lang_seo_description'] ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
                </div>
            </div>
        </div>

        <div class="box-footer text-center mt10">
            <button type="submit" class="btn btn-primary" name="fsubmit" value="Save"><i class="fa fa-save"></i> Save</button>
        </div>
        </section>
    </aside>
</form>
<script type="text/javascript">
<?php
    if(!empty($language)){
        foreach ($language as $key => $value) {
            ?>
            CKEDITOR.replace("product_lang_detail_<?= $value->lang?>");
            // CKEDITOR.replace("product_lang_more_<?= $value->lang?>");
            CKEDITOR.replace("product_lang_using_<?= $value->lang?>");
            CKEDITOR.replace("product_lang_advantage_<?= $value->lang?>");
            CKEDITOR.replace("product_lang_unadvantage_<?= $value->lang?>");
            <?php
        }
    }
?>

<?php if($formData['product_id']=="" || $formData['product_id'] == NULL) { ?>
$('.product_lang_name_vn').bind('blur',function(){
    var _val_vn  = $(this).val();
    <?php if(!empty($language)){ foreach ($language as $key => $value) {?>
        var _tmp_name_<?= $value->lang?> = $('.product_lang_name_<?= $value->lang; ?>').val();
        if(_tmp_name_<?= $value->lang?> == '')
        {
            $('.product_lang_name_<?= $value->lang; ?>').val(_val_vn);
        }
    <?php } } ?>
});
$('.product_code_vn').bind('blur',function(){
    var _val_vn  = $(this).val();
    <?php if(!empty($language)){ foreach ($language as $key => $value) {?>
        var _tmp_name_<?= $value->lang?> = $('.product_code_<?= $value->lang; ?>').val();
        if(_tmp_name_<?= $value->lang?> == '')
        {
            $('.product_code_<?= $value->lang; ?>').val(_val_vn);
        }
    <?php } } ?>
});
<?php } ?>
</script>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
 <script>
  $(function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  });
  </script>