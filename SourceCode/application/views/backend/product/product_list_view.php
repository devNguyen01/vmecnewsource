<form accept-charset="utf-8" method="get" enctype="multipart/form-data">
    <aside class="right-side">
        <section class="content-header">
            <h1>
                <small><?= $title ?></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= admin_url?>home/" ><i class="fa fa-dashboard"></i> <?= trang_chu?></a></li>
                <li class="active"><?= $title ?></li>
            </ol>
        </section>

        <section class="content">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="fa fa-cubes"></i>
                    <h3 class="box-title"><?= $title ?></h3>
                    <small class="badge bg-aqua"><?= $record?></small>
                    <div class="pull-right box-tools">
                        <a href="<?= admin_url ?>product/add/" class="btn btn-primary btn-sm" title="<?= them?> <?= san_pham?>">
                            <i class="fa fa-plus"></i>
                            <span><?= them?></span>
                        </a>
                    </div>
                </div>

                <div id="example1_wrapper" class="box-body" role="grid">
                    <div class="row">
                        <div class="form-group col-md-3">
                            <select onchange="this.form.submit()" class="form-control input-sm" name="product_parent" id="product_parent">
                                <option value="">---<?= loc_theo?> <?= danh_muc?>---</option>
                                <?php 
                                    $this->mmenu->dropDownMenu($product_parent,'product'); 
                                ?>
                            </select>
                        </div>
                        <!-- <div class="form-group col-md-2">
                            <select onchange="this.form.submit()" name="thot" class="form-control input-sm">
                                <option value="">-- <?= loai?> --</option>
                                <option <?= $thot == 1 ? "selected":"";?> value="1">Sản phẩm HOT</option>
                            </select>
                        </div> -->
                        <div class="form-group col-md-2">
                            <select onchange="this.form.submit()" name="tstatus" class="form-control input-sm">
                                <option <?= $tstatus == 1 ? "selected":"";?> value="1"><?= hien_thi?></option>
                                <option <?= $tstatus == 0 ? "selected":"";?> value="0"><?= an?></option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" name="fkeyword" class="form-control input-sm" placeholder="<?= tim_kiem?>" value="<?= $fkeyword; ?>">
                        </div>
                        <div class="form-group col-md-2">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> <?= tim_kiem?></button>
                        </div>
                    </div>
                </div>

                <div class="box-body table-responsive  no-padding">
                    <table id="myTable" class="table table-bordered table-striped" style="font-size:12px">
                        <thead>
                        <tr role="row">
                            <th class="text-center">ID</th>
                            <th class="text-center">Mã</th>
                            <th class="text-center">Hình ảnh</th>
                            <th><?= tieu_de?></th>
                            <th><?= danh_muc?></th>
                            <th><?= tac_gia?></th>
                            <th class="text-center"><?= ngay_dang?></th>
                            <th class="text-center" style="width: 110px;"><?= hanh_dong?></th>
                        </tr>
                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                        <?php if($list){
                            foreach($list as $item){
                                $myMenuLang = $this->mmenu_lang->getData('',array("menu_lang_name"=>$lang,"menu_id"=>$item->product_parent));
                                $name_parent = isset($myMenuLang["menu_lang_name"]) ? $myMenuLang["menu_lang_name"]:'';
                                
                                /**begin tac gia*/
                                $author = $this->muser->getOnceAnd(array("id"=>$item->user));
                                $authorName = (isset($author['user_username']) && $author['user_username'])?$author['user_username']:'--';
                                /**end tac gia*/
                                
                                $status = $item->product_status==1?"icon-eye-open":"icon-eye-close";
                                $status_change = $item->product_status==1?0:1;
                                $status_title = $item->product_status==1?"Hiện thị":"Không hiện thị";
                                $opacity = $item->product_status == 1?"":"opcity02";

                                $product_name = $item->product_lang_name;
                                /**picture*/
                                $picture = base_file.'product/'.$item->product_picture;
                                /**begin trang thai*/
                                $view = $item->product_view;
                                $link_edit = admin_url.'product/update/'.$item->id.'/?redirect='.base64_encode(curPageURL());
                                $link_delete = admin_url.'product/delete/'.$item->id.'/?redirect='.base64_encode(curPageURL());
                                $link_status = admin_url.'product/status/'.$item->id.'/'.$status_change.'/?redirect='.base64_encode(curPageURL());
                                ?>
                                <tr>
                                    <td class="text-center"><?= $item->id?></td>
                                    <td class="text-center"><?= $item->product_code?></td>
                                    <td  class="text-center">
                                        <?php if($item->product_picture) { ?>
                                        <a href="<?= $picture?>" class="image zoom_img tip-bottom cboxElement" title="Click vào xem ảnh lớn">
                                            <img src="<?= $picture?>" alt="<?= $product_name?>" title="<?= $product_name?>" class="img-circle">
                                        </a>
                                        <?php } ?>
                                    </td>
                                    <td><a><?= $product_name?></a> <label class="label label-success"><?= $view; ?></label></td>
                                    <td><?= $name_parent?></td>
                                    
                                    
                                    <td><i><?= $authorName?></i></td>
                                    <td class="text-center"><?= date("d/m/Y",$item->product_create_date)?></td>
                                    <td class="text-center">
                                        <a href="<?= $link_edit?>" class="btn btn-xs btn-primary" title="<?= sua?> [<?= $product_name?>]"><i class="fa fa-edit"></i></a>
                                        <a href="<?= $link_delete?>" onclick="return Delete();" class="btn btn-xs btn-danger" title="<?= bo_vao_thung_rac?>  [<?= $product_name?>]"><i class="fa fa-trash-o"></i></a>
                                        <a href="<?= $link_status?>" class="btn btn-xs btn-warning" title="<?= $status_title?>"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                            <?php
                            }
                        }?>
                        </tbody>
                    </table>
                </div>

                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <?php if(isset($pagination)){echo $pagination;};?>
                        <li class="disabled"><a><?= tat_ca?>: <?= $record?> <?= san_pham?></a></li>
                    </ul>
                </div>
            </div>
        </section>
    </aside>
</form>