<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Tinh
 * Date: 11/27/13
 * Time: 4:35 PM
 * To change this template use File | Settings | File Templates.
 */

class mcompany extends CI_Model{
    protected $table = "tkwp_company";
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    /**lay danh sach theo dieu kien AND*/
    public function getListAND($and="",$orderby=""){
        if($and){
            $this->db->where($and);
        }
        if($orderby){
            $this->db->order_by($orderby);
        }
        $query = $this->db->get($this->table);
        return $query->result_array();
    }
    /**lay danh sach theo dieu kien AND*/

    /**begin lay 1 dong*/
    public function getOnceAnd($object="",$and=""){
        if($object){
            $this->db->select($object);
        }
        if($and){
            $this->db->where($and);
        }
        $rs = $this->db->get($this->table);
        return $rs->row_array();
    }
    /**end lay 1 dong*/

    /**begin dem theo dieu kien end*/
    public function countAnd($and=""){
        if($and!=""){
            $this->db->where($and);
        }
        $query = $this->db->get($this->table);
        $count = $query->num_rows();
        return $count;
    }
    /**end dem theo dieu kien end*/
}
?>