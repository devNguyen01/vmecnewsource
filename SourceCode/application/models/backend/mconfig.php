<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Tinh
 * Date: 11/22/13
 * Time: 3:12 PM
 * To change this template use File | Settings | File Templates.
 */
class mconfig extends CI_Model{
    private $table = "tkwp_config";
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    /**begin update*/
    public function update($id,$data){
        $this->db->where("id",$id);
        $this->db->update($this->table,$data);
        return true;
    }
    /**end update*/

    /**begin lay 1 dong co dieu kien*/
    public function getOnceAnd($and=""){
        if($and) {
            $this->db->where($and);
        }
        $rs = $this->db->get($this->table);
        return $rs->row_array();
    }
    /**end lay 1 dong co dieu kien*/

    /**begin upload file*/
    public function do_upload(){
        $path = dir_root. '/public/frontend/images/'.date("Ymd");
        if(!is_dir($path))
        {
            mkdir($path,DIR_WRITE_MODE);
            chmod($path, DIR_WRITE_MODE);
        } 
        $config = array('upload_path'   => $path,
                        'allowed_types' => 'gif|jpg|png',
                        'max_size'      => '2000');
        $this->load->library("upload",$config);
        if(!$this->upload->do_upload("config_background")){
            $error = array($this->upload->display_errors());
            var_dump($error);
        }else{
            $image_data = $this->upload->data();
            return $image_data; 
        }
    }
    /**end upload file*/

}