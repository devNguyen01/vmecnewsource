<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Tinh
 * Date: 11/26/13
 * Time: 9:11 AM
 * To change this template use File | Settings | File Templates.
 */
class mcompany_contact extends CI_Model{
    protected $table = "tkwp_company_contact";
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    /**lay danh sach theo dieu kien AND*/
    public function getListAND($and="",$orderby=""){
        if($and){
            $this->db->where($and);
        }
        if($orderby){
            $this->db->order_by($orderby);
        }
        $query = $this->db->get($this->table);
        return $query->result_array();
    }
    /**lay danh sach theo dieu kien AND*/

    /**begin lay 1 dong*/
    public function getOnceAnd($and=""){
        if($and){
            $this->db->where($and);
        }
        $this->db->order_by("company_id","desc");
        $rs = $this->db->get($this->table);
        return $rs->row_array();
    }
    /**end lay 1 dong*/


    /**begin dem theo dieu kien end*/
    public function countAnd($and=""){
        if($and!=""){
            $this->db->where($and);
        }
        $query = $this->db->get($this->table);
        $count = $query->num_rows();
        return $count;
    }
    /**end dem theo dieu kien end*/


    /**begin them moi*/
    public function addData($data){
        $this->db->insert($this->table,$data);
    }
    /**end them moi*/

    /**begin them moi*/
    public function updateData($id,$data,$lang='vn'){
        $this->db->where(array("company_id"=>$id,"company_contact_lang"=>$lang));
        $this->db->update($this->table,$data);
    }
    /**end them moi*/

    /***begin xoa*/
    public function deleteData($id){
        if(is_numeric($id)){
            $this->db->where("company_id",$id);
        }elseif(is_array($id)){
            $this->db->where_in($id);
        }
        $this->db->delete($this->table);
    }
    /**end xoa*/
}