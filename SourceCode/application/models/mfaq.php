<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Tinh
 * Date: 11/29/13
 * Time: 11:04 AM
 * To change this template use File | Settings | File Templates.
 */
class mfaq extends MY_Model{
    public $table = "tkwp_faq";
    public function __construct(){
        parent::__construct();
        $this->load->Model("mfaq_reply");
    }
    public function getFAQ($object = '', $condition = '', $order_by = 'n.id desc', $limit = '')
    {
        if($object){
            $sql = 'select '.$object.' ';
        }else{
            $sql = 'select * ';
        }

        $sql .= 'from '.$this->table.' n ';
        $sql .= ' where faq_status = 1 ';
        if($condition){
            $sql .= ' and '.$condition;
        }
        $sql .= ' GROUP BY n.id ';

        if($order_by){
            $sql .= ' order by '.$order_by;
        }

        if($limit){
            $sql .= ' limit '.$limit;
        }

        $query = $this->db->query($sql);
        if(!empty($limit) && $limit=="1")
        {
            return $query->row_array();
        }
        else
        {
            return $query->result_array();
        }
    }
}