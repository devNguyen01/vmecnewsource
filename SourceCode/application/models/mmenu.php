<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Tinh
 * Date: 11/27/13
 * Time: 10:20 AM
 * To change this template use File | Settings | File Templates.
 */

class mmenu extends MY_Model{
    protected $table = "tkwp_menu";
    protected $table_lang = "tkwp_menu_lang";
    public function __construct(){
        parent::__construct();
        $this->load->Model("mmenu_lang");
    }
    public function getMenus($object = '', $condition = '', $order_by = 'n.id desc', $limit = '')
    {
        if($object){
            $sql = 'select '.$object.' ';
        }else{
            $sql = 'select * ';
        }

        $sql .= 'from '.$this->table.' n ';
        $sql .= 'inner join '.$this->table_lang.' nl on n.id = nl.menu_id';
        
        $sql .= ' where n.menu_status = 1 ';
        if($condition){
            $sql .= ' and '.$condition;
        }
        $sql .= ' GROUP BY n.id ';

        if($order_by){
            $sql .= ' order by '.$order_by;
        }

        if($limit){
            $sql .= ' limit '.$limit;
        }

        $query = $this->db->query($sql);
        if(!empty($limit) && $limit=="1")
        {
            return $query->row_object();
        }
        else
        {
            return $query->result_object();
        }
    }
    public function countData($condition='')
    {
        $data = $this->getMenus('n.id',$condition);
        return count($data);
    }
    public function getList($object="",$and="",$orderby=""){
        if($object){
            $this->db->select($object);
        }
        if($and){
            $this->db->where($and);
        }
        if($orderby){
            $this->db->order_by($orderby);
        }
        $query = $this->db->get($this->table);
        return $query->result_array();
    }
    /**begin lay 1 dong co dieu kien*/
    public function getOnceAnd($object="",$and=""){
        if($object){
            $this->db->select($object);
        }
        if($and){
            $this->db->where($and);
        }
        $this->db->order_by("id","desc");
        $rs = $this->db->get($this->table);
        return $rs->row_array();
    }
    /**end lay 1 dong co dieu kien*/

     /**begin danh sach*/
    public function getOnceSql($object="",$join="",$and=""){
        if($object){
            $sql = 'select '.$object.' ';
        }else{
            $sql = 'select * ';
        }

        $sql .= 'from '.$this->table.' m ';
        if($join){
            $sql .= $join;
        }
        if($and){
            $sql .= ' where m.menu_status = 1 '.$and;
        }
    
        $sql .= ' order by m.id desc';  
           
        $query = $this->db->query($sql);
        return $query->row_array();

    }
    /**end danh sach*/

    public function getInfoAliasTmp($menu_alias)
    {        
        $data = '';
        if($menu_alias)
        {
            $sql = 'select m.id';
            $sql .= ' from '.$this->table.' m ';
            $sql .= ' inner join tkwp_menu_lang ml on ml.menu_id = m.id ';
            $sql .= ' and menu_lang_alias="'.$menu_alias.'"';
            $query = $this->db->query($sql);
            $rs = $query->result_array();
            if(!empty($rs))
            {
                $data = $rs[0]['id'];
            }
        }
        return $data;
    }

    /**begin getInfoAlias*/
    public function getInfoAlias($menu_alias,$lang='vn')
    {        
        $data = array();
        if($menu_alias)
        {
            $object = 'm.id,m.menu_parent,m.menu_com,m.menu_view,m. menu_picture,ml.menu_lang_name as menu_name,ml.menu_lang_alias as menu_alias,ml.menu_lang_detail as menu_detail,m.menu_hdid';
            $join = ' inner join tkwp_menu_lang ml on ml.menu_id = m.id ';
            $and = 'and menu_lang="'.$lang.'" and menu_lang_alias="'.$menu_alias.'"';
            $data = $this->getOnceSql($object,$join,$and);
        }
        return $data;
    }
    /**end getInfoAlias*/

    /**begin getInfoAlias*/
    public function getInfoID($id,$lang='vn')
    {        
        $data = array();
        if($id)
        {
            $object = 'm.id,m.menu_parent,m.menu_com,m.menu_view,m.menu_picture,ml.menu_lang_name as menu_name,ml.menu_lang_alias as menu_alias,ml.menu_lang_detail as menu_detail,m.menu_hdid';
            $join = ' inner join tkwp_menu_lang ml on ml.menu_id = m.id ';
            $and = 'and menu_lang="'.$lang.'" and m.id="'.$id.'"';
            $data = $this->getOnceSql($object,$join,$and);
        }
        return $data;
    }
    /**end getInfoAlias*/

    /**lay menu alias
    @ khi count=="TRUE" lay tat ca menu id cap con    
    */
    public function getIdParent($menu_alias='',$lang='vn')
    {        
        $menu_parent = '';
        if($menu_alias)
        {
            $object_m="m.menu_parent";
            $join_m="inner join tkwp_menu_lang ml on m.id = ml.menu_id";
            $and_m=" and ml.menu_lang_alias = '".$menu_alias."' and menu_lang='".$lang."'";
            $tmpList = $this->getOnceSql($object_m,$join_m,$and_m);
            if($tmpList){
                $menu_parent = $tmpList['menu_parent'];
            }

        }
        return $menu_parent;
    }
    /**lay menu alias
    @ khi count=="TRUE" lay tat ca menu id cap con
    @ khi count = FALSE lay id theo alias bai viet
    */
    public function getIDAnd($menu_alias='',$count=true,$lang='vn')
    {        
        $id = '';
        if($menu_alias)
        {            
            $tmpList = $this->mmenu_lang->getData("menu_id as id",array("menu_lang"=>$lang,"menu_lang_alias"=>$menu_alias),"id desc");            
            if($tmpList)
            {
                $id = $tmpList['id'].',';
                if($count==true)
                {
                    $object_menu = 'm.id';
                    $join_menu = '';
                    $and_menu = 'menu_parent='.$tmpList['id'];
                    $orderby_menu = 'm.id desc';
                    $limit_menu = '';
                    $getViewMenu = $this->getQuerySql($object_menu,$join_menu,$and_menu,$orderby_menu,$limit_menu);
                    if($getViewMenu)
                    {
                        foreach ($getViewMenu as $key => $value) {
                            $id .= $value['id'].',';
                        }
                    }
                }
                $id = rtrim($id,",");
            }

        }
        return $id;
    }

    /**begin danh sach tour*/
    public function getQuerySql($object="",$join="",$and="",$orderby="",$limit=""){
        if($object){
            $sql = 'select '.$object.' ';
        }else{
            $sql = 'select * ';
        }

        $sql .= 'from '.$this->table.' m ';
        if($join){
            $sql .= $join;
        }
        if($and){
            $sql .= ' where '.$and;
        }

        if($orderby){
            $sql .= ' order by '.$orderby;
        }

        if($limit){
            $sql .= ' limit '.$limit;
        }
        $query = $this->db->query($sql);
        return $query->result_array();

    }
    /**end danh sach tour*/

    /**begin dem theo query sql*/
    public function countQuery($join="",$and=""){
        $sql = 'select * from '.$this->table.' m' ;
        if($join){
            $sql .= $join;
        }
        $sql .= ' where 1 ';
        if($and){
            $sql .= ' and '.$and;
        }
        $query = $this->db->query($sql);
        $count = $query->num_rows();
        return $count;
    }
    /**end dem theo query sql*/


    /**begin check com*/
    public function checkView($menu_alias,$lang='vn')
    {        
        $view = '';        
        $object_menu = 'm.menu_view';
        $join_menu = 'inner join tkwp_menu_lang ml on ml.menu_id = m.id';
        $and_menu = 'ml.menu_lang="'.$lang.'" and menu_lang_alias="'.$menu_alias.'"';
        $orderby_menu = 'm.id desc';
        $limit_menu = '0,1';
        $getViewMenu = $this->getQuerySql($object_menu,$join_menu,$and_menu,$orderby_menu,$limit_menu);
        $view = isset($getViewMenu[0]['menu_view']) && $getViewMenu[0]['menu_view'] ? $getViewMenu[0]['menu_view']:'';
        return $view; 
    }
    /**end check com*/

    /**begin menu top*/
    public function  getMenu($menu_parent="",$lang='vn',$orderby = "menu_orderby asc"){ 
        $menu_parent = $menu_parent > 0 ? $menu_parent:1;      
        $and = array("menu_parent"=>$menu_parent,"menu_status"=>1);
        $list = $this->getList("id,menu_com,menu_hdid,menu_picture,menu_view",$and,$orderby);
        $data = array();
        if($list){
            foreach($list as $key => $item){
                $getLang = $this->mmenu_lang->getData(array("menu_lang_alias","menu_lang_name","menu_lang_detail"),array("menu_lang"=>$lang,"menu_id"=>$item["id"]));
                $data[$key]["id"] = $item["id"];
                $data[$key]["menu_com"] = $item["menu_com"];
                $data[$key]["menu_view"] = $item["menu_view"];
                $data[$key]["menu_hdid"] = $item["menu_hdid"];
                $data[$key]["menu_picture"] = $item["menu_picture"];
                if($getLang){
                    $data[$key]["menu_detail"] = strip_tags($getLang["menu_lang_detail"]);
                    $data[$key]["menu_name"] = $getLang["menu_lang_name"];
                    $data[$key]["menu_alias"] = $getLang["menu_lang_alias"];
                }else{
                    $data[$key]["menu_name"] = "";
                }
            }
        }
        return $data;
    }
    /**end menu top*/

    /**begin menu home*/
    public function  getHome($menu_parent='', $lang='vn'){
        $and = array("menu_home"=>1,"menu_status"=>1);
        if(!empty($menu_parent))
        {
            $and = array("menu_home"=>1,"menu_status"=>1,'menu_parent'=>$menu_parent);
        }
        $orderby = "menu_orderby asc";
        $list = $this->getList("",$and,$orderby);
        $data = array();
        if($list){
            foreach($list as $key => $item){
                $getLang = $this->mmenu_lang->getData(array("menu_lang_alias","menu_lang_name","menu_lang_detail"),array("menu_lang"=>$lang,"menu_id"=>$item["id"]));
                $data[$key]["id"] = $item["id"];                
                $data[$key]["menu_com"] = $item["menu_com"];
                $data[$key]["menu_picture"] = $item["menu_picture"];
                if($getLang){
                    $data[$key]["menu_detail"] = strip_tags($getLang["menu_lang_detail"]);
                    $data[$key]["menu_name"] = $getLang["menu_lang_name"];
                    $data[$key]["menu_alias"] = $getLang["menu_lang_alias"];
                }else{
                    $data[$key]["menu_name"] = "";
                }
            }
        }
        return $data;
    }
    /**end menu home*/

    /**begin menu hot*/
    public function  getHot($menu_parent='', $lang='vn'){
        $and = array("menu_hot"=>1,"menu_status"=>1);
        if(!empty($menu_parent))
        {
            $and = array("menu_hot"=>1,"menu_status"=>1,'menu_parent'=>$menu_parent);
        }
        $orderby = "menu_orderby asc";
        $list = $this->getList("",$and,$orderby);
        $data = array();
        if($list){
            foreach($list as $key => $item){
                $getLang = $this->mmenu_lang->getData(array("menu_lang_alias","menu_lang_name","menu_lang_detail"),array("menu_lang"=>$lang,"menu_id"=>$item["id"]));
                $data[$key]["id"] = $item["id"];                
                $data[$key]["menu_com"] = $item["menu_com"];
                $data[$key]["menu_picture"] = $item["menu_picture"];
                if($getLang){
                    $data[$key]["menu_detail"] = strip_tags($getLang["menu_lang_detail"]);
                    $data[$key]["menu_name"] = $getLang["menu_lang_name"];
                    $data[$key]["menu_alias"] = $getLang["menu_lang_alias"];
                }else{
                    $data[$key]["menu_name"] = "";
                }
            }
        }
        return $data;
    }
    /**end menu hot*/

    public function getAllID($menu_id, &$arr = array())
    {
        if(empty($arr)){
            array_push($arr, $menu_id);
        }
        if(!empty($menu_id))
        {
            $data = $this->getList('id',array('menu_parent'=>$menu_id));
            if(!empty($data))
            {
                foreach ($data as $key => $value) {
                    array_push($arr, $value['id']);
                    $this->getAllID($value['id'],$arr);
                }
            }
        }
        return $arr;
    }
    public function getAllParent($menu_id, &$arr = array())
    {
        if(empty($arr)){
            array_push($arr, $menu_id);
        }
        if(!empty($menu_id))
        {
            $data = $this->getList('menu_parent',array('id'=>$menu_id));
            if(!empty($data))
            {
                foreach ($data as $key => $value) {
                    array_push($arr, $value['menu_parent']);
                    $this->getAllParent($value['menu_parent'],$arr);
                }
            }
        }
        return $arr;
    }


    /**begin trinh bay danh muc menu*/
    public function dropDownMenu( $active='' , $com='', $menu_parent = 0, $lang='vn',$caret = ''){
        /**begin trinh bay danh sach menu cap 1*/
        $and = array("menu_parent"=>$menu_parent);
        $data = $this->getList('',$and);
        if($data){
            foreach($data as $item){
                $myLangCate = $this->mmenu_lang->getData('menu_lang_name',array("menu_lang"=>$lang,"menu_id"=>$item["id"]));
                $seleted = $item["id"]==$active?"selected":"";
                if($com == 'all'){
                    $disable = '';
                }
                else
                {
                    $disable = 'disabled';
                    if(!empty($com) && $item['menu_com'] == $com)
                    {
                        $disable = '';
                    }
                }
                echo '<option '.$disable.' '.$seleted.' value="'.$item["id"].'">'.$caret.$myLangCate["menu_lang_name"].'</option>';
                $this->dropDownMenu($active,$com,$item['id'],$lang='vn',$caret.'-- ');
            }
        }
        /**end trinh bay danh sach menu cap 1*/
    }
    /**end trinh bay danh muc menu*/

    /**begin trinh bay danh muc menu*/
    public function showMenu($menu_parent = 0, $lang='vn',$caret = ''){
        /**begin trinh bay danh sach menu cap 1*/
        $and = array("menu_parent"=>$menu_parent);
        $data = $this->getList('',$and);
        if($data){
            foreach($data as $item){
                $myLangCate = $this->mmenu_lang->getData('menu_lang_name,menu_lang_alias',array("menu_lang"=>$lang,"menu_id"=>$item["id"]));
                $picture = $item["menu_picture"]!=""?base_file.'menu/'.$item["menu_picture"]:admin_img.'no_image.png';
                $status_change = $item["menu_status"] == 1 ? 0 : 1;
                $opacity = $item["menu_status"] == 1?"":"opcity02";
                $menu_com = $item["menu_com"];
                $menu_view = $item["menu_view"]  ? '<code>'.$item["menu_view"].'</code>':'';
                echo '<tr class="'.$opacity.'">';
                    echo '<td>'.$item['id'].'</td>';
                    echo '<td class="cate-c1">';
                        echo '<a class="title_sum">';
                            echo '<span>'.$caret.strip_tags($myLangCate["menu_lang_name"]).'</span>';
                        echo '</a>';
                    echo '</td>';
                    echo '<td>';
                        echo '<span>'.$menu_com.' '.$menu_view.'</span>';
                        echo '<code>'.$myLangCate["menu_lang_alias"].'</code>';
                    echo '</td>';
                    echo '<td class="text-center" width="200px"><a href="'.$picture.'" class="image zoom_img tip-bottom"><img src="'.$picture.'" class="img-circle"></a></td>';
                    echo '<td class="text-center" width="200px">';
                        if($item['menu_parent'] != 0) { 
                        echo '<span>';
                            echo '<a href="'.admin_url.'menu/update/'.$item["id"].'/?redirect='.base64_encode(curPageURL()).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a> ';
                            echo '<a href="'.admin_url.'menu/trash/'.$item["id"].'/?redirect='.base64_encode(curPageURL()).'" class="btn btn-xs btn-danger" onclick="return Delete();"><i class="fa fa-trash-o"></i></a> ';
                            echo '<a href="'.admin_url.'menu/update_status/'.$item["id"].'/'.$status_change.'/?redirect='.base64_encode(curPageURL()).'" class="btn btn-xs btn-warning"><i class="fa fa-eye"></i></a> ';
                        echo '</span>';
                        }
                    echo '</td>';
                echo '</tr>';
                $this->showMenu($item['id'],$lang='vn',$caret.'<i class="fa fa-angle-double-right"></i> ');
            }
        }
        /**end trinh bay danh sach menu cap 1*/
    }
    /**end trinh bay danh muc menu*/

    public function menu_left($lang)
    {
        $html = '';
        $this->load->Model("mcategory");
        $s_info = $this->session->userdata('userInfo');
        if(!isset($s_permission)){
            $permission = array();
            if(isset($s_info['s_user_group']) && !is_array($s_info['s_user_group'])){
                $tmp_group = $this->mgroupaction->getQuery("gc_value",$join="","id in (".$s_info['s_user_group'].")","id asc",$limit="");
                if($tmp_group)
                {
                    foreach ($tmp_group as $key => $value) {
                        $permission[] = $value['gc_value'];
                    }
                    $this->session->set_userdata('s_permission',$permission);
                }
            }
        }      
        $s_permission = $this->session->userdata('s_permission');
        $object_cate = 'id,category_component,category_action,category_icon';
        $menu_list = $this->mcategory->getArray($object_cate,array("category_parent" => 0, "category_status" => 1), "category_orderby asc");
        if ($menu_list) {
            $html .= '<ul class="sidebar-menu">';
            foreach ($menu_list as $item) {
                $tmpAction = $item["category_component"].'_'.$item['category_action'];
                if(($s_info['s_user_id']==0 ) || ($s_permission && in_array($tmpAction, $s_permission)) ){
                    /**begin lay danh sach cap 2*/
                    $and_c2 = array("category_parent" => $item["id"], "category_status" => 1);
                    $get_cate_c2 = $this->mcategory->getArray($object_cate, $and_c2, "category_orderby asc");
                    $record_cate_c2 = $this->mcategory->countAnd($and_c2);
                    /**end lay danh sach cap2*/

                    $get_onceLang = $this->mcategory_lang->getData('category_lang_name',array("category_id" => $item["id"], "category_lang" => $lang ));
                    if ($get_onceLang) {
                        $menu_name = $get_onceLang["category_lang_name"];
                    } else {
                        $menu_name = $item["category_component"];
                    }
                    if ($record_cate_c2 == 0) {
                        $link = 'href="' . admin_url . $item["category_component"] . '/'.$item['category_action'].'"';
                        $submenu = "";
                    } else {
                        $link = "";
                        $submenu = "treeview";
                    }
                    $active = $this->uri->segment(2) == $item["category_component"] ? " active" : "";
                    $disblock = $this->uri->segment(2) == $item["category_component"] ? " treeview-menu" : "";
                    $icon_m = (isset($item["category_icon"]) && $item["category_icon"]) ? $item["category_icon"] : "fa fa-angle-double-right";

                    if (trim($menu_name) == "-") {
                        $html .= '<div style="height: 1px; background: #3c8dbc"></div> ';
                    } else {
                        $html .= '<li class="' . $submenu . $active . '">';
                        if ($get_cate_c2) {
                            $html .= '<a ><i class="' . $icon_m . '"></i> <span>' . $menu_name . '</span>  <i class="fa fa-angle-left pull-right"></i></a>';
                            $html .= '<ul class="' . $disblock . ' treeview-menu">';
                            foreach ($get_cate_c2 as $item_c2) {
                                $get_onceLang_c2 = $this->mcategory_lang->getData('category_lang_name',array("category_id" => $item_c2["id"], "category_lang" => $lang ));
                                if ($get_onceLang_c2) {
                                    $menu_name_c2 = $get_onceLang_c2["category_lang_name"];
                                } else {
                                    $menu_name_c2 = $item_c2["category_component"];
                                }
                                $link_c2 = admin_url . $item_c2["category_component"] . '/' . $item_c2["category_action"] . '/';
                                $active_sub = $this->uri->segment(3) == $item_c2["category_action"] ? " active" : "";
                                $icon_m2 = (isset($item_c2["category_icon"]) && $item_c2["category_icon"]) ? $item_c2["category_icon"] : "fa fa-angle-double-right";
                                $tmpAction_c2 = $item_c2["category_component"].'_'.$item_c2['category_action'];                    
                                if(($s_info['s_user_id']==0 ) || ($s_permission && in_array($tmpAction_c2,$s_permission))){
                                    $html .= '<li class="' . $active_sub . '"><a  href="' . $link_c2 . '"><i class="icon ' . $icon_m2 . '"></i> ' . $menu_name_c2 . '</a></li>';
                                }
                            }
                            $html .= '</ul>';
                        } else {
                            $html .= '<a ' . $link . '"><i class="' . $icon_m . '" title=""></i> <span>' . $menu_name . '</span></a>';
                        }
                        $html .= '</li>';
                    }
                }
            }
            $html .= '</ul>';
        }
        return $html;
    }

    public function getTitle($id,$lang)
    {
        $data = $this->mmenu_lang->getData('menu_lang_name',array('menu_lang'=>$lang,'menu_id'=>$id));
        if(!empty($data))
            return $data['menu_lang_name'];
        return NULL;
    }
    public function getHiddenID($id,$lang)
    {
        $data = $this->mmenu->getData('menu_hdid,menu_parent',array('id'=>$id));
        if(empty($data['menu_hdid']))
        {
            $data = $this->mmenu->getData('menu_hdid,menu_parent',array('id'=>$data['menu_parent']));
        }
        if(!empty($data))
            return $data['menu_hdid'];
        return NULL;
    }

    public function menuSpecial($lang,$menu_id_active='')
    {
        $this->load->Model("mnews");
        $html = ' <div class="submenu ">';
        $html .= '<ul class="list-unstyled">';
        $getListIDActive = $this->getAllParent($menu_id_active);
        // p($getListIDActive);
        /*Quality In Motion*/
        $menu1 = 54;
        $menuInfo1 = $this->mmenu->getInfoID($menu1,$lang);
        $listMenu1 = $this->mmenu->getMenu($menu1,$lang);
        if(!empty($menuInfo1))
        {
            $html .='<li class="bold">';
                $html .='<a class="arr_sqg" title="'.strip_tags($menuInfo1['menu_name']).'" href="'.base_url().$menuInfo1['menu_alias'].'.html">'.strip_tags($menuInfo1['menu_name']).'</a>';
                if(!empty($listMenu1)){
                    $block = in_array($menu1, $getListIDActive) ? 'style="display:block"':'';
                    $html .='<i class="indicator fa fa-angle-right"  title="Click Menu Cấp con"></i>';
                    $html .='<ul class="list-unstyled ulsub" '.$block.'>';
                    foreach ($listMenu1 as $key => $value) {
                        $link1 = base_url().$value['menu_alias'].'.html#id'.$value['id'];
                        $active = $value['menu_alias'] == str_replace('.html', '', $this->uri->segment(1)) ? 'active':'';
                        $html .='<li class="'.$active.'">';
                            $html .='<a href="'.$link1.'" title="'.$value['menu_name'].'" class="arr">'.$value['menu_name'].'</a>';
                        $html .='</li>';
                    }
                    $html .='</ul>';
                }
            $html .='</li>';
        }
        /*Eco Changes*/
        $menu2 = 55;
        $menuInfo2 = $this->mmenu->getInfoID($menu2,$lang);
        $listMenu2 = $this->mmenu->getMenu($menu2,$lang);
        if(!empty($menuInfo2))
        {
            $html .='<li class="bold">';
                $html .='<a class="arr_sqg" title="'.strip_tags($menuInfo2['menu_name']).'" href="'.base_url().$menuInfo2['menu_alias'].'.html">'.strip_tags($menuInfo2['menu_name']).'</a>';
                if(!empty($listMenu2)){
                    $block = in_array($menu2, $getListIDActive) ? 'style="display:block"':'';
                    $html .='<i class="indicator fa fa-angle-right"  title="Click Menu Cấp con"></i>';
                    $html .='<ul class="list-unstyled ulsub" '.$block.'>';
                    foreach ($listMenu2 as $key => $value) {
                        $link2 = base_url().$value['menu_alias'].'.html#id'.$value['id'];
                        $active = $value['menu_alias'] == str_replace('.html', '', $this->uri->segment(1)) ? 'active':'';
                        $html .='<li class="'.$active.'">';
                            $html .='<a href="'.$link2.'" title="'.$value['menu_name'].'" class="arr">'.$value['menu_name'].'</a>';
                        $html .='</li>';
                    }
                    $html .='</ul>';
                }
            $html .='</li>';
        }
        /* Maintenance*/
        $menu3 = 56;
        $menuInfo3 = $this->mmenu->getInfoID($menu3,$lang);
        $object_news3 = 'n.id,n.news_parent';
        $object_news3 .= ',nl.news_lang_name,nl.news_lang_alias';
        $condition_news3 =  ' news_status = 1 and  nl.news_lang = "'.$lang.'" and news_parent = '.$menu3;
        $listNews3 = $this->mnews->getNews($object_news3, $condition_news3, 'n.news_orderby ASC, n.id desc');
        if(!empty($menuInfo3))
        {
            $html .='<li class="bold">';
                $html .='<a class="arr_sqg" title="'.strip_tags($menuInfo3['menu_name']).'" href="'.base_url().$menuInfo3['menu_alias'].'.html">'.strip_tags($menuInfo3['menu_name']).'</a>';
                if(!empty($listNews3)){
                    $block = in_array($menu3, $getListIDActive) ? 'style="display:block"':'';
                    $html .='<i class="indicator fa fa-angle-right"  title="Click Menu Cấp con"></i>';
                    $html .='<ul class="list-unstyled ulsub" '.$block.'>';
                    foreach ($listNews3 as $key => $value) {
                        $link3 = base_url().$menuInfo3['menu_alias'].'/'.$value->news_lang_alias.'-news'.$value->id.'.html';
                        $active = $value->news_lang_alias == str_replace('-news'.$value->id.'.html', '', $this->uri->segment(2)) ? 'active':'';
                        $html .='<li class="'.$active.'">';
                            $html .='<a href="'.$link3.'" title="'.$value->news_lang_name.'" class="arr">'.$value->news_lang_name.'</a>';
                        $html .='</li>';
                    }
                    $html .='</ul>';
                }
            $html .='</li>';
        }
        /*Social Care*/
        $menu4 = 57;
        $menuInfo4 = $this->mmenu->getInfoID($menu4,$lang);
        $listMenu4 = $this->mmenu->getMenu($menu4,$lang);
        if(!empty($menuInfo4))
        {
            $html .='<li class="bold">';
                $html .='<a class="arr_sqg" title="'.strip_tags($menuInfo4['menu_name']).'" href="'.base_url().$menuInfo4['menu_alias'].'.html">'.strip_tags($menuInfo4['menu_name']).'</a>';
                if(!empty($listMenu4)){
                    $block = in_array($menu4, $getListIDActive) ? 'style="display:block"':'';
                    $html .='<i class="indicator fa fa-angle-right"  title="Click Menu Cấp con"></i>';
                    $html .='<ul class="list-unstyled ulsub" '.$block.'>';
                    foreach ($listMenu4 as $key => $value) {
                        $listMenuC4 = $this->mmenu->getMenu($value['id'],$lang);
                        $link4 = base_url().$value['menu_alias'].'.html';
                        $active = $value['menu_alias'] == str_replace('.html', '', $this->uri->segment(1)) ? 'active':'';
                        $html .='<li class="'.$active.'">';
                            $html .='<a href="'.$link4.'" title="'.$value['menu_name'].'" class="arr">'.$value['menu_name'].'</a>';
                            if(!empty($listMenuC4))
                            {
                                $html .= '<ul class="list-unstyled ulsub2">';
                                foreach ($listMenuC4 as $keyc4 => $valuec4) {
                                    $linkc4 = base_url().$valuec4['menu_alias'].'.html';
                                    $active = $valuec4['menu_alias'] == str_replace('.html', '', $this->uri->segment(1)) ? 'active':'';
                                    $html .='<li class="'.$active.'">';
                                        $html .='<a href="'.$linkc4.'" title="'.$valuec4['menu_name'].'" class="arr">'.$valuec4['menu_name'].'</a>';
                                    $html .='</li>';
                                }
                                $html .= '</ul>';
                            }
                        $html .='</li>';
                    }
                    $html .='</ul>';
                }
            $html .='</li>';
        }
        /*Innovation*/
        $menu5 = 58;
        $menuInfo5 = $this->mmenu->getInfoID($menu5,$lang);
        $listMenu5 = $this->mmenu->getMenu($menu5,$lang);
        if(!empty($menuInfo5))
        {
            $html .='<li class="bold">';
                $html .='<a class="arr_sqg" title="'.strip_tags($menuInfo5['menu_name']).'" href="'.base_url().$menuInfo5['menu_alias'].'.html">'.strip_tags($menuInfo5['menu_name']).'</a>';
                if(!empty($listMenu5)){
                    $block = in_array($menu5, $getListIDActive) ? 'style="display:block"':'';
                    $html .='<i class="indicator fa fa-angle-right"  title="Click Menu Cấp con"></i>';
                    $html .='<ul class="list-unstyled ulsub" '.$block.'>';
                    foreach ($listMenu5 as $key => $value) {
                        $link5 = base_url().$value['menu_alias'].'.html#id'.$value['id'];
                        $active = $value['menu_alias'] == str_replace('.html', '', $this->uri->segment(1)) ? 'active':'';
                        $html .='<li class="'.$active.'">';
                            $html .='<a href="'.$link5.'" title="'.$value['menu_name'].'" class="arr">'.$value['menu_name'].'</a>';
                        $html .='</li>';
                    }
                    $html .='</ul>';
                }
            $html .='</li>';
        }
        $html .= '</ul>';
        $html .= '</div>';
        return $html;
    }
}