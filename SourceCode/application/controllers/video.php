<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Tinh
 * Date: 11/8/13
 * Time: 11:02 AM
 * To change this template use File | Settings | File Templates.
 */
class video extends  MY_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->Model("frontend/mvideo");
    }

    public function index($menu_alias){
        $menuTmp = $this->mmenu->getInfoAliasTmp($menu_alias);
        $this->_data['menuInfo'] = $this->mmenu->getInfoID($menuTmp,$this->_data['lang']);
        $menu_id = $this->mmenu->getIDAnd($this->_data['menuInfo']['menu_alias'],TRUE,$this->_data['lang']);
        if(empty($this->_data['menuInfo']))
        {
            redirect(base_url());
        }
        $orderby = " id DESC";
        $this->_data['myMenu'] = $this->mmenu->getMenu($this->_data['menuInfo']['id']);
        if(empty($this->_data['myMenu']) && $this->_data['menuInfo']['menu_parent'] != 1)
        {
            $this->_data['myMenu'] = $this->mmenu->getMenu($this->_data['menuInfo']['menu_parent']);
        }
        $page = (isset($_REQUEST['page']) && $_REQUEST['page']) ? $_REQUEST['page'] : '1';
        $config['per_page'] = 10;
        $config['uri_segment'] = (($page - 1) * $config['per_page']);  

        $object_video = 'n.id';
        $object_video .= ',nl.video_lang_name,n.video_picture,nl.video_lang_alias';
        $condition_video= 'nl.video_lang="'.$this->_data['lang'].'" and n.video_parent in ('.$menu_id.')';
        $this->_data['list'] = $this->mvideo->getVideo($object_video, $condition_video, 'n.id desc', $config['uri_segment'] . ',' . $config['per_page']);

        if(count($this->_data["list"])==1 && !isset($_REQUEST['page'])){
            redirect (base_url.$this->_data["list"][0]->video_lang_alias.'-video'.$this->_data["list"][0]->id.'.html');
        }
        $this->_data["record"] = $this->mvideo->countQuery($condition_video);

        $config['total_rows'] = $this->_data["record"];
        $config['num_links'] = 5;
        $config['base_url'] = base_url() . $this->uri->segment(1) . '/?page=';
        $this->_data["pagination"] = $this->paging->paging_url($this->_data["record"], $page, $config['per_page'], $config['num_links'], $config['base_url']);
        $this->_data["title"]= $this->_data['menuInfo']["menu_name"];
        $this->my_layout->view("frontend/video/list_view",$this->_data);
    }

    /**begin trinh bay chi tiet*/
    public function detail($menu_alias,$id)
    {
        $this->_data['menuInfo'] = $this->mmenu->getInfoAlias($menu_alias,$this->_data['lang']);
        if(empty($this->_data['menuInfo']))
        {
            redirect(base_url());
        }
        $object_video = 'n.id,n.video_parent,n.video_view,n.video_picture,n.video_link';
        $object_video .= ',nl.video_lang_name,nl.video_lang_alias';
        $this->_data['same'] = NULL;
        if(!empty($id))
        {
            $condition_video="  n.id=".$id;
            $this->_data['info'] = $this->mvideo->getvideo($object_video, $condition_video, 'n.id desc', '1');
            if(!empty($this->_data['info']))
            {
                $condition_video_same =  '  nl.video_lang = "'.$this->_data['lang'].'" and n.id != '.$id.' and video_parent = '.$this->_data['info']->video_parent;
                $this->_data['same'] = $this->mvideo->getvideo($object_video, $condition_video_same, 'n.id desc', '0,20');
                $arr_up=array(
                    "video_view"=>($this->_data['info']->video_view + 1),
                );
                $this->mvideo->updateData($id,$arr_up);
                $this->_data["title"] = $this->_data['info']->video_lang_name;
            }
        }
        $this->_data['banner_detail'] = $this->mbanner->banner('banner_detail',$this->_data['info']->video_parent);
        $this->my_layout->view("frontend/video/detail_view",$this->_data);
    }
    /**end trinh bay chi tiet*/
}
