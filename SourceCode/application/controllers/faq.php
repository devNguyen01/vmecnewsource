<?php
/**
* 
*/
class faq extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->Model("mfaq");
    }
    /**begin trinh bay danh sach*/
    public function index($menu_alias)
    {
        $menuTmp = $this->mmenu->getInfoAliasTmp($menu_alias);
        $this->_data['menuInfo'] = $this->mmenu->getInfoID($menuTmp,$this->_data['lang']);
        if(empty($this->_data['menuInfo']) && !isset($_REQUEST['fkey']))
        {
            redirect(base_url());
        }
        $this->_data['menuChild'] = $this->mmenu->getMenu($this->_data['menuInfo']['id'],$this->_data['lang']);
        if(empty($this->_data['menuChild']))
        {
            $this->_data['menuChild'] = $this->mmenu->getMenu($this->_data['menuInfo']['menu_parent'],$this->_data['lang']);
        }
        $this->_data['captcha'] = $_SESSION['captcha'];
        $_SESSION['captcha']    =   rand(11111,99999);
        $this->_data['ftype'] = isset($_REQUEST['ftype']) && is_numeric($_REQUEST['ftype']) ? $_REQUEST['ftype']:'';
        $orderby = " id DESC";
        $this->_data['myMenu'] = $this->mmenu->getMenu($this->_data['menuInfo']['id']);
        if(empty($this->_data['myMenu']) && $this->_data['menuInfo']['menu_parent'] != 1)
        {
            $this->_data['myMenu'] = $this->mmenu->getMenu($this->_data['menuInfo']['menu_parent']);
        }
        $this->_data['banner_right_xv'] = $this->mbanner->banner('banner_right_xv',$this->_data['menuInfo']['id']);
        $page = (isset($_REQUEST['page']) && is_numeric($_REQUEST['page'])) ? $_REQUEST['page'] : '1';
        $config['per_page'] = 4;
        $config['uri_segment'] = (($page - 1) * $config['per_page']);  

        $object_faq = '';
        $condition_faq= 'faq_title_'.$this->_data['lang'].' != ""';
        if($this->_data['ftype'])
        {
            $condition_faq .= ' and faq_type='.$this->_data['ftype'];
        }
        $this->_data['list'] = $this->mfaq->getFAQ($object_faq, $condition_faq, 'id desc', $config['uri_segment'] . ',' . $config['per_page']);
        $this->_data["record"] = $this->mfaq->countQuery($condition_faq);

        $config['total_rows'] = $this->_data["record"];
        $config['num_links'] = 5;
        $config['base_url'] = base_url() . $this->uri->segment(1) . '/?ftype='.$this->_data['ftype'].'&page=';
        $this->_data["pagination"] = $this->paging->paging_url($this->_data["record"], $page, $config['per_page'], $config['num_links'], $config['base_url']);
        $this->_data["title"]= $this->_data['menuInfo']["menu_name"];
        /*gui cau hoi*/
        $this->_data['formData']['faq_fullname'] = '';
        $this->_data['formData']['faq_email'] = '';
        $this->_data['formData']['faq_phone'] = '';
        $this->_data['formData']['faq_title_vn'] = '';
        if(isset($_POST['fsubmit']))
        {
            $this->_data['formData']['faq_fullname'] = $this->security->sanitize_filename($this->input->post('faq_fullname'));
            $this->_data['formData']['faq_email'] = $this->security->sanitize_filename($this->input->post('faq_email'));
            $this->_data['formData']['faq_phone'] = $this->security->sanitize_filename($this->input->post('faq_phone'));
            $this->_data['formData']['faq_title_vn'] = $this->security->sanitize_filename($this->input->post('faq_title'));
            $this->_data['formData']['faq_type'] = $this->_data['ftype'];
            $this->_data['formData']['faq_status'] = 0;
            $this->_data['formData']['faq_createdate'] = time();
            $this->_data['formData']['faq_updatedate'] = time();
            $faq_captcha = $this->input->post('faq_captcha');
            if($faq_captcha == $this->_data['captcha']){
                $this->mfaq->addData($this->_data['formData']);
                redirect(current_url().'/?success=ok');
            }else{
                $this->_data['error'][] = 'Mã xác nhận không đúng.';
            }
        }
        /*banner page*/
        $this->_data['banner_page'] = $this->mbanner->banner('banner_page',$this->_data['menuInfo']['id']);
        if(empty($this->_data['banner_page'])){
            $this->_data['banner_page'] = $this->mbanner->banner('banner_page',$this->_data['menuInfo']['menu_parent']);
        }
        $this->_data['title_cate'] = $this->mmenu->getTitle(8,$this->_data['lang']);
        $this->_data['hd_cate'] = $this->mmenu->getHiddenID($this->_data['menuInfo']['menu_parent'],$this->_data['lang']);
        $this->my_layout->view("frontend/faq/list_view",$this->_data);
    }
    /**end trinh bay danh sach*/
}