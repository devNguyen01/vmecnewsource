<?php
class contact extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->Model("frontend/mmailto");
    }
    
    public function index($menu_alias)
    {
        $this->_data["success"] = NULL;
        $menuTmp = $this->mmenu->getInfoAliasTmp($menu_alias);
        $this->_data['menuInfo'] = $this->mmenu->getInfoID($menuTmp,$this->_data['lang']);
        $this->_data["title"] = $this->_data['menuInfo']['menu_name'];
        if(isset($_POST['fsubmit'])){
            $name = $this->input->post("mailto_fullname");
            $email = $this->input->post("mailto_email");
            $phone = $this->input->post("mailto_phone");
            $title = $this->input->post("mailto_title");
            $content = $this->input->post("mailto_content");
            $address = $this->input->post("mailto_address");
            $file="";
            $content_mail = "";
            $content_mail .= "<h2>Thông tin khách hàng</h2>";
            $content_mail .= "<strong>Họ tên: </strong>".$name.'<br />';
            $content_mail .= "<strong>Địa chỉ: </strong>".$address.'<br />';
            $content_mail .= "<strong>Điện thoại: </strong>".$phone.'<br />';
            $content_mail .= "<strong>Email: </strong>".$email.'<br />';
            $content_mail .= "<strong>Tiêu đề: </strong>".$title.'<br />';
            $content_mail .= "<strong>Nội dung: </strong>".$content.'<br />';

            $this->_data["set_value"] = array(
                "mailto_fullname"=>$name,
                "mailto_email"=>$email,
                "mailto_address"=>$address,
                "mailto_phone"=>$phone,
                "mailto_title"=>$title,
                "mailto_content"=>$content,
                "mailto_file"=>$file,
                "mailto_create_date"=>time(),
                "mailto_status"=>0
            );
            $this->mmailto->insert($this->_data["set_value"]);
            $this->Msendmail($name,$email,company_email_1,$email,'',$title,$content_mail,$file='');
            if($this->_data['lang']=="vn")
                $this->_data["success"]  = "Chúng tôi đã nhận được mail đóng góp ý kiến của bạn, chúng tôi sẽ sớm trả lời bạn, cảm ơn bạn rất nhiều !";
            else{
                $this->_data["success"]  = "Send mail success !";
            }
        }
        /*banner page*/
        $this->_data['banner_page'] = $this->mbanner->banner('banner_page',$this->_data['menuInfo']['id']);
        if(empty($this->_data['banner_page'])){
            $this->_data['banner_page'] = $this->mbanner->banner('banner_page',$this->_data['menuInfo']['menu_parent']);
        }
        $this->_data['title_cate'] = $this->mmenu->getTitle(8,$this->_data['lang']);
        $this->_data['menuChild'] = $this->mmenu->getMenu(8,$this->_data['lang']);
        $this->my_layout->view("frontend/contact/contact_view",$this->_data);
    }
}
?>