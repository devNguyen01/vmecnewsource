<?php
    /**
    * 
    */
    class user extends MY_Controller
    {
        
        function __construct()
        {
            parent::__construct();
            $this->load->Model("frontend/muser");
        }
        
        public function index()
        {
            $this->_data['user_username'] = '';
            $this->_data['user_password'] = '';
            $this->_data['user_email'] = '';
            $this->_data['user_phone'] = '';
            $this->_data['s_member'] = $s_member = $this->session->userdata('s_member'); 
            $this->_data['title'] = 'Đăng nhập';
            if($s_member)
            {
                $this->my_layout->view("frontend/user/welcome",$this->_data);
            }
            else
            {
                $this->login();
                return;
            }
        }
        public function ajlogin()
        {
            $username = $this->input->post('user');
            $password = $this->input->post('pass');
            if($username && $password)
            {
                $check_login = $this->muser->getOnceAnd("",array("user_username"=>$username,"user_password"=>md5($password)));
                if($check_login)
                {
                    if($check_login['user_status']!=1)
                    {
                        echo 'Username is blocked !';
                    }
                    else
                    {
                        /**begin gan session*/
                        $s_member = array(
                            "s_user_id"=>$check_login["id"],
                            "s_user_fullname"=>$check_login["user_last_name"].' ' .$check_login["user_first_name"],
                            "s_user_username"=>$check_login["user_username"],
                            "s_user_password"=>$check_login["user_password"],
                            "s_user_phone"=>$check_login["user_phone"],
                            "s_user_email"=>$check_login["user_email"],
                        );
                        /**end gan session*/
                        $this->session->set_userdata('s_member',$s_member);
                        $s_member = $this->session->userdata('s_member');  
                        if($s_member)
                        {
                            echo 1;
                        }
                    }
                }else{
                    echo 'Username or password incorrect.';
                }
            }
        }
        public function login()
        {
            $this->_data['user_username'] = '';
            $this->_data['user_password'] = '';
            $this->_data['user_email'] = '';
            $this->_data['user_phone'] = '';
            $s_member = $this->session->userdata('s_member');  
            $this->_data['title'] = 'Đăng nhập';
            if($s_member)
            {
                redirect(base_url().'user/');
            }
            $username = isset($_POST['username']) && $_POST['username'] ? trim($_POST['username']):'';
            $password = isset($_POST['password']) &&  $_POST['password'] ? md5(trim($_POST['password'])):'';
            if($username && $password)
            {
                $check_login = $this->muser->getOnceAnd("",array("user_username"=>$username,"user_password"=>$password));
                if($check_login)
                {
                    if($check_login['user_status']!=1)
                    {
                        $this->_data['error'][] = 'Username is blocked !';
                    }
                    else
                    {
                        /**begin gan session*/
                        $s_member = array(
                            "s_user_id"=>$check_login["id"],
                            "s_user_fullname"=>$check_login["user_last_name"].' ' .$check_login["user_first_name"],
                            "s_user_username"=>$check_login["user_username"],
                            "s_user_password"=>$check_login["user_password"],
                            "s_user_phone"=>$check_login["user_phone"],
                            "s_user_email"=>$check_login["user_email"],
                        );
                        /**end gan session*/
                        $this->session->set_userdata('s_member',$s_member);
                        $s_member = $this->session->userdata('s_member');  
                        if($s_member)
                        {
                            if(isset($_REQUEST['redirect']) && $_REQUEST['redirect'])
                            {
                                redirect(base64_decode($_REQUEST["redirect"]));
                            }
                            else
                            {
                                redirect(base_url());
                            }
                        }
                    }
                }else{
                    $this->_data['error'][] = 'Username or password incorrect.';
                }
            }
            else
            {
                $this->_data['error'][] = 'Please check data';
            }
            $this->load->view("frontend/user/login_view",$this->_data);

        }
        public function register()
        {
            $this->_data['title'] = 'Đăng ký thành viên';
            $this->_data['error_re'] = NULL;
            $s_member = $this->session->userdata('s_member'); 
            if($s_member)
            {
                redirect(base_url().'user/');
            }
            else
            {
                $this->_data['user_username'] = isset($_POST['user_username']) && $_POST['user_username'] ? trim($_POST['user_username']):'';
                $this->_data['user_password'] = isset($_POST['user_password']) && $_POST['user_password'] ? md5(trim($_POST['user_password'])):'';
                $this->_data['user_email'] = isset($_POST['user_email']) && $_POST['user_email'] ? $_POST['user_email']:'';
                $this->_data['user_phone'] = isset($_POST['user_phone']) && $_POST['user_phone'] ? $_POST['user_phone']:'';
                $this->_data['user_first_name'] = isset($_POST['user_first_name']) && $_POST['user_first_name'] ? $_POST['user_first_name']:'';
                $this->_data['user_address'] = isset($_POST['user_address']) && $_POST['user_address'] ? $_POST['user_address']:'';

                if($this->_data['user_username'] && $this->_data['user_password'] && $this->_data['user_email'] && $this->_data['user_phone'])
                {
                    $userAdd = array(
                        "user_username"=>$this->_data['user_username'],
                        "user_password"=>$this->_data['user_password'],
                        "user_email"=>$this->_data['user_email'],
                        "user_phone"=>$this->_data['user_phone'],
                        "user_first_name"=>$this->_data['user_first_name'],
                        "user_address"=>$this->_data['user_address'],
                        "user_level"=>13,
                        "user_status"=>1
                    );
                    if($userAdd)
                    {
                        $check_login = $this->muser->getOnceAnd("",array("user_username"=>$this->_data['user_username']));
                        if($check_login==NULL)
                        {
                            $this->muser->addUser($userAdd);
                            /**end gan session*/
                            $s_member = array(
                                "s_user_fullname"=>$this->_data["user_first_name"],
                                "s_user_username"=>$this->_data["user_username"],
                                "s_user_password"=>$this->_data["user_password"],
                                "s_user_phone"=>$this->_data["user_phone"],
                                "s_user_email"=>$this->_data["user_email"],
                            );
                            $this->session->set_userdata('s_member',$s_member);
                            $s_member = $this->session->userdata('s_member');  
                            if($s_member)
                            {
                                redirect(base_url().'user/');
                            }
                        }
                        else
                        {
                            $this->_data['error_re'][] = 'User is exists';
                            $this->_data['user_username'] ='';
                            $this->_data['user_password'] ='';
                        }
                    }
                }
                // $this->load->view("frontend/user/register",$this->_data);
            }
        }
        public function logout()
        {
            $this->session->unset_userdata("s_member");
            if(isset($_REQUEST['redirect']) && $_REQUEST['redirect'])
            {
                redirect(base64_decode($_REQUEST["redirect"]));
            }
            else
            {
                redirect(base_url());
            }
        }

        public function forgot()
        {
            $s_member = $this->session->userdata('s_member');
            if($s_member)
            {
                redirect(base_url().'/user/');
            }
            $this->my_layout->view("frontend/user/forgot",$this->_data);
        }
        public function loadfull()
        {
            $this->load->view("frontend/user/loadfull",$this->_data);
        }
    }
?>