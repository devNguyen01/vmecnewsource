<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Tinh
 * Date: 11/21/13
 * Time: 2:20 PM
 * To change this template use File | Settings | File Templates.
 */
class Product extends MY_Admin_Controller{
    protected $_file_path = "";
    protected $_file_url = "";
    public function __construct(){
        parent::__construct();
        $this->load->Model("mproduct");
        $this->load->Model("mproduct_picture");
        $this->load->Model("morder");
        $this->load->Model("morder_detail");
        $this->_file_url = base_file.'product/';
        $this->_file_path = dir_root. '/public/frontend/uploads/files/product';
    }
    protected $table_lang = "tkwp_product_lang";

    /**begin danh sach*/
    public function index($start=""){
        $this->muser->permision("product","index");
        $this->_data["title"] = danh_sach;
        $and = " nl.product_lang = '".$this->_data['lang']."'";
        $page = isset($_REQUEST['page']) ? $_REQUEST['page']:1;
        $this->_data['product_parent'] = isset($_REQUEST['product_parent']) ? $_REQUEST['product_parent']:0;
        $this->_data['thot'] = isset($_REQUEST['thot']) ? $_REQUEST['thot']:0;
        $this->_data['tstatus'] = isset($_REQUEST['tstatus']) ? $_REQUEST['tstatus']:1;
        $this->_data['fkeyword'] = isset($_REQUEST['fkeyword']) ? $_REQUEST['fkeyword']:'';

        if($this->_data['product_parent'] != 0){
            $and .= " and product_parent =  ".$this->_data['product_parent'];
        }
        if($this->_data['thot'] != 0){
            $and .= " and product_hot =  ".$this->_data['thot'];
        }
        $and .= " and product_status =  ".$this->_data['tstatus'];
        $fkeyword  = (isset($_REQUEST['fkeyword']) && $_REQUEST['fkeyword']!="")?$_REQUEST['fkeyword']:'';
        if($fkeyword){
            $and .= " and ( nl.product_code like '%".$fkeyword."%'";
            $and .= " or nl.product_lang_name like '%".$fkeyword."%'";
            $and .= " or nl.product_lang_alias like '%".$fkeyword."%'";
            $and .= " or nl.product_lang_search like '%".$fkeyword."%'";
            $and .= " or nl.product_lang_seo_title like '%".$fkeyword."%')";
        }
        $orderby = " n.id DESC";
        $config['per_page']         =   15;
        $config['uri_segment']      =   (($page-1)   * $config['per_page']);
        $object = "n.id,n.user,n.product_status,n.product_parent,n.product_create_date,n.product_picture,n.product_view";
        $object .= ",nl.product_lang_name,nl.product_code,nl.product_lang_alias";
        $this->_data["list"] = $this->mproduct->getProduct($object,$and,$orderby,$config['uri_segment'].','.$config['per_page']);    
        $this->_data["record"] =  $this->mproduct->countData($and);
        $config['total_rows']       =   $this->_data["record"];
        $config['num_links']        =   5;
        $config['base_url']         =   admin_url.'product/?fkeyword='.$this->_data['fkeyword'].'&product_parent='.$this->_data['product_parent'].'&thot='.$this->_data['thot'].'&tstatus='.$this->_data['tstatus'].'&page=';
        $this->_data["pagination"]                 =   $this->paging->paging_donturl($this->_data["record"],$page,$config['per_page'],$config['num_links'],$config['base_url']);
        $this->my_layout->view("backend/product/product_list_view",$this->_data);
    }
    /**end danh sach*/

    /**begin them moi*/
    public function add(){
        $this->muser->permision("product","add");
        $this->_data["title"] = them_moi;
        $this->_data['formData']['product_id']    =   '';
        $this->_data['formData']['product']['product_picture']                        = '';
        $this->_data['formData']['product']['product_parent']                        = 0;
        $this->_data['formData']['product']['product_height']                        = '';
        $this->_data['formData']['product']['product_width']                        = '';
        $this->_data['formData']['product']['product_length']                        = '';
        $this->_data['formData']['product']['product_orderby']                        = '';
        $this->_data['formData']['product']['product_status']                        = 1;
        $this->_data['formData']['product']['product_home']                        = 0;
        $this->_data['formData']['product']['product_view']                        = 0;
        $this->_data['formData']['product']['product_hot']                        = 0;
        $this->_data['formData']['product']['product_like']                        = 0;
        $this->_data['formData']['product']['product_unlike']                        = 0;
        $this->_data['formData']['product']['product_create_date']                        = time();
        $this->_data['formData']['product']['product_update_date']                        = time();
        $this->_data['formData']['product']['user']                        = $this->_data['s_info']['s_user_id'];;
        if(!empty($this->_data['language']))
        {
            foreach ($this->_data['language'] as $key => $value) {
                $this->_data['formData'][$value->lang]['product_id']            = '';
                $this->_data['formData'][$value->lang]['product_lang']            = $value->lang;
                $this->_data['formData'][$value->lang]['product_code']            = '';
                $this->_data['formData'][$value->lang]['product_lang_name']            = '';
                $this->_data['formData'][$value->lang]['product_lang_alias']            = '';
                $this->_data['formData'][$value->lang]['product_lang_search']            = '';
                $this->_data['formData'][$value->lang]['product_lang_price']            = '';
                $this->_data['formData'][$value->lang]['product_lang_promotion']            = '';
                $this->_data['formData'][$value->lang]['product_lang_quality']            = '';
                $this->_data['formData'][$value->lang]['product_lang_madein']            = '';
                $this->_data['formData'][$value->lang]['product_lang_warranty']            = '';
                $this->_data['formData'][$value->lang]['product_lang_unit']            = '';
                $this->_data['formData'][$value->lang]['product_lang_quantity']            = '';
                $this->_data['formData'][$value->lang]['product_lang_detail']            = '';
                $this->_data['formData'][$value->lang]['product_lang_more']            = '';
                $this->_data['formData'][$value->lang]['product_lang_using']            = '';
                $this->_data['formData'][$value->lang]['product_lang_advantage']            = '';
                $this->_data['formData'][$value->lang]['product_lang_unadvantage']            = '';
                $this->_data['formData'][$value->lang]['product_lang_seo_title']            = '';
                $this->_data['formData'][$value->lang]['product_lang_seo_keyword']            = '';
                $this->_data['formData'][$value->lang]['product_lang_seo_description']            = '';
            }
        }
        if(isset($_POST['fsubmit']))
        {
            /*anh dai dien*/
            $removefile = $this->input->post('removefile');
            if($removefile==1)
            {
                $name_picture       =   '';
                $this->mproduct->removeFile($this->_file_path,$this->_data['formData']['product']["product_picture"]);
            }else{
                $name_picture           =   $this->_data['formData']['product']['product_picture'];
            }
            $picture                =   $this->mproduct->upload($this->_file_path, 'product_picture');
            $product_picture           =   !empty($picture) ? $picture['file_name'] : '';
            if($product_picture)
            {
                $name_picture       =   $product_picture;
                $this->mproduct->removeFile($this->_file_path,$this->_data['formData']['product']["product_picture"]);
            }
            $this->_data['formData']['product']['product_picture']       =    $name_picture;
            $this->_data['formData']['product']['product_parent']       =   $this->input->post('product_parent');
            $this->_data['formData']['product']['product_height']       =   $this->input->post('product_height');
            $this->_data['formData']['product']['product_width']        =   $this->input->post('product_width');
            $this->_data['formData']['product']['product_length']       =   $this->input->post('product_length');
            $this->_data['formData']['product']['product_orderby']      =   $this->input->post('product_orderby');
            $this->_data['formData']['product']['product_status']       =   $this->input->post('product_status');
            $this->_data['formData']['product']['product_home']     =   $this->input->post('product_home');
            $this->_data['formData']['product']['product_hot']      =   $this->input->post('product_hot');
            
            $product_id = $this->mproduct->addData($this->_data['formData']['product']);
            $this->_data['formData']['product_id'] = $product_id;

            if($this->input->post('product_lang_more') && $this->input->post('product_lang_more_name'))
            {
                $nmore=$this->input->post('product_lang_more_name');
                $vmore=$this->input->post('product_lang_more');
                $demso=0;
                $product_lang_more=array();
                foreach($nmore as $nm)
                {
                    if($nm){
                        $product_lang_more[$nm]=$vmore[$demso];
                        $demso++;
                    }
                }
                $product_lang_more=json_encode($product_lang_more);
            }else{
                $product_lang_more="";
            }
            if(!empty($this->_data['language']))
            {
                foreach ($this->_data['language'] as $key => $value) {
                    $product_lang_alias = mb_strtolower(url_title(convert_alias($this->_data['formData'][$value->lang]['product_lang_name'])));
                    $product_lang_alias = $product_lang_alias ? $product_lang_alias : time();

                    $this->_data['formData'][$value->lang]['product_id']            = $product_id;
                    $this->_data['formData'][$value->lang]['product_lang']            = $value->lang;
                    $this->_data['formData'][$value->lang]['product_code']  =   $this->input->post('product_code_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_name'] =   $this->input->post('product_lang_name_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_alias']    =   $product_lang_alias;
                    $this->_data['formData'][$value->lang]['product_lang_search']   =   $this->input->post('product_lang_search_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_price']    =   $this->input->post('product_lang_price_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_promotion']    =   $this->input->post('product_lang_promotion_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_quality']  =   $this->input->post('product_lang_quality_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_madein']   =   $this->input->post('product_lang_madein_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_warranty'] =   $this->input->post('product_lang_warranty_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_unit'] =   $this->input->post('product_lang_unit_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_quantity'] =   $this->input->post('product_lang_quantity_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_detail']   =   $this->input->post('product_lang_detail_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_more'] =   $product_lang_more;
                    $this->_data['formData'][$value->lang]['product_lang_unadvantage']  =   $this->input->post('product_lang_unadvantage_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_seo_title']    =   $this->input->post('product_lang_seo_title_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_seo_keyword']  =   $this->input->post('product_lang_seo_keyword_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_seo_description']  =   $this->input->post('product_lang_seo_description_'.$value->lang);
                    $check = $this->mproduct_lang->countAnd(array('product_id'=>$product_id,'product_lang'=>$value->lang));
                    if(empty($check)){
                        $this->mproduct_lang->addData($this->_data['formData'][$value->lang]);
                    }else{
                        $this->mproduct_lang->updateData($product_id,$this->_data['formData'][$value->lang],$value->lang);
                    }
                }
            }

            if(isset($_REQUEST['redirect']) && $_REQUEST['redirect']){
                redirect(base64_decode($_REQUEST['redirect']));
            }else{
                redirect(admin_url.'product/index/');
            }
        }
        $this->my_layout->view("backend/product/product_post_view",$this->_data);
    }
    /**end them moi*/

    /**begin them moi*/
    public function update($product_id){
        $this->muser->permision("product","update");
        $this->_data["title"] = cap_nhat;
        $myProduct = $this->mproduct->getData('',array('id'=>$product_id));
        $this->_data['formData']['product_id']    =   $product_id;
        $this->_data['formData']['product']['product_picture']  = $myProduct['product_picture'];
        $this->_data['formData']['product']['product_parent']   = $myProduct['product_parent'];
        $this->_data['formData']['product']['product_height']   = $myProduct['product_height'];
        $this->_data['formData']['product']['product_width']    = $myProduct['product_width'];
        $this->_data['formData']['product']['product_length']   = $myProduct['product_length'];
        $this->_data['formData']['product']['product_orderby']  = $myProduct['product_orderby'];
        $this->_data['formData']['product']['product_status']   = $myProduct['product_status'];
        $this->_data['formData']['product']['product_home'] = $myProduct['product_home'];
        $this->_data['formData']['product']['product_hot']  = $myProduct['product_hot'];
        $this->_data['formData']['product']['product_update_date']  = $myProduct['product_update_date'];
        if(!empty($this->_data['language']))
        {
            foreach ($this->_data['language'] as $key => $value) {
                $myProductLang = $this->mproduct_lang->getData('',array('product_id'=>$product_id,'product_lang'=>$value->lang));
                $this->_data['formData'][$value->lang]['product_id']            = $product_id;
                $this->_data['formData'][$value->lang]['product_lang']            = $value->lang;
                $this->_data['formData'][$value->lang]['product_code']                  = !empty($myProductLang) ? $myProductLang['product_code'] : '';
                $this->_data['formData'][$value->lang]['product_lang_name']             = !empty($myProductLang) ? $myProductLang['product_lang_name'] : '';
                $this->_data['formData'][$value->lang]['product_lang_alias']            = !empty($myProductLang) ? $myProductLang['product_lang_alias'] : '';
                $this->_data['formData'][$value->lang]['product_lang_search']           = !empty($myProductLang) ? $myProductLang['product_lang_search'] : '';
                $this->_data['formData'][$value->lang]['product_lang_price']            = !empty($myProductLang) ? $myProductLang['product_lang_price'] : '';
                $this->_data['formData'][$value->lang]['product_lang_promotion']        = !empty($myProductLang) ? $myProductLang['product_lang_promotion'] : '';
                $this->_data['formData'][$value->lang]['product_lang_quality']          = !empty($myProductLang) ? $myProductLang['product_lang_quality'] : '';
                $this->_data['formData'][$value->lang]['product_lang_madein']           = !empty($myProductLang) ? $myProductLang['product_lang_madein'] : '';
                $this->_data['formData'][$value->lang]['product_lang_warranty']         = !empty($myProductLang) ? $myProductLang['product_lang_warranty'] : '';
                $this->_data['formData'][$value->lang]['product_lang_unit']             = !empty($myProductLang) ? $myProductLang['product_lang_unit'] : '';
                $this->_data['formData'][$value->lang]['product_lang_quantity']         = !empty($myProductLang) ? $myProductLang['product_lang_quantity'] : '';
                $this->_data['formData'][$value->lang]['product_lang_detail']           = !empty($myProductLang) ? $myProductLang['product_lang_detail'] : '';
                $this->_data['formData'][$value->lang]['product_lang_more']             = !empty($myProductLang) ? json_decode($myProductLang['product_lang_more'],true) : '';
                $this->_data['formData'][$value->lang]['product_lang_using']            = !empty($myProductLang) ? $myProductLang['product_lang_using'] : '';
                $this->_data['formData'][$value->lang]['product_lang_advantage']        = !empty($myProductLang) ? $myProductLang['product_lang_advantage'] : '';
                $this->_data['formData'][$value->lang]['product_lang_unadvantage']      = !empty($myProductLang) ? $myProductLang['product_lang_unadvantage'] : '';
                $this->_data['formData'][$value->lang]['product_lang_seo_title']        = !empty($myProductLang) ? $myProductLang['product_lang_seo_title'] : '';
                $this->_data['formData'][$value->lang]['product_lang_seo_keyword']      = !empty($myProductLang) ? $myProductLang['product_lang_seo_keyword'] : '';
                $this->_data['formData'][$value->lang]['product_lang_seo_description']  = !empty($myProductLang) ? $myProductLang['product_lang_seo_description'] : '';
            }
        }
        if(isset($_POST['fsubmit']))
        {
            /*anh dai dien*/
            $removefile = $this->input->post('removefile');
            if($removefile==1)
            {
                $name_picture       =   '';
                $this->mproduct->removeFile($this->_file_path,$this->_data['formData']['product']["product_picture"]);
            }else{
                $name_picture           =   $this->_data['formData']['product']['product_picture'];
            }
            $picture                =   $this->mproduct->upload($this->_file_path, 'product_picture');
            $product_picture           =   !empty($picture) ? $picture['file_name'] : '';
            if($product_picture)
            {
                $name_picture       =   $product_picture;
                $this->mproduct->removeFile($this->_file_path,$this->_data['formData']['product']["product_picture"]);
            }
            $this->_data['formData']['product']['product_picture']       =    $name_picture;

            $this->_data['formData']['product']['product_parent']       =   $this->input->post('product_parent');
            $this->_data['formData']['product']['product_height']       =   $this->input->post('product_height');
            $this->_data['formData']['product']['product_width']        =   $this->input->post('product_width');
            $this->_data['formData']['product']['product_length']       =   $this->input->post('product_length');
            $this->_data['formData']['product']['product_orderby']      =   $this->input->post('product_orderby');
            $this->_data['formData']['product']['product_status']       =   $this->input->post('product_status');
            $this->_data['formData']['product']['product_home']     =   $this->input->post('product_home');
            $this->_data['formData']['product']['product_hot']      =   $this->input->post('product_hot');
            
            $this->mproduct->updateData($product_id, $this->_data['formData']['product']);
            $this->_data['formData']['product_id'] = $product_id;
            if($this->input->post('product_lang_more') && $this->input->post('product_lang_more_name'))
            {
                $nmore=$this->input->post('product_lang_more_name');
                $vmore=$this->input->post('product_lang_more');
                $demso=0;
                $product_lang_more=array();
                foreach($nmore as $nm)
                {
                    if($nm){
                        $product_lang_more[$nm]=$vmore[$demso];
                        $demso++;
                    }
                }
                $product_lang_more=json_encode($product_lang_more);
            }else{
                $product_lang_more="";
            }

            if(!empty($this->_data['language']))
            {
                foreach ($this->_data['language'] as $key => $value) {
                    $product_lang_alias = mb_strtolower(url_title(convert_alias($this->_data['formData'][$value->lang]['product_lang_name'])));
                    $product_lang_alias = $product_lang_alias ? $product_lang_alias : time();

                    $this->_data['formData'][$value->lang]['product_id']            = $product_id;
                    $this->_data['formData'][$value->lang]['product_lang']            = $value->lang;
                    $this->_data['formData'][$value->lang]['product_code']  =   $this->input->post('product_code_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_name'] =   $this->input->post('product_lang_name_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_alias']    =   $product_lang_alias;
                    $this->_data['formData'][$value->lang]['product_lang_search']   =   $this->input->post('product_lang_search_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_price']    =   $this->input->post('product_lang_price_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_promotion']    =   $this->input->post('product_lang_promotion_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_quality']  =   $this->input->post('product_lang_quality_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_madein']   =   $this->input->post('product_lang_madein_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_warranty'] =   $this->input->post('product_lang_warranty_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_unit'] =   $this->input->post('product_lang_unit_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_quantity'] =   $this->input->post('product_lang_quantity_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_detail']   =   $this->input->post('product_lang_detail_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_more'] =   $product_lang_more;
                    $this->_data['formData'][$value->lang]['product_lang_using']    =   $this->input->post('product_lang_using_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_advantage']    =   $this->input->post('product_lang_advantage_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_unadvantage']  =   $this->input->post('product_lang_unadvantage_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_seo_title']    =   $this->input->post('product_lang_seo_title_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_seo_keyword']  =   $this->input->post('product_lang_seo_keyword_'.$value->lang);
                    $this->_data['formData'][$value->lang]['product_lang_seo_description']  =   $this->input->post('product_lang_seo_description_'.$value->lang);
                    $check = $this->mproduct_lang->countAnd(array('product_id'=>$product_id,'product_lang'=>$value->lang));
                    if(empty($check)){
                        $this->mproduct_lang->addData($this->_data['formData'][$value->lang]);
                    }else{
                        $this->mproduct_lang->updateData($product_id,$this->_data['formData'][$value->lang],$value->lang);
                    }
                }
            }
            if(isset($_REQUEST['redirect']) && $_REQUEST['redirect']){
                redirect(base64_decode($_REQUEST['redirect']));
            }else{
                redirect(admin_url.'product/index/');
            }
        }
        $this->_data['title'] = 'Update';
        $this->my_layout->view("backend/product/product_post_view",$this->_data);
    }
    /**end them moi*/


    /**begin cap nhat trang thai*/
    public function status($id,$status){
        $this->muser->permision("product","status");
        $this->_data = array(
            "product_status"=>$status
        );
        $this->mproduct->updateProduct($id,$this->_data);
        if(isset($_REQUEST['redirect']))
            redirect(base64_decode($_REQUEST['redirect']));
        else
            redirect(admin_url.'product/index/');
    }
    /**end cap nhat trang thai*/

    /**begin xoa san pham*/
    public function delete($id){
        $this->muser->permision("product","delete");
        $url = "public/frontend/uploads/files/".$this->uri->segment(3)."/";
        if(isset($id) && is_numeric($id)){
            $myProduct = $this->mproduct->getOnceAnd(array("id"=>$id));
            if($myProduct){
                /**xoa hinh*/
                if(file_exists($url.$myProduct["product_picture"])){
                    unlink($url.$myProduct["product_picture"]);
                }
                if(file_exists($url.'thumbnail/'.$myProduct["product_picture"])){
                    unlink($url.'thumbnail/'.$myProduct["product_picture"]);
                }
                $this->mproduct_lang->deleteProducLang($myProduct["id"]);
            }
            $this->mproduct->deleteProduct($id);

            /**tkwp_product_picture*/
            $list_picture = $this->mproduct_picture->getArray('',array("product_id"=>$id));
            if($list_picture){
                foreach($list_picture as $item){
                    if(file_exists($url.'thumbnail/'.$item["product_picture_name"])){
                        unlink($url.'thumbnail/'.$item["product_picture_name"]);
                    }
                    if(file_exists($url.$item["product_picture_name"])){
                        unlink($url.$item["product_picture_name"]);
                    }
                    $this->mproduct_picture->deleteData($item["id"]);
                }
            }

            if(isset($_REQUEST['redirect']))
                redirect(base64_decode($_REQUEST['redirect']));
            else
                redirect(admin_url.'product/index/');
        }else{
            $this->_data["title"]="Không tìm thấy thông tin để xóa";
            $this->_data["message"] = "Vui lòng kiểm tra lại đường dẫn !!";
            $this->my_layout->view("backend/404/404_view",$this->_data);
        }
    }
    /**end xoa san pham*/

    /**begin don hang*/
    public function order($start=""){
        $this->muser->permision("product","order");
        $this->load->Model("backend/morder");
        $this->load->library(array("my_layout"));
        $this->my_layout->setLayout("template/backend/template");/**load file layout chinh*/
        $this->_data["title"] = danh_sach;        
        $and_list = array("order_status !="=>"-1");
        /**begin cau ihnh phan trang*/
        $config["base_url"] = base_url().$this->_data['lang'].'/'.admin_name.'/product/order/';
        $config["total_rows"] =  $this->morder->countAnd($and_list);
        $config["per_page"] = 10;
        $config["uri_segment"] = $this->uri->segment(5);
        $config['num_links'] = 3;
        $config['cur_tag_open'] =  '<a class="currentpage">';
        $config['cur_tag_close'] =  '</a>';
        $config['next_link'] =  'Next »';
        $config['prev_link'] =  '« Prev';
        $this->pagination->initialize($config);
        $this->_data["create_links"] = $this->pagination->create_links();
        /**end cau hinh phan trang*/

        /**lay du lieu*/
        $this->_data["list"] = $this->morder->listAll($and_list,$orderby="",$config["per_page"],$start);        
        $this->_data["record"] = $this->morder->countAnd($and_list);
        $this->my_layout->view("backend/product/product_list_order_view",$this->_data);        
    }
    /**end don hang*/

    /**begin don hang*/
    public function order_detail($order_id){
         
        $this->load->Model("frontend/mconfig");
        $this->mconfig->defined_helper($this->_data['lang']);
        $this->muser->permision("product","order_detail");
        $this->load->library(array("my_layout"));
        $this->my_layout->setLayout("template/backend/order");/**load file layout chinh*/
        $this->_data["title"] = "Thông tin đơn hàng";
        $this->_data["customer"] = $this->morder->getOnceAnd(array("id"=>$order_id));        
        $this->_data["order_detail"] = $this->morder_detail->listAll(array("order_id"=>$order_id),$orderby="id desc");        

        if(isset($_POST['fsubmit']))
        {
            $status = $_POST['status'];
            $this->_data["set_status"] = array(
            "order_status"=>$status
        );
        $this->morder->updateData($order_id,$this->_data["set_status"]);
        }        

        $this->my_layout->view("backend/product/product_order_detail_view",$this->_data);
    }
    /**end don hang*/

    /**begin xoa don hang*/
    public function delorder_detail($id)
    {
        if(is_numeric($id)>0){
            $this->morder->delete($id);
            $this->morder_detail->delete($id);
            if(isset($_REQUEST['redirect']))
                redirect(base64_decode($_REQUEST['redirect']));
            else
                redirect(admin_url.'product/order/');
        }else
        {
            $this->_data["title"]="Không tìm thấy thông tin để xóa";
            $this->_data["message"] = "Vui lòng kiểm tra lại đường dẫn !!";
            $this->my_layout->view("backend/404/404_view",$this->_data);
        }
    }
    /**end xoa don hang*/
}