<?php

/**
 * Created by PhpStorm.
 * User: Tinh
 * Date: 12/28/13
 * Time: 11:11 AM
 */
class faq extends MY_Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->Model("mfaq");
    }

    /**begin danh sach */
    public function index(){
        $this->muser->permision("faq","index");
        $this->_data["title"] = "Danh sách hỏi đáp";
        $page = isset($_REQUEST['page']) ? $_REQUEST['page']:1;
        $this->_data['tstatus'] = isset($_REQUEST['tstatus']) ? $_REQUEST['tstatus']:'';
        $this->_data['fkeyword'] = isset($_REQUEST['fkeyword']) ? $_REQUEST['fkeyword']:'';
        $and = ' 1 ';
        if($this->_data['tstatus']){
            $and .= " and faq_status =  ".$this->_data['tstatus'];
        }
        
        if($this->_data['fkeyword']){
            $and .= " and ( faq_fullname like '%".$this->_data['fkeyword']."%'";
            $and .= " or faq_email like '%".$this->_data['fkeyword']."%'";
            $and .= " or faq_phone like '%".$this->_data['fkeyword']."%'";
            $and .= " or faq_title_vn like '%".$this->_data['fkeyword']."%'";
            $and .= " or faq_title_en like '%".$this->_data['fkeyword']."%')";
        }
        $orderby = " id DESC";
        $config['per_page']         =   15;
        $config['uri_segment']      =   (($page-1)   * $config['per_page']);
        $this->_data["list"] =$list= $this->mfaq->getQuery($object="",$and,$orderby,$config['uri_segment'].','.$config['per_page']);
        $this->_data["record"] =  $this->mfaq->countQuery($and);
        /**begin cau ihnh phan trang*/
        $config['total_rows']       =   $this->_data["record"];
        $config['num_links']        =   5;
        $config['base_url']         =   admin_url.'faq/?fkeyword='.$this->_data['fkeyword'].'&tstatus='.$this->_data['tstatus'].'&page=';
        $this->pagination->initialize($config);
        $this->_data["pagination"]                 =   $this->paging->paging_donturl($this->_data["record"],$page,$config['per_page'],$config['num_links'],$config['base_url']);
        $this->my_layout->view("backend/faq/faq_list_view",$this->_data);
    }
    /**end danh sach*/

    /**begin them moi cau hoi*/
    public function add(){
        $this->muser->permision("faq","add");
        $this->_data['title'] = "Thêm mới hỏi đáp";
        $this->_data['formData']['faq_fullname'] = '';
        $this->_data['formData']['faq_email'] = '';
        $this->_data['formData']['faq_phone'] = '';
        
        if(!empty($this->_data['language']))
        {
            foreach ($this->_data['language'] as $key => $value) {
                $this->_data['formData']['faq_title_'.$value->lang] = '';
            }
        }
        $this->_data['formData']['faq_status'] = '1';
        $this->_data['formData']['faq_type'] = '1';
        $this->_data['formData']['faq_createdate'] = time();
        $this->_data['formData']['faq_updatedate'] = time();
        $this->_data['formData']['user'] = $this->_data['s_info']['s_user_id'];
        if(isset($_POST['fsubmit']))
        {
            if(!empty($this->_data['language']))
            {
                foreach ($this->_data['language'] as $key => $value) {
                    $this->_data['formData']['faq_title_'.$value->lang] = $this->input->post('faq_title_'.$value->lang);
                }
            }
            $this->_data['formData']['faq_fullname'] = $this->input->post('faq_fullname');
            $this->_data['formData']['faq_email'] = $this->input->post('faq_email');
            $this->_data['formData']['faq_phone'] = $this->input->post('faq_phone');
            $this->_data['formData']['faq_type'] = $this->input->post('faq_type');
            $this->_data['formData']['faq_status'] = $this->input->post('faq_status');
            $this->mfaq->addData($this->_data['formData']);
            redirect(admin_url.'faq/');
        }
        $this->my_layout->view("backend/faq/faq_post_view",$this->_data);
    }
    /**end them moi cau hoi*/

    public function update($id)
    {
        $this->muser->permision("faq","update");
        $this->_data['title'] = "Cập nhật hỏi đáp";
        $myFaq = $this->mfaq->getData('',array('id'=>$id));
        if(empty($myFaq))
        {
            redirect(admin_url.'faq/');
        }
        $this->_data['formData']['faq_fullname'] = $myFaq['faq_fullname'];
        $this->_data['formData']['faq_email'] = $myFaq['faq_email'];
        $this->_data['formData']['faq_phone'] = $myFaq['faq_phone'];
        $this->_data['formData']['faq_type']  = $myFaq['faq_type'];
        $this->_data['formData']['faq_status'] = $myFaq['faq_status'];
        $this->_data['formData']['faq_updatedate'] = time();
        if(!empty($this->_data['language']))
        {
            foreach ($this->_data['language'] as $key => $value) {
                $this->_data['formData']['faq_title_'.$value->lang] = $myFaq['faq_title_'.$value->lang];
            }
        }
        if(isset($_POST['fsubmit']))
        {
            if(!empty($this->_data['language']))
            {
                foreach ($this->_data['language'] as $key => $value) {
                    $this->_data['formData']['faq_title_'.$value->lang] = $this->input->post('faq_title_'.$value->lang);
                }
            }
            $this->_data['formData']['faq_fullname'] = $this->input->post('faq_fullname');
            $this->_data['formData']['faq_email'] = $this->input->post('faq_email');
            $this->_data['formData']['faq_phone'] = $this->input->post('faq_phone');
            $this->_data['formData']['faq_type'] = $this->input->post('faq_type');
            $this->_data['formData']['faq_status'] = $this->input->post('faq_status');
            $this->mfaq->updateData($id,$this->_data['formData']);
            redirect(admin_url.'faq/');
        }
        $this->my_layout->view("backend/faq/faq_post_view",$this->_data);
    }
    public function info($id)
    {
        $this->muser->permision("faq","info");
        $this->_data['title'] = "Chi tiết câu hỏi";
        $this->_data['myFaq'] = $this->mfaq->getData('',array('id'=>$id));
        if(empty($this->_data['myFaq']))
        {
            redirect(admin_url.'faq/');
        }
        $this->_data['myFaqReply'] = $this->mfaq_reply->getQuery('','faq_id='.$id,'id desc');
        /*begin them moi tra loi*/
        $this->_data['formData']['faq_id'] = $id;
        if(!empty($this->_data['language']))
        {
            foreach ($this->_data['language'] as $key => $value) {
                $this->_data['formData']['reply_content_'.$value->lang] = $this->input->post('reply_content_'.$value->lang);
            }
        }
        $this->_data['formData']['reply_create_date'] = time();
        $this->_data['formData']['user'] = $this->_data['s_info']['s_user_id'];
        if(isset($_POST['fsubmit']))
        {
            if(!empty($this->_data['language']))
            {
                foreach ($this->_data['language'] as $key => $value) {
                    $this->_data['formData']['reply_content_'.$value->lang] = $this->input->post('reply_content_'.$value->lang);
                    if($this->_data['formData']['reply_content_'.$value->lang]){
                        $this->mfaq_reply->addData($this->_data['formData']);
                    }
                }
            }
            redirect(current_url());
        }
        /*end them moi tra loi*/
        $this->my_layout->view("backend/faq/faq_info_view",$this->_data);
    }

    /**begin delete faq*/
    public  function delete($id){
        $this->muser->permision("faq","delete");
        if(is_numeric($id)){
            $this->mfaq->deleteData($id);
            $this->mfaq_reply->deleteAnd(array("faq_id"=>$id));
            redirect(admin_url."faq/index/");
        }
    }
    /**end delete faq*/
    public function del_reply($id)
    {
        $this->muser->permision("faq","del_reply");
        if(is_numeric($id)){
            $this->mfaq_reply->delete($id);
            if(isset($_REQUEST['redirect']) && $_REQUEST['redirect'])
            {
                redirect(base64_decode($_REQUEST['redirect']));
            }
            else{
                redirect(admin_url."faq/index/");
            }
        }
    }

}