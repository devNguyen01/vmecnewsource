<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Tinh
 * Date: 11/7/13
 * Time: 11:45 AM
 * To change this template use File | Settings | File Templates.
 */
class Home extends MY_Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->Model("mnews");
        $this->load->Model("backend/mhistory_login");
        $this->load->Model("backend/mmailto");
    }
    public function index(){
        $this->muser->permision("home","index");
        $this->_data["title"]= 'Welcome admin CMS Admin';
        /**begin top 5 album*/
        $and_album = array("album_status !="=>-1);
        $this->_data["list_album"] = '';
        $this->_data["record_album"] = '';
        /**end top 5 album*/

        /**begin top 5 bai viet*/
        $and_news = array("news_status !="=>-1);
        $this->_data["list_news"] = $this->mnews->getArray('',$and_news,$orderby="id DESC",5);
        $this->_data["record_news"] = $this->mnews->countAnd($and_news);
        /**end top 5 bai viet*/

        /**begin top 5 san pham*/
        $this->_data["list_product"] = '';
        $this->_data["record_product"] = '';
        /**end top 5 san pham*/
        
        /**begin hostory login*/
        $and_login  = "";
        $this->_data["list_history_login"] = $this->mhistory_login->listAllLimit($and_login,$orderby="id DESC",10,0);
        $this->_data["record_history_login"] = $this->mhistory_login->countAnd($and_login);
        
        /**end hostory login*/

        /***begin thong ke tong*/
        $this->_data["sum_aritce"] = $this->mnews->countAnd(array("news_status"=>1));
        $this->_data["sum_user"] = $this->muser->countAnd(array("user_status"=>1));
        $this->_data["sum_mailto"] = $this->mmailto->countAnd();
        $this->_data["sum_album"] = '';
        $this->_data["sum_product"] = '';
        /**end thong ke tong*/

        $this->my_layout->view("backend/home/home_view",$this->_data);
    }
}