<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Tinh
 * Date: 11/22/13
 * Time: 12:01 PM
 * To change this template use File | Settings | File Templates.
 */
class config extends MY_Admin_Controller{
    public function __construct(){
        parent::__construct();        
        $this->load->Model("backend/mconfig");        
    }
    public function index(){

    }
    public function setting(){
        $this->_data["title"]="Cấu hình webiste";
        $and = "";
        $this->_data["info"] = $this->mconfig->getOnceAnd($and);
        $this->form_validation->set_error_delimiters('<div class="error">','</div>');
        $this->form_validation->CI =& $this;
        $this->form_validation->set_rules('config_title','Title website','required');
        if($this->form_validation->run()==TRUE){
            $id = 1;
//            $config_background =  $this->mconfig->do_upload();
            $this->_data["set_value"] = array(
                "config_title"=>$this->input->post("config_title"),
                "config_video"=>$this->input->post("config_video"),
                "config_keyword"=>$this->input->post("config_keyword"),
                "config_description"=>$this->input->post("config_description"),
                "config_contact"=>$this->input->post("config_contact"),
                "config_footer"=>$this->input->post("config_footer"),
//                "config_background"=> !empty($config_background) ? date("Ymd").'/'.$config_background['file_name'] : '',
                "config_goole_analytics"=>$this->input->post("config_goole_analytics"),
                "config_dublin_core"=>$this->input->post("config_dublin_core"),
                "config_geometa_tag"=>$this->input->post("config_geometa_tag"),
                "config_feeds"=>$this->input->post("config_feed"),
                "config_logo"=>$this->input->post("config_logo"),
                "config_favicon"=>$this->input->post("config_favicon"),
                "config_mail_type"=>$this->input->post("config_mail_type"),
                "config_mail_user_gmail"=>$this->input->post("config_mail_user_gmail"),
                "config_mail_pass_gmail"=>$this->input->post("config_mail_pass_gmail"),
                "config_mail_port_gmail"=>$this->input->post("config_mail_port_gmail"),
                "config_mail_smtp_gmail"=>$this->input->post("config_mail_smtp_gmail"),
                "config_mail_user_server"=>$this->input->post("config_mail_user_server"),
                "config_mail_pass_server"=>$this->input->post("config_mail_pass_server"),
                "config_mail_port_server"=>$this->input->post("config_mail_port_server"),
                "config_mail_smtp_server"=>$this->input->post("config_mail_smtp_server"),
                "config_upload"=>$this->input->post("config_upload"),
                "config_upload_u"=>$this->input->post("config_upload_u"),
                "config_upload_p"=>$this->input->post("config_upload_p"),
                "config_upload_id"=>$this->input->post("config_upload_id"),
                "config_create_date"=>time(),
            );
            $update = $this->mconfig->update($id,$this->_data["set_value"]);
            if($update){
                if(isset($_REQUEST['redirect']) && $_REQUEST['redirect']){
                    redirect(base64_decode($_REQUEST['redirect']));
                }else{
//                    $redirect_url = admin_url . "config/setting/";
//                    echo '<script type="text/javascript">window.location = "'.$redirect_url.'"</script>';
                    redirect(admin_url."config/setting/");
                }
            }
        }

        $this->my_layout->view("backend/config/config_config_view",$this->_data);
    }

    /**begin thong tin doanh nghiep*/
    public function company(){
        $this->_data["title"]  = "Thông tin doanh nghiệp";
        $this->load->Model("backend/mcompany");
        $this->load->Model("backend/mcompany_contact");
        $this->load->Model("backend/mcompany_support");
        $this->_data["company"] = $this->mcompany->getOnceAnd(array("company_lang"=>$this->_data['lang'],'company_id'=>1));
        $this->_data["company_contact"] = $this->mcompany_contact->getListAND(array("company_contact_lang"=>$this->_data['lang']));
        $this->_data["company_support"] = $this->mcompany_support->getListAND();
        $this->my_layout->view("backend/config/config_company_view",$this->_data);
    }
    /**end thong tin odnah nghiep*/

    public function company_update($company_id)
    {
        $this->_data["title"]  = "Cập nhật thông tin doanh nghiệp";
        $this->load->Model("backend/mcompany");
        if(!empty($this->_data['language']))
        {
            foreach ($this->_data['language'] as $key => $value) {
                $myCompany = $this->mcompany->getOnceAnd(array("company_id"=>$company_id,"company_lang"=>$value->lang));
                $this->_data['formData'][$value->lang]['company_name']            = $myCompany['company_name'];
                $this->_data['formData'][$value->lang]['company_name_short']            = $myCompany['company_name_short'];
            }
        }
        if(isset($_POST['fsubmit']))
        {
            if(!empty($this->_data['language']))
            {
                foreach ($this->_data['language'] as $key => $value) {
                    $this->_data['formData'][$value->lang]['company_lang']            = $value->lang;
                    $this->_data['formData'][$value->lang]['company_id']            = $company_id;
                    $this->_data['formData'][$value->lang]['company_name']       =     $this->input->post('company_name_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_name_short']      =     $this->input->post('company_name_short_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_create_date']            = date("Y-m-d H:i:s");
                    $this->_data['formData'][$value->lang]['company_update_date']            = date("Y-m-d H:i:s");
                    $this->_data['formData'][$value->lang]['user']            = $this->_data['s_info']['s_user_id'];
                    $check = $this->mcompany->countAnd(array('company_id'=>$company_id,'company_lang'=>$value->lang));
                    if(empty($check)){
                        $this->mcompany->addData($this->_data['formData'][$value->lang]);
                    }else{
                        $this->mcompany->updateData($company_id,$this->_data['formData'][$value->lang],$value->lang);
                    }
                }
            }
            if(isset($_REQUEST['redirect']) && $_REQUEST['redirect'])
                redirect(base64_decode($_REQUEST['redirect']));
            else
                redirect(admin_url."config/company/");
        }
        $this->my_layout->view("backend/config/config_company_edit",$this->_data);
    }

    /**begin xoa company_contact*/
    public function delete_contact($id){
        $this->load->Model("backend/mcompany_contact");
        if(isset($id) && is_numeric($id)){
           $this->mcompany_contact->deleteData($id);
            if(isset($_REQUEST['redirect']) && $_REQUEST['redirect']){
                redirect(base64_decode($_REQUEST['redirect']));
            }else{
                redirect(admin_url."config/company/");
            }
        }else{
            $this->_data["title"]="Không tìm thấy thông tin để xóa";
            $this->_data["message"] = "Vui lòng kiểm tra lại đường dẫn !!";
            $this->my_layout->view("backend/404/404_view",$this->_data);
        }
    }
    /**end xoa company contact*/

    /**begin xoa company_contact*/
    public function delete_support($id){
        $this->load->Model("backend/mcompany_support");
        if(isset($id) && is_numeric($id)){
           $this->mcompany_support->deleteCompanySupport($id);
            if(isset($_REQUEST['redirect']) && $_REQUEST['redirect']){
                redirect(base64_decode($_REQUEST['redirect']));
            }else{
                redirect(admin_url."config/company/");
            }
        }else{
            $this->_data["title"]="Không tìm thấy thông tin để xóa";
            $this->_data["message"] = "Vui lòng kiểm tra lại đường dẫn !!";
            $this->my_layout->view("backend/404/404_view",$this->_data);
        }
    }
    /**end xoa company contact*/

    /**begin cap nhat company contact*/
    public function add_contact(){
        $this->_data["title"] = "Cập nhật thông tin liên hệ";
        $this->load->Model("backend/mcompany_contact");
        if(!empty($this->_data['language']))
        {
            foreach ($this->_data['language'] as $key => $value) {
                $this->_data['formData'][$value->lang]['company_contact_address']            = '';
                $this->_data['formData'][$value->lang]['company_contact_position']            = '';
                $this->_data['formData'][$value->lang]['company_contact_maps']            = '';
                $this->_data['formData'][$value->lang]['company_contact_phone']            = '';
                $this->_data['formData'][$value->lang]['company_contact_hotline']            = '';
                $this->_data['formData'][$value->lang]['company_contact_email']            = '';
                $this->_data['formData'][$value->lang]['company_contact_fax']            = '';
                $this->_data['formData'][$value->lang]['company_contact_website']            = '';
                $this->_data['formData'][$value->lang]['company_contact_facebook']            = '';
                $this->_data['formData'][$value->lang]['company_contact_google']            = '';
                $this->_data['formData'][$value->lang]['company_contact_twitter']            = '';
                $this->_data['formData'][$value->lang]['company_contact_youtube']            = '';
            }
        }
        if(isset($_POST['fsubmit']))
        {
            if(!empty($this->_data['language']))
            {
                $tmpCompany= $this->mcompany_contact->getOnceAnd();
                $company_id = 1;
                if(!empty($tmpCompany)){
                    $company_id = $tmpCompany['company_id'] + 1;
                }
                foreach ($this->_data['language'] as $key => $value) {
                    $this->_data['formData'][$value->lang]['company_contact_lang']            = $value->lang;
                    $this->_data['formData'][$value->lang]['company_id']            = $company_id;
                    $this->_data['formData'][$value->lang]['company_contact_position']       =     $this->input->post('company_contact_position_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_address']       =     $this->input->post('company_contact_address_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_maps']      =     $this->input->post('company_contact_maps_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_phone']     =     $this->input->post('company_contact_phone_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_hotline']       =     $this->input->post('company_contact_hotline_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_email']     =     $this->input->post('company_contact_email_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_fax']       =     $this->input->post('company_contact_fax_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_website']       =     $this->input->post('company_contact_website_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_facebook']      =     $this->input->post('company_contact_facebook_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_google']        =     $this->input->post('company_contact_google_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_twitter']       =     $this->input->post('company_contact_twitter_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_youtube']       =     $this->input->post('company_contact_youtube_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_create_date']            = date("Y-m-d H:i:s");
                    $this->_data['formData'][$value->lang]['company_contact_update_date']            = date("Y-m-d H:i:s");
                    $this->_data['formData'][$value->lang]['user']            = $this->_data['s_info']['s_user_id'];
                    $check = $this->mcompany_contact->countAnd(array('company_id'=>$company_id,'company_contact_lang'=>$value->lang));
                    if(empty($check)){
                        $this->mcompany_contact->addData($this->_data['formData'][$value->lang]);
                    }else{
                        $this->mcompany_contact->updateData($id,$this->_data['formData'][$value->lang],$value->lang);
                    }
                }
            }
            if(isset($_REQUEST['redirect']) && $_REQUEST['redirect'])
                redirect(base64_decode($_REQUEST['redirect']));
            else
                redirect(admin_url."config/company/");
        }
        $this->my_layout->view("backend/config/config_company_add_contact_view",$this->_data);
    }
    /**end cap nhat company contact*/


    /**begin cap nhat company contact*/
    public function update_contact($id){
        $this->_data["title"] = "Cập nhật thông tin liên hệ";
        $this->load->Model("backend/mcompany_contact");
        if(!empty($this->_data['language']))
        {
            foreach ($this->_data['language'] as $key => $value) {
                $myContact = $this->mcompany_contact->getOnceAnd(array("company_id"=>$id,"company_contact_lang"=>$value->lang));
                $this->_data['formData'][$value->lang]['company_contact_lang']            = $value->lang;
                $this->_data['formData'][$value->lang]['company_contact_position']            = $myContact['company_contact_position'];
                $this->_data['formData'][$value->lang]['company_contact_address']            = $myContact['company_contact_address'];
                $this->_data['formData'][$value->lang]['company_contact_maps']            = $myContact['company_contact_maps'];
                $this->_data['formData'][$value->lang]['company_contact_phone']            = $myContact['company_contact_phone'];
                $this->_data['formData'][$value->lang]['company_contact_hotline']            = $myContact['company_contact_hotline'];
                $this->_data['formData'][$value->lang]['company_contact_email']            = $myContact['company_contact_email'];
                $this->_data['formData'][$value->lang]['company_contact_fax']            = $myContact['company_contact_fax'];
                $this->_data['formData'][$value->lang]['company_contact_website']            = $myContact['company_contact_website'];
                $this->_data['formData'][$value->lang]['company_contact_facebook']            = $myContact['company_contact_facebook'];
                $this->_data['formData'][$value->lang]['company_contact_google']            = $myContact['company_contact_google'];
                $this->_data['formData'][$value->lang]['company_contact_twitter']            = $myContact['company_contact_twitter'];
                $this->_data['formData'][$value->lang]['company_contact_youtube']            = $myContact['company_contact_youtube'];
            }
        }
        if(isset($_POST['fsubmit']))
        {
            if(!empty($this->_data['language']))
            {
                foreach ($this->_data['language'] as $key => $value) {
                    $this->_data['formData'][$value->lang]['company_contact_lang']            = $value->lang;
                    $this->_data['formData'][$value->lang]['company_contact_position']       =     $this->input->post('company_contact_position_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_address']       =     $this->input->post('company_contact_address_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_maps']      =     $this->input->post('company_contact_maps_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_phone']     =     $this->input->post('company_contact_phone_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_hotline']       =     $this->input->post('company_contact_hotline_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_email']     =     $this->input->post('company_contact_email_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_fax']       =     $this->input->post('company_contact_fax_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_website']       =     $this->input->post('company_contact_website_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_facebook']      =     $this->input->post('company_contact_facebook_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_google']        =     $this->input->post('company_contact_google_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_twitter']       =     $this->input->post('company_contact_twitter_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_youtube']       =     $this->input->post('company_contact_youtube_'.$value->lang);
                    $this->_data['formData'][$value->lang]['company_contact_create_date']            = date("Y-m-d H:i:s");
                    $this->_data['formData'][$value->lang]['company_contact_update_date']            = date("Y-m-d H:i:s");
                    $this->_data['formData'][$value->lang]['user']            = $this->_data['s_info']['s_user_id'];
                    $check = $this->mcompany_contact->countAnd(array('company_id'=>$id,'company_contact_lang'=>$value->lang));
                    if(empty($check)){
                        $this->mcompany_contact->addData($this->_data['formData'][$value->lang]);
                    }else{
                        $this->mcompany_contact->updateData($id,$this->_data['formData'][$value->lang],$value->lang);
                    }
                }
            }
            if(isset($_REQUEST['redirect']) && $_REQUEST['redirect'])
                redirect(base64_decode($_REQUEST['redirect']));
            else
                redirect(admin_url."config/company/");
        }
        $this->my_layout->view("backend/config/config_company_edit_contact_view",$this->_data);
    }
    /**end cap nhat company contact*/

    /**begin cap nhat company support*/
    public function update_support($id){

        if(isset($id) && is_numeric($id)){
            $this->_data["title"] = "Cập nhật thông tin liên hệ";
            /**lay thong tin*/
            $this->load->Model("backend/mcompany_support");
            $this->_data["info"] = $this->mcompany_support->getOnceAnd(array("id"=>$id,"company_support_lang"=>$this->_data['lang']));

            /***begin xu ly cap nhat*/
            $this->form_validation->set_error_delimiters('<div class="error">','</div>');
            $this->form_validation->CI =& $this;
            /**begin check du lieu*/
            $this->form_validation->set_rules('company_support_name','Tên nhóm hoặc phòn ban','required');
            if($this->form_validation->run()==TRUE){
                $this->_data["company_support"] = array(
                    "company_support_name"=>$this->input->post("company_support_name"),
                    "company_support_hotline"=>$this->input->post("company_support_hotline"),
                    "company_support_yahoo"=>$this->input->post("company_support_yahoo"),
                    "company_support_skype"=>$this->input->post("company_support_skype"),
                    "company_support_email"=>$this->input->post("company_support_email"),
                    "company_support_lang"=>$this->_data['lang'],
                );

                /**check contact*/
                $check_company_support= $this->mcompany_support->countAnd(array("company_support_lang"=>$this->_data['lang']));
                if($check_company_support > 0){
                    /**da ton tai => cap nhat*/
                    $this->mcompany_support->updateCompanySupport($id,$this->_data["company_support"]);
                }else{
                    /**chua ton tai => them moi*/
                    $this->mcompany_support->addCompanySupport($this->_data["company_support"]);
                }
                if(isset($_REQUEST['redirect']) && $_REQUEST['redirect']){
                    redirect(base64_decode($_REQUEST['redirect']));
                }else{
                    redirect(admin_url."config/company/");
                }
            }

        }else{
            $this->_data["title"]="Không tìm thấy thông tin để xóa";
            $this->_data["message"] = "Vui lòng kiểm tra lại đường dẫn !!";
            $this->my_layout->view("backend/404/404_view",$this->_data);
        }
        $this->my_layout->view("backend/config/config_company_edit_support_view",$this->_data);
    }
    /**end cap nhat company contact*/

    /**begin them moi support*/
    public function add_support(){
        $this->_data['title'] = 'Thêm mới hổ trợ trực tuyến';
        $this->load->Model("backend/mcompany_support");
        /***begin xu ly cap nhat*/
        $this->form_validation->set_error_delimiters('<div class="error">','</div>');
        $this->form_validation->CI =& $this;
        /**begin check du lieu*/
        $this->form_validation->set_rules('company_support_name','Tên nhóm hoặc phòn ban','required');
        if($this->form_validation->run()==TRUE){
            $this->_data["company_support"] = array(
                "company_support_name"=>$this->input->post("company_support_name"),
                "company_support_hotline"=>$this->input->post("company_support_hotline"),
                "company_support_yahoo"=>$this->input->post("company_support_yahoo"),
                "company_support_skype"=>$this->input->post("company_support_skype"),
                "company_support_email"=>$this->input->post("company_support_email"),
                "company_support_lang"=>$this->_data['lang'],
            );

            /**check support*/
            if($this->_data["company_support"]["company_support_email"] || $this->_data["company_support"]["company_support_skype"] || $this->_data["company_support"]["company_support_yahoo"] || $this->_data["company_support"]["company_support_name"] || $this->_data["company_support"]["company_support_hotline"]){
                $this->mcompany_support->addCompanySupport($this->_data["company_support"]);
            }
            if(isset($_REQUEST['redirect']) && $_REQUEST['redirect']){
                redirect(base64_decode($_REQUEST['redirect']));
            }else{
                redirect(admin_url."config/company/");
            }
        }
        $this->my_layout->view("backend/config/config_company_add_support_view",$this->_data);
    }
    /**end them moi support*/

    public function del_background()
    {
        $info = $this->mconfig->getOnceAnd();
        if($info){ 
            $fileName = dir_root. '/public/frontend/images/'.$info['config_background'];
            if (file_exists($fileName))
            {
                unlink($fileName);
                $this->mconfig->update($info['id'],array('config_background'=>''));
            }
        }
    }
}