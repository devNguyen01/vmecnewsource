<?php
class home extends  MY_Controller{
    public function __construct(){
        parent::__construct();
    }
    public function index(){
        $object_news = 'n.id,n.news_picture,n.news_parent,n.news_create_date,n.news_video,n.news_link';
        $object_news .= ',nl.news_lang_name,nl.news_lang_alias,nl.news_lang_detail';
        $condition = 'news_status = 1 and nl.news_lang="'.$this->_data['lang'].'"';

        $menuTrangChu = 2;
        $this->_data['menuInfo'] = $this->mmenu->getInfoID($menuTrangChu,$this->_data['lang']);
        $this->_data['home']['banner_home'] = $this->mbanner->banner('banner_home',$menuTrangChu);
        /*gioi thieu*/
        $menuAbout = '3';
        $this->_data['about']['aboutus'] = $this->mnews->getNews($object_news,$condition.' and news_parent='.$menuAbout,'n.id desc', 1);
        $this->_data['about']['category'] =   $this->mmenu->getMenu($menuAbout,$this->_data['lang']);
        $this->_data['about']['menuAb'] =   $this->mmenu->getInfoID($menuAbout,$this->_data['lang']);
        /*khach hang*/
        $menuCustomer = '4';
        $this->_data['customer']['category'] =   $this->mmenu->getMenu($menuCustomer,$this->_data['lang']);
        $this->_data['customer']['categoryInfo'] =   $this->mmenu->getInfoID($menuCustomer,$this->_data['lang']);
        /*san pham*/
        $menuProduct = '5';
        $this->_data['products']['category'] =   $this->mmenu->getMenu($menuProduct,$this->_data['lang']);
        $this->_data['products']['categoryInfo'] =   $this->mmenu->getInfoID($menuProduct,$this->_data['lang']);
        /*dich vu*/
        $menuService = '6';
        $this->_data['service']['category'] =   $this->mmenu->getMenu($menuService,$this->_data['lang'],'id');
        $this->_data['service']['categoryInfo'] =   $this->mmenu->getInfoID($menuService,$this->_data['lang']);
        /*tin tuc*/
        $menuNews = '7';
        $this->_data['news']['category'] =   $this->mmenu->getMenu($menuNews,$this->_data['lang']);
        $this->_data['news']['categoryInfo'] =   $this->mmenu->getInfoID($menuNews,$this->_data['lang']);
        /*ho tro*/
        $menuSupport = '8';
        $this->_data['support']['category'] =   $this->mmenu->getMenu($menuSupport,$this->_data['lang']);
        $this->_data['support']['categoryInfo'] =   $this->mmenu->getInfoID($menuSupport,$this->_data['lang']);
        /*tuyen dung*/
        $menuRecruit = '9';
        $this->_data['recruit']['category'] =   $this->mmenu->getMenu($menuRecruit,$this->_data['lang']);
        $this->_data['recruit']['categoryInfo'] =   $this->mmenu->getInfoID($menuRecruit,$this->_data['lang']);
        
        
        $menu_id = $this->mmenu->getAllID(7);
        $menu_id = implode(',', $menu_id);
        $condition_news = $condition.' and news_parent in ('.$menu_id.')';
        
        $this->_data['listNewHome'] = $this->mnews->getNews($object_news, $condition.' and news_hot = 1 ', 'n.news_create_date DESC,n.news_orderby ASC,n.id desc', 3);
        if(empty($this->_data['listNewHome']))
        {
            $this->_data['listNewHome'] = $this->mnews->getNews($object_news, $condition_news, 'n.news_create_date DESC,n.news_orderby ASC,n.id desc', 3);
        }
        $this->_data['banner_page'] = $this->mbanner->banner('banner_page',$menuTrangChu);
        $this->_data['hd_cate'] ='home';
		$view = $this->_data['mobi'];
		//if($this->_data['ie8']=='ie8'){
		//	$view = 'ie8';
		//}
        $this->my_layout->view("frontend/home/home_view".$view,$this->_data);
    }
}
