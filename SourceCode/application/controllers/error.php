<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* name: tinhnguyenvan
* email: tinhnguyenvan91@gmail.com
* phone: 0909 977 920
* @__construct => khoi tao doi tuong, load module
* @ index() => mac dinh
*/
class error extends MY_Controller {
    function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $this->_data['title'] = 'Not Found. - '.company;
        $this->my_layout->view("frontend/404/404_view",$this->_data);
    }
}
