<?php
/**
* 
*/
class news extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    /**begin trinh bay danh sach*/
    public function index($menu_alias)
    {
        $this->_data['fix_img'] = 'fix_img';
        $menuTmp = $this->mmenu->getInfoAliasTmp($menu_alias);
        $this->_data['menuInfo'] = $this->mmenu->getInfoID($menuTmp,$this->_data['lang']);
        $menu_id = $this->mmenu->getIDAnd($this->_data['menuInfo']['menu_alias'],TRUE,$this->_data['lang']);
        if(empty($this->_data['menuInfo']) && !isset($_REQUEST['fkey']))
        {
            redirect(base_url());
        }
        /*banner page*/
        $this->_data['banner_left_page'] = $this->mbanner->banner('banner_left',$this->_data['menuInfo']['id']);
        $this->_data['banner_page'] = $this->mbanner->banner('banner_page',$this->_data['menuInfo']['id']);
        if(empty($this->_data['banner_page'])){
            $this->_data['banner_page'] = $this->mbanner->banner('banner_page',$this->_data['menuInfo']['menu_parent']);
        }
        $this->_data['ntype'] = isset($_REQUEST['ntype'])  && is_numeric($_REQUEST['ntype']) ? $_REQUEST['ntype'] : 1;

        $this->_data['title_cate'] = $this->mmenu->getTitle($this->_data['menuInfo']['id'],$this->_data['lang']);
        $this->_data['hd_cate'] = $this->mmenu->getHiddenID($this->_data['menuInfo']['menu_parent'],$this->_data['lang']);
        $this->_data['menuChild'] = $this->mmenu->getMenu($this->_data['menuInfo']['id'],$this->_data['lang']);
        if(empty($this->_data['menuChild']))
        {
            $this->_data['title_cate'] = $this->mmenu->getTitle($this->_data['menuInfo']['menu_parent'],$this->_data['lang']);
            $this->_data['menuChild'] = $this->mmenu->getMenu($this->_data['menuInfo']['menu_parent'],$this->_data['lang']);
        }
        $condition_news = 'news_lang_name != "" and news_status = 1 and n.news_type ='.$this->_data['ntype'].' and nl.news_lang="'.$this->_data['lang'].'"';
        $url_search = '';
        if(isset($_REQUEST['fkey']))
        {
            $this->_data['fkey'] = $this->security->sanitize_filename($_REQUEST['fkey']);
            $condition_news .= " and (nl.news_lang_name like '%".$this->_data['fkey']."%'";
            $condition_news .= " or nl.news_lang_alias like '%".$this->_data['fkey']."%'";
            $condition_news .= " or nl.news_lang_search like '%".$this->_data['fkey']."%'";
            $condition_news .= " or nl.news_lang_summary like '%".$this->_data['fkey']."%')";
            $this->_data['banner_page'] = $this->_data['menuChild'] = NULL;

            $url_search = 'fkey='.$this->_data['fkey'].'&';
        }
        else
        {
            if($menu_id){
                $condition_news .= ' and n.news_parent in ('.$menu_id.')';
            }
        }
        $menu_view = $this->_data['menuInfo']['menu_view'] ? $this->_data['menuInfo']['menu_view']:'';
        $orderby = "n.news_create_date DESC, n.news_orderby ASC, n.id DESC";
        $this->_data['page'] = isset($_REQUEST['page']) && is_numeric($_REQUEST['page']) ? $_REQUEST['page'] : '1';
        $config['per_page'] = $menu_view == 'download' ? 12:6;
		if($menu_view=='tuyendung')
        {
            $config['per_page'] = 100;
        }
        $config['uri_segment'] = (($this->_data['page'] - 1) * $config['per_page']);  

        $object_news = 'n.id,n.news_picture,n.news_parent,n.news_create_date,n.news_file,n.news_file2,n.news_video,n.news_picture_more,n.news_link';
        $object_news .= ',nl.news_lang_name,nl.news_lang_summary,nl.news_lang_alias,nl.news_lang_summary,nl.news_lang_detail,nl.news_lang_seo_description,nl.news_lang_seo_keyword,nl.news_lang_seo_title';
        $this->_data['list'] = $this->mnews->getNews($object_news, $condition_news, $orderby, $config['uri_segment'] . ',' . $config['per_page']);
        
        $this->_data["record"] = $this->mnews->countData($condition_news);
        $config['total_rows'] = $this->_data["record"];
        $config['num_links'] = 5;
        $config['base_url'] = base_url() . $this->uri->segment(1) . '/?'.$url_search.'page=';
        $this->_data["pagination"] = $this->paging->paging_url($this->_data["record"], $this->_data['page'], $config['per_page'], $config['num_links'], $config['base_url']);
        $this->_data["title"]= !empty($this->_data['menuInfo']) ? strip_tags($this->_data['menuInfo']["menu_name"]) : tim_kiem.':  <mark>'.$this->_data['fkey'].'</mark> ';
        /*menu khach hang*/

        $arrbox = array('55','56','57','58','79','80','81','82','83','95');
        if(in_array($this->_data['menuInfo']['menu_parent'], $arrbox) || in_array($this->_data['menuInfo']['id'], $arrbox))
        {
            $this->_data['menubox5']= 1;
            $this->_data['menuSpecial'] = $this->mmenu->menuSpecial($this->_data['lang'],$this->_data['menuInfo']['id']);
        }
        $view = 'list_view';
        switch ($menu_view) {
            case 'innovation':
                $object_in = $object_news.', n.news_video ';
                $condition_in =   'news_status = 1 and nl.news_lang="'.$this->_data['lang'].'"';
                $this->_data['footer_special']= 1;
                $this->_data['innovation']['box1_gioithieu'] = $this->mnews->getNews($object_in,$condition_in.' and news_parent=108','n.news_orderby ASC,n.id desc', 1);
                $this->_data['innovation']['box2'] = $this->mnews->getNews($object_in,$condition_in.' and news_parent=58','n.news_create_date desc ,n.news_orderby ASC,n.id desc', 4);
                $this->_data['banner_innovation'] = $this->mbanner->banner('banner_innovation');
                $view = 'innovation_view';
                break;
            case 'socialcare':
                $this->_data['footer_special']= 1;
                $condition_soci =   'news_status = 1 and nl.news_lang="'.$this->_data['lang'].'" and news_parent = 106 ';
                $this->_data['listNews'] = $this->mnews->getNews($object_news, $condition_soci, 'n.news_orderby ASC, n.id DESC, n.id desc');
                $this->_data['myCate'] = $this->mmenu->getMenu(78,$this->_data['lang']);
                $condition_soci2 =   'news_status = 1 and nl.news_lang="'.$this->_data['lang'].'"';
                $this->_data['social']['box1'] = $this->mnews->getNews($object_news,$condition_soci2.' and news_parent=77','n.id desc', 1);
                $this->_data['social']['menubox2'] = $this->mmenu->getInfoID(78,$this->_data['lang']);
                $this->_data['social']['menubox3'] = $this->mmenu->getInfoID(106,$this->_data['lang']);
                $view = 'socialcare_view';
                break;
            case 'maintenance':
                $condition_main =   'news_status = 1 and nl.news_lang="'.$this->_data['lang'].'"';
                $this->_data['main']['box1'] = $this->mnews->getNews($object_news,$condition_main.' and news_parent=56','n.news_orderby ASC, n.id desc', 4);
                $this->_data['footer_special']= 1;
                $this->_data['banner_page'] = $this->mbanner->banner('banner_page',$this->_data['menuInfo']['menu_parent'],'id');
                $view = 'maintenance_view';
                break;
            case 'ecochanges':
                $this->_data['footer_special']= 1;
                $condition_eco =   'news_status = 1 and nl.news_lang="'.$this->_data['lang'].'"';
                /*thong diep moi truong*/
                $this->_data['ecochanges']['box1'] = $this->mnews->getNews($object_news,$condition_eco.' and news_parent=84','n.id desc', 1);
                /*video*/
                $object_ecovideo = $object_news.', n.news_video ';
                $this->_data['ecochanges']['video'] = $this->mnews->getNews($object_ecovideo,$condition_eco.' and news_parent=85','n.id desc', 1);
                /*hoat dong toan cau*/
                $this->_data['ecochanges']['box2'] = $this->mnews->getNews($object_news,$condition_eco.' and news_parent=86','n.news_orderby ASC, n.id desc', 4);
                $this->_data['ecochanges']['menubox2'] = $this->mmenu->getInfoID(86,$this->_data['lang']);
                /*NHỮNG CHIẾC THANG MÁY TIẾT KIỆM NĂNG LƯỢNG ĐỈNH CAO*/
                $this->_data['ecochanges']['box3'] = $this->mnews->getNews($object_news,$condition_eco.' and news_parent=87','n.id desc', 1);
                $this->_data['ecochanges']['menubox3'] = $this->mmenu->getInfoID(87,$this->_data['lang']);
                
                /* Ứng dụng điển hình giúp tiết kiệm điện năng tiêu thụ */
                $this->_data['ecochanges']['box4'] = $this->mnews->getNews($object_news,$condition_eco.' and news_parent=88','n.id desc', 1);
                $this->_data['ecochanges']['menubox4'] = $this->mmenu->getInfoID(88,$this->_data['lang']);

                /* Ứng dụng điển hình giúp tiết kiệm điện năng tiêu thụ */
                $this->_data['ecochanges']['box5'] = $this->mnews->getNews($object_news,$condition_eco.' and news_parent=89','n.news_orderby ASC, n.id desc', 2);
                $this->_data['ecochanges']['menubox5'] = $this->mmenu->getInfoID(89,$this->_data['lang']);
                $view = 'ecochanges_view';
                break;
            case 'quality':
                $object_qua = $object_news.', n.news_video ';
                $condition_qua =  'news_status = 1 and nl.news_lang="'.$this->_data['lang'].'"';
                $this->_data['quality']['box1'] = $this->mnews->getNews($object_qua,$condition_qua.' and news_parent=54','n.news_orderby ASC,n.id desc', 5);
                $this->_data['quality']['menubox1'] = $this->mmenu->getInfoID(54,$this->_data['lang']);
                $this->_data['quality']['box2'] = $this->mnews->getNews($object_qua,$condition_qua.' and news_parent=95','n.news_orderby ASC,n.id desc', 5);
                $this->_data['quality']['menubox2'] = $this->mmenu->getInfoID(95,$this->_data['lang']);
                $this->_data['footer_special']= 1;
                $view = 'quality_view';
                break;
            case 'download':
                $view = 'download_view';
                break;
            case 'tuyendung':
                $view = 'tuyendung_view';
                break;
            case 'kinhnghiem':
                $this->_data['menuChild'] = $this->mmenu->getMenu(8,$this->_data['lang']);
                $this->_data['title_cate'] = $this->mmenu->getTitle(8,$this->_data['lang']);
                $this->_data['menuCate'] = $this->mmenu->getMenu(50,$this->_data['lang']);
                $view = 'kinhnghiem_view';
                break;
            case 'product':
                $this->_data['menuChild'] = $this->mmenu->getMenu(5,$this->_data['lang']);
                $condition_news=" news_lang='".$this->_data['lang']."' and news_status = 1 and  n.news_parent=".$this->_data['menuInfo']['id'];
                $object_product = $object_news.', nl.news_lang_ungdung, nl.news_lang_tienich, n.news_tieuchuan,nl.news_lang_chucnang ';
                // echo $object_product;
                $this->_data['info'] = $this->mnews->getNews($object_product, $condition_news, 'n.id desc', '1');
                $this->_data['title'] = !empty($this->_data['info']) ? strip_tags($this->_data['info']->news_lang_name) : strip_tags($this->_data['menuInfo']['menu_name']);
                if(!empty($this->_data['info']->news_lang_seo_title)){
                    $this->_data["title"] = $this->_data['info']->news_lang_seo_title;
                }
                if(!empty($this->_data['info']->news_lang_seo_description)){
                    $this->_data["description"] = $this->_data['info']->news_lang_seo_description;
                }
                if(!empty($this->_data['info']->news_lang_seo_keyword)){
                    $this->_data["keywords"] = $this->_data['info']->news_lang_seo_keyword;
                }
                $this->_data['tieuchuan'] = !empty($this->_data['info']->news_tieuchuan) ? unserialize($this->_data['info']->news_tieuchuan) : '';
                $view = 'product_view';
                if($this->_data['menuInfo']['id'] == 37 || $this->_data['menuInfo']['id'] == 38){
                    $view = 'detail_view';
                }
                break;
            default:
                if(count($this->_data["list"])==1 && !isset($_REQUEST['page'])){
                    $this->_data['info'] = $this->_data["list"][0];
                    $this->_data["title"] = strip_tags($this->_data["info"]->news_lang_name);

                    if(!empty($this->_data['info']->news_lang_seo_description)){
                        $this->_data["description"] = $this->_data['info']->news_lang_seo_description;
                    }
                    if(!empty($this->_data['info']->news_lang_seo_keyword)){
                        $this->_data["keywords"] = $this->_data['info']->news_lang_seo_keyword;
                    }
                    $detail_view = 'detail_view';
                    /*begin khach hang*/
                    if($this->_data['menuInfo']['id'] == 4 
                        || $this->_data['menuInfo']['menu_parent'] == 4 ){
                        $this->_data['same'] = NULL;
                        $id = $this->_data['info']->id;
                        if(!empty($id))
                        {
                            $condition_news="  n.id=".$id;
                            $this->_data['info'] = $this->mnews->getNews($object_news, $condition_news, 'n.id desc', '1');
                            if(!empty($this->_data['info']))
                            {
                                $condition_news_same =  ' news_status = 1 and  nl.news_lang = "'.$this->_data['lang'].'" and n.id != '.$id.' and news_parent = '.$this->_data['info']->news_parent;
                                $this->_data['same'] = $this->mnews->getNews($object_news, $condition_news_same, 'n.news_create_date DESC,n.news_orderby ASC, n.id desc', '0,20');
                                $arr_up=array(
                                    "news_view"=>($this->_data['info']->news_view + 1),
                                );
                                $this->mnews->updateData($id,$arr_up);
                                $this->_data["title"] = strip_tags($this->_data['info']->news_lang_name);
                            }
                            $this->_data['mulImg'] = !empty($this->_data['info']) && $this->_data['info']->news_picture_more ?  unserialize($this->_data['info']->news_picture_more):'';
                        }
                        $detail_view =  'khachhang_view';
                    }
                    /*end khach hang*/

                    $this->my_layout->view("frontend/news/".$detail_view,$this->_data);
                    return;
                }
                break;
        }

        /*fix_img*/
        if($this->_data['menuInfo']['id'] == 4 || $this->_data['menuInfo']['menu_parent']==4)
        {
            $this->_data['fix_img'] = '';
        }
        /*fillter*/
        if($this->_data['menuInfo']['id'] == 7 || $this->_data['menuInfo']['menu_parent']==7)
        {
            $this->_data['fillter']= 1;
        }
        /*begin hoat dong vmec*/
        if($this->_data['menuInfo']['id'] == 22 || $this->_data['menuInfo']['menu_parent'] == 22)
        {
            $this->_data['menuHoatDong'] = $this->mmenu->getMenu(22,$this->_data['lang']);
            $this->_data['menuChild'] = $this->mmenu->getMenu(9,$this->_data['lang']);
            $this->_data['title_cate'] = $this->mmenu->getTitle(9,$this->_data['lang']);
        }
        $this->my_layout->view("frontend/news/".$view,$this->_data);
    }
    /**end trinh bay danh sach*/

    /**begin trinh bay chi tiet*/
    public function detail($menu_alias,$id)
    {
        $menuTmp = $this->mmenu->getInfoAliasTmp($menu_alias);
        $this->_data['menuInfo'] = $this->mmenu->getInfoID($menuTmp,$this->_data['lang']);
        // $this->_data['menuInfo'] = $this->mmenu->getInfoID($this->_data['menuTmp']['id'],$this->_data['lang']);
        if(empty($this->_data['menuInfo']))
        {
            redirect(base_url());
        }
        $object_news = 'n.id,n.news_parent,n.news_view,n.news_picture,n.news_comment,n.news_create_date,n.news_video,n.news_picture_more,n.news_link';
        $object_news .= ',nl.news_lang_name,nl.news_lang_alias,nl.news_lang_summary, nl.news_lang_detail,nl.news_lang_seo_title,nl.news_lang_seo_keyword,nl.news_lang_seo_description';
        $this->_data['same'] = NULL;
        if(!empty($id))
        {
            $condition_news="  nl.news_lang = '".$this->_data['lang']."' and news_status = 1 and  n.id=".$id;
            $this->_data['info'] = $this->mnews->getNews($object_news, $condition_news, 'n.id desc', '1');
            if(!empty($this->_data['info']))
            {
                $condition_news_same =  ' news_status = 1 and  nl.news_lang = "'.$this->_data['lang'].'" and n.id != '.$id.' and news_parent = '.$this->_data['info']->news_parent;
                $this->_data['same'] = $this->mnews->getNews($object_news, $condition_news_same, 'n.news_create_date DESC,n.news_orderby ASC, n.id desc', '0,20');
                $arr_up=array(
                    "news_view"=>($this->_data['info']->news_view + 1),
                );
                $this->mnews->updateData($id,$arr_up);
                $this->_data["title"] = $this->_data['info']->news_lang_seo_title ? $this->_data['info']->news_lang_seo_title:$this->_data['info']->news_lang_name;
                if(!empty($this->_data['info']->news_lang_seo_description)){
                    $this->_data["description"] = $this->_data['info']->news_lang_seo_description;
                }
                if(!empty($this->_data['info']->news_lang_seo_keyword)){
                    $this->_data["keywords"] = $this->_data['info']->news_lang_seo_keyword;
                }
            }
        }
        $this->_data['menuChild'] = $this->mmenu->getMenu($this->_data['menuInfo']['id'],$this->_data['lang']);
        $this->_data['title_cate'] = $this->mmenu->getTitle($this->_data['menuInfo']['id'],$this->_data['lang']);
        if(empty($this->_data['menuChild'])){
            $this->_data['title_cate'] = $this->mmenu->getTitle($this->_data['menuInfo']['menu_parent'],$this->_data['lang']);
            $myMenu = $this->mmenu->getData('id',array('id'=>$this->_data['menuInfo']['menu_parent']));
            $this->_data['menuChild'] = $this->mmenu->getMenu($myMenu['id'],$this->_data['lang']);
        }
        // if($this->_data['info']->news_parent==95)
        // {
        //     $this->_data['menuChild'] = $this->mmenu->getMenu(53,$this->_data['lang']);
        // }
        $view = 'detail_view';
        if($this->_data['menuInfo']['id'] == 4 || $this->_data['menuInfo']['menu_parent'] == 4){
            $this->_data['mulImg'] = !empty($this->_data['info']) && $this->_data['info']->news_picture_more ?  unserialize($this->_data['info']->news_picture_more):'';
            $view =  'khachhang_view';
        }
        /*banner page*/
        $this->_data['banner_page'] = $this->mbanner->banner('banner_page',$this->_data['menuInfo']['id']);
        if(empty($this->_data['banner_page'])){
            $this->_data['banner_page'] = $this->mbanner->banner('banner_page',$this->_data['menuInfo']['menu_parent']);
        }

        $arrbox = array('55','56','57','58','79','80','81','82','83','95');
        if(in_array($this->_data['menuInfo']['menu_parent'], $arrbox) || in_array($this->_data['menuInfo']['id'], $arrbox))
        {
            $this->_data['menubox5']= 1;
            $this->_data['menuSpecial'] = $this->mmenu->menuSpecial($this->_data['lang'],$this->_data['menuInfo']['id']);
        }
        $this->_data['hd_cate'] = $this->mmenu->getHiddenID($this->_data['menuInfo']['menu_parent'],$this->_data['lang']);
        $this->my_layout->view("frontend/news/".$view,$this->_data);
    }
    /**end trinh bay chi tiet*/

    public function map()
    {
        $this->load->view("frontend/news/map-service",$this->_data);
    }

    public function timkiem()
    {
        $this->my_layout->view("frontend/news/timkiem",$this->_data);
    }

    public function showmenu(){
        $data = $this->mmenu->menuSpecial($this->_data['lang']);
        echo $data;
    }
}