<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
$route['default_controller'] = "home";
$route['404_override'] = 'error';
$route[admin_name.'/(:any)'] = admin_name.'/$1';
$route[admin_name] = admin_name.'/index';
$route['tim-kiem'] = 'news/timkiem/';
$route['login-web-confidentials'] = admin_name.'/index/check';

if(!empty($_SERVER['PATH_INFO'])){
    
    $menu_alias = $_SERVER['PATH_INFO'];
    $menu_alias = ltrim($menu_alias,'/');
    $menu_alias = explode('/',$menu_alias);
    $menu_alias = str_replace('.html', '',$menu_alias[0]);

    require_once( BASEPATH .'database/DB'. EXT );
    $db =& DB();
    $lang = isset($_SESSION['lang']) ? $_SESSION['lang'] : 'vn';
    $tmp = $db->select('menu_id as id')->where(array('menu_lang_alias'=>$menu_alias))->get( PREFIX.'menu_lang' );
    $tmp_rs = $tmp->result();

    if(!empty($tmp_rs)){
        $query = $db->select('menu_com as com,menu_view as view')->where(array('id'=>$tmp_rs[0]->id))->get( PREFIX.'menu' );
        $result = $query->result();

        foreach( $result as $row )
        {
            $route['(:any)/(:any)-'.$row->com.'(:num).html'] = $row->com.'/detail/$1/$3';
            $route['(:any).html'] = $row->com.'/index/$1';

        }
        
    }
    else{
        $query = $db->select('com_com as com')->where(array('com_parent'=>0,'com_status'=>1))->get( PREFIX.'com' );
        $rs = $query->result();
        if(!empty($rs))
        {
            foreach ($rs as $key => $value) {
                $route[$value->com.'/(:any)'] = $value->com.'/$1';
                $route[$value->com] = $value->com.'/index';
           
            }
        }

    }
    
}
