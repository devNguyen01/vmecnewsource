<?php
class MY_Controller extends CI_Controller{
    protected $_data;
    public function __construct(){
        parent::__construct();
        $this->load->library(array("my_layout","pagination","session","paging","cart","user_agent"));
        $this->load->helper(array("url","my_helper","string"));
        $this->my_layout->setLayout("template/frontend/template");/**load file layout chinh*/

        
        $this->load->Model("mmenu");
        $this->load->Model("mbanner");
        $this->load->Model("mnews");
        $this->load->Model("mconfig");
        $this->load->Model("frontend/mmailto");
        $this->load->Model("backend/mtranslate");
        $this->load->Model("frontend/mcompany_support");
        $this->load->Model("mlanguage");
        //loadmore
        $this->load->Model("frontend/marea");
        $this->_data['support'] = $this->mcompany_support->getOjects('company_support_name,company_support_hotline,company_support_yahoo,company_support_skype,company_support_email');
        $this->_data['title'] = '';
        $this->_data['s_member'] = $this->session->userdata('s_member'); 
        /**begin submit dang ky nhan mail*/
        
        if(!isset($_SESSION['lang']))
        {
            $_SESSION['lang'] = 'vn';
        }
        $this->_data["lang"] =  isset($_SESSION['lang']) ? $_SESSION['lang']:'vn';
        $this->_data['infoLang'] = $this->mlanguage->getData('language_name,language_picture',array('language_name_short'=>$this->_data['lang']));
        $this->mconfig->defined_helper($this->_data['lang']);
        $this->server_busy(1000,company_email_1); // 1000 là số người truy cập tại 1 thời điểm
        $this->mtranslate->defined_helper($this->_data['lang']);
        $this->_data['menuMain'] = $this->mmenu->getMenu(1,$this->_data["lang"]);
        $this->_data['menuFooter'] = $this->mmenu->getMenu(10,$this->_data["lang"]);
        $this->_data['menuLeft'] = $this->mmenu->getMenu(53,$this->_data["lang"]);
        $this->_data['contactInfo'] = $this->mcompany_contact->getObject('',array('company_contact_lang'=>$this->_data['lang']));
        $this->_data['banner_left'] = $this->mbanner->banner('banner_left',1);
        $this->_data['banner_left_page'] = '';
        $this->_data['footer_special']= 0;
        $this->_data['fillter']= 0;
        $this->_data['menubox5']= 0;
        /*begin dan ky nha mail*/
        if(isset($_POST['sfMail']))
        {
            $fsemail = $this->input->post('mailto_email');
            $faddress = '';
            $ffullname = 'Khách hàng đăng ký nhận mail';
            if($fsemail)
            {
                $this->registerEmail($fsemail,$ffullname,$faddress);
            }
        }
        /**end submit dang ky nhan mail*/
        $this->_data['ipad'] = '';
        if (!$this->agent->is_mobile()){
            $this->_data['mobi'] = '';
        }elseif ($this->agent->is_mobile()){
            $this->_data['mobi'] = 'mobi';
            if($this->agent->is_mobile('ipad')){
                $this->_data['ipad'] = 'ipad';
            }
        }
        $this->_data['ie8'] = '';
        if ($this->agent->browser() == 'Internet Explorer' and $this->agent->version() <= 8){
            $this->_data['ie8'] = 'ie8';
        }
        if($this->_data['lang']=='vn')
        {
            $this->_data['theight'] = '135';
        }
        else{
            $this->_data['theight'] = '95';
        }

        $this->_data['picture'] = base_img.'logo.png';
        $this->_data['title'] = config_title;
        $this->_data['description'] = config_description;
        $this->_data['keywords'] = config_keyword;
    }


    /**begin dang ky nhat mail*/
    public function registerEmail($email,$ffullname,$faddress){
        $error  = "";
        if($email){
            $checkemail = $this->checkemail($email);
            // 0=> khong ton tai, 1=> ton tai
            if($checkemail==0){
                $phone = "";
                $title = $email;
                $content = '';
                $file="";
                $data["set_value"] = array(
                    "mailto_fullname"=>$ffullname,
                    "mailto_email"=>$email,
                    "mailto_address"=>$faddress,
                    "mailto_phone"=>$phone,
                    "mailto_title"=>$title,
                    "mailto_content"=>$content,
                    "mailto_file"=>$file,
                    "mailto_create_date"=>time(),
                    "mailto_status"=>0
                );
                if($data['set_value']){
                    $this->mmailto->insert($data["set_value"]);
                    echo '<script>';
                        echo 'alert("Cám ơn bạn đã đăng ký nhận email từ '.company.'");';
                        echo 'window.location.href="'.current_url().'"';
                    echo '</script>';
                    $error = $email = "";
                }else{
                    $error = "Vui lòng kiểm tra email";
                }
            }else{
                echo '<script>';
                echo 'alert("Email đã tồn tại !");';
                echo 'window.location.href="'.base_url().'#registeremail";';
                echo '</script>';
            }
        }
    }
    /**end dang ky nhan mail*/

    /**begin check email*/
    public function checkemail($email){
        if($email){
            $check = $this->mmailto->countQuery('  mailto_email = "'.$email.'" ');
            if($check > 0 ){
                return 1; // da ton tai
            }else{
                return 0; // khong ton tai
            }
        }else{
            return 0; // khong ton tai
        }
    }
    /***end check email*/

    public function Msendmail($name,$from,$to,$cc='',$bcc='',$title,$content,$file='')
    {
        $this->load->library('email');
        $this->load->library('parser');
        $config['useragent']        = 'CodeIgniter';
        $config['protocol']         = 'mail';
        $config['mailpath']         = '/usr/sbin/sendmail';
        $config['wordwrap']         = FALSE;
        $config['wrapchars']        = 76;
        $config['mailtype']         = 'html';
        $config['charset']          = 'utf-8';
        $config['validate']         = FALSE;
        $config['priority']         = 3;
        $config['crlf']             = "\r\n";
        $config['newline']          = "\r\n";
        $config['bcc_batch_mode']   = FALSE;
        $config['bcc_batch_size']   = 200;

        $this->email->initialize($config);
        $this->email->from($from, $name);
        $this->email->to($to); 
        if($cc){
            $this->email->cc($cc);
        }
        if($bcc){
            $this->email->bcc($bcc);
        }

        $this->email->subject($title);
        $this->email->message($content); 
        if($file){
            $this->email->attach($file); 
        }
        if($name && $from && $to && $title && $content){
            $this->email->send();
        }
    }
    public function server_busy($numer,$mail) {
        if (PHP_OS == 'Linux' AND @file_exists('/proc/loadavg') AND $filestuff = @file_get_contents('/proc/loadavg')){
            $loadavg = explode ( ' ', $filestuff );
            if (trim ( $loadavg [0] ) > $numer) {
                print '';
                print 'Lượng truy cập đang quá tải, mời bạn quay lại sau vài phút.';
                $this->Msendmail('System admin','System',$mail,'','','SERVER Quá tải','Lượng truy cập server quá tải. Quá mức '.$numer.' người truy cập');
                exit ( 0 );
            }
        }
    }
}
?>