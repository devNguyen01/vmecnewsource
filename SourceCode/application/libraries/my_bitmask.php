<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class my_bitmask
{
    public $permissions = array(
        /*
        "add"=>false, // 1
        "edit"=>false, // 2
        "delete"=>false, // 4
        "view"=>false // 8        
        */

        /**quy luat*/
        /**
        * view = 1
        * add = 2
        * edit = 4
        * delete = 8
        * sum = 15 ==>tat ca cac quyen
        * sum = 0 ==>khong co quyen nao
        */
        /**quy luat*/  

        "view"=>false, /* 1*/
        "add"=>false, /* 2*/
        "edit"=>false, /* 4*/
        "delete"=>false /* 8 */

    );

   
    public function getPermissions($bitMask =0)
    {
        $i =0;
        foreach($this->permissions as $key => $value)
        {
            $this->permissions[$key]=(($bitMask & pow(2, $i))!=0)?true:false;
                /*uncomment the next line if you would like to see what is happening.*/
                /*echo $key . " i= ".strval($i)." power=" . strval(pow(2,$i)). "bitwise & = " . strval($bitMask & pow(2,$i))."<br>";*/
            $i++;
        }
        return $this->permissions;
    }
  
    function toBitmask()
    {
        $bitmask =0;
        $i =0;
        foreach($this->permissions as $key => $value)
        {

            if($value)
            {
                $bitmask += pow(2, $i);
            }
            $i++;
        }
        return $bitmask;
    }
}