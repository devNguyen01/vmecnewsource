var max_rand=3;
var style_model_one=0;
var style_model_two=0;
var style_model_tree=0;
var style_model_hover;
var _opacity=0.5;
var _time=800;
var _wheel_time=1500;
(function($){

	function chang_time_out(_this){

		_this.find('.inner_span, .parallelogram span, .parallelogram a').each(function(index,ui){
			_random=Math.floor((Math.random() * 3)+1);
			switch(_random){
				case 1: $(ui).css({'display':'block'}).animate({opacity:_opacity},_time); break;
				case 2: $(ui).css({'display':'block'}).animate({opacity:1},_time); break;
				case 3:  $(ui).animate({opacity:0},_time).css({'display':'block'}); break;
			}
		});
		setTimeout(function() {chang_time_out(_this)}, _wheel_time);
	}

	$.fn.random_model = function(){
		chang_time_out(this);
	}

})(jQuery);