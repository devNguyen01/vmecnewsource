// google_dynamic_map.js file

// Map Initialize function
function initializegmap(x,y)
{
    // Set static latitude, longitude value
    var latlng = new google.maps.LatLng(x,y);
    // Set map options
    var myOptions = {
        zoom: 15,
        center: latlng,
        panControl: true,
        zoomControl: true,
        scaleControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
        //content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
    }

    // Create map object with options
    map = new google.maps.Map(document.getElementById("map_canvas1"), myOptions);
    // Create and set the marker

    marker = new google.maps.Marker({
        map: map,
        draggable:false,
        position: latlng,
        animation:google.maps.Animation.BOUNCE
    });

    // Register Custom "dragend" Event
    google.maps.event.addListener(marker, 'dragend', function() {

        // Get the Current position, where the pointer was dropped
        var point = marker.getPosition();
        // Center the map at given point
        map.panTo(point);
        // Update the textbox
        //document.getElementById('ctl00_head_txt_dv_02').value=point.lat() + ', ' + point.lng();
    });
}
// google_dynamic_map.js file

// Map Initialize function
function initializegmap2(x,y)
{
    // Set static latitude, longitude value
    var latlng = new google.maps.LatLng(x,y);
    // Set map options
    var myOptions = {
        zoom: 16,
        center: latlng,
        panControl: true,
        zoomControl: true,
        scaleControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
        //content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
    }

    // Create map object with options
    map = new google.maps.Map(document.getElementById("map_canvas2"), myOptions);
    // Create and set the marker

    marker = new google.maps.Marker({
        map: map,
        draggable:false,
        position: latlng,
        animation:google.maps.Animation.BOUNCE
    });

    // Register Custom "dragend" Event
    google.maps.event.addListener(marker, 'dragend', function() {

        // Get the Current position, where the pointer was dropped
        var point = marker.getPosition();
        // Center the map at given point
        map.panTo(point);
        // Update the textbox
        //document.getElementById('ctl00_head_txt_dv_02').value=point.lat() + ', ' + point.lng();
    });
}

var map;
function initializegmap3(x,y,title,infoshow){
    var myLatlng = new google.maps.LatLng(x,y);
    var myOptions = {
    zoom: 16,
    center: myLatlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas2"), myOptions); 
      // Biến text chứa nội dung sẽ được hiển thị
    var text;
    text= infoshow;
   var infowindow = new google.maps.InfoWindow(
    { content: text,
        size: new google.maps.Size(100,50),
        position: myLatlng
    });
       infowindow.open(map);    
    var marker = new google.maps.Marker({
      position: myLatlng, 
      map: map,animation:google.maps.Animation.BOUNCE,
      title: title
  });
}