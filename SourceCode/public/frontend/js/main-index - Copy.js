$(document).ready(function() {  
    var anchors= ['#recruit', '#support', '#news', '#service', '#products', '#customer', '#about', '#home'];
    var positions=new Array();
    var _max=anchors.length;
    var element_target=new Array();
    var index_current=0;
    var anchors_index=new Array();
    var time_current=0;
    var old_index=0;
    var time_out_event_wheel=1500;
    var stop=0;
    var is_post=0;
    var flag_fixed=0;
    var flag_menu_change_location=0;
    $.each(anchors,function(index,obj){
        element_target[index]=$(obj);
        anchors_index[obj]=index;
        $('[href="'+obj+'"]').attr('data-index',index);
    });
    _width=$('#width-fixed').width();
    _full_width=$('body').width();
    _padding=((_full_width-_width)/2);
    _padding2=((_full_width-_width)/2 - 97.5);
    $('#aside_menu').css({right:_padding2,bottom: 0});
    $('#aside_service').css({left:_padding2,bottom: 0});                        
    $('.main_content-body').css({center:_padding,bottom: 0});
    
    setTimeout(function() {
        if(document.location.hash)
        {
            index_current=anchors_index[document.location.hash];
            change_page();
            is_post=1;
        }
        else
        {
            _elemnt_name=$('a.page-scroll:last-child').attr('href');
            index_current=anchors_index[_elemnt_name];
            change_page();
            is_post=1;
        }
    }, 200);
   
    function change_page(){
            $.each(element_target,function(index,obj){
                if(index!=index_current)
                {
                    obj.animate({opacity:0},400);
                }
                else
                {
                    obj.animate({opacity:1},400);
                }
            });
            if(old_index==index_current){ return;}
            $('html, body').stop().animate({
                scrollTop: element_target[index_current].offset().top
            }, time_out_event_wheel, 'easeInOutExpo');
            $('#arrow-sidebar').addClass('active');
            if(old_index<index_current){
                    $('#arrow-sidebar .fa').removeClass('fa-angle-double-up');
                    $('#arrow-sidebar .fa').addClass('fa-angle-double-down');
            }
            else
            {
                    $('#arrow-sidebar .fa').removeClass('fa-angle-double-down');
                    $('#arrow-sidebar .fa').addClass('fa-angle-double-up');
            }
            flag_fixed=1;
            setTimeout(function() {
                document.location.hash=element_target[index_current].attr('id');
                $('a.page-scroll').parent().removeClass('active');
                $('a.page-scroll[data-index="'+index_current+'"]').parent().addClass('active');

                $('#arrow-sidebar').removeClass('active');
                $('#number_elevator').html($('#number-scroll li a.page-scroll[data-index="'+index_current+'"]').html());
                old_index=index_current;
            }, time_out_event_wheel);
            setTimeout(function() {
                flag_fixed=0;
            }, time_out_event_wheel+100);
    }
    $(window).bind('mousewheel DOMMouseScroll',function(event){
        flag_fixed=1;
        event.preventDefault();
        compare=time_current+time_out_event_wheel;
        old_index=index_current;
        if(compare<=$.now()){
            if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
                old_index=index_current;
                if(index_current>0){
                    index_current--;
                }
            }
            else {
                if(index_current<_max-1){
                    index_current++;
                }
            }
            change_page();
            time_current=$.now();
        }
    });
    $(document).on('click','a.page-scroll',function(event){
        event.preventDefault();
        index_current=$(this).attr('data-index');
        change_page();
    });
function chang_menu()
    {
     __offset_change=element_target[_max-2].height()+element_target[_max-2].offset().top;
     __compare=$(window).scrollTop()+$('#nav_service').height();
     if(__compare>=__offset_change){
      margin_top=$('html').height()-__compare-$('#carousel-home').height()-120;
      $('#nav_menu').css({'margin-top':margin_top});
      $('#nav_service').css({'margin-top':margin_top}); 
      $('#aside_service').addClass('small-radion');       
      $('#arrow-sidebar .fa').removeClass('fa-angle-double-down');
      $('#arrow-sidebar .fa').addClass('fa-angle-double-up');
     }
    else
     {
      $('#nav_menu').css({'margin-top':0});
      $('#nav_service').css({'margin-top':0}); 
      $('#aside_service').removeClass('small-radion'); 
     }
    
    }  

    $(window).scroll(function(event){
        chang_menu();   
        $.each(element_target,function(index,obj){
            if(stop==0)
            {   

                if(is_post==1&&flag_fixed==0)
                {   
                    obj.css({opacity:1});
                    _compare=$(window).scrollTop();
                    if(index+1<_max){
                        if(_compare>=obj.offset().top && _compare<element_target[index+1].offset().top ){
                            
                            stop=1;
                            $('#number_elevator').html($('#number-scroll li a.page-scroll[data-index="'+index+'"]').html());
                            $('a.page-scroll').parent().removeClass('active');
                            $('a.page-scroll[data-index="'+index+'"]').parent().addClass('active');
                            index_current=index;
                        }
                    }
                    else
                    {
                        if(_compare>element_target[index-1].offset().top){
                            stop=1;
                            $('#number_elevator').html($('#number-scroll li a.page-scroll[data-index="'+index+'"]').html());
                            $('a.page-scroll').parent().removeClass('active');
                            $('a.page-scroll[data-index="'+index+'"]').parent().addClass('active');
                            index_current=index;
                        }
                    }
                    if((_compare+$(window).height())>=$('html').height()-50)
                    {
                        stop=1;
                            $('#number_elevator').html($('#number-scroll li a.page-scroll[data-index="'+(_max-1)+'"]').html());
                            $('a.page-scroll').parent().removeClass('active');
                            $('a.page-scroll[data-index="'+(_max-1)+'"]').parent().addClass('active');
                        index_current=(_max-1);
                        element_target[_max-1].css({opacity:1});
                    }
                                    
                }
                
            }

        });
        stop=0;
    })
    $(window).resize(function(){
        _width=$('#width-fixed').width();
        _full_width=$('body').width();
        _padding=((_full_width-_width)/2);
        _padding2=((_full_width-_width)/2 - 97.5);
        $('#aside_menu').css({right:_padding2,bottom: 0});
        $('#aside_service').css({left:_padding2,bottom: 0});    
    });