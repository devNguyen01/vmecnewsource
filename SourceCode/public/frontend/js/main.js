$(document).ready(function(){
    $('.indicator').bind('click',function(){
        $( ".indicator" ).next('ul.ulsub').slideUp(150);
        $(this).next('ul.ulsub').slideDown(250);
    });
    $('.buttonsearch').bind('click',function(){
        var input = $(this).prev('.check').val();
        if($.trim(input)=='')
        {
            $(this).prev('.check').addClass('error');
            return false;
        }
    });
});

/* javascript main*/
/*------------------------- Scroll To top -------------------------*/
jQuery(window).scroll(function(){
    if (jQuery(this).scrollTop() > 100) {
        jQuery('#topcontrol').css({bottom:"15px"});
    } else {
        jQuery('#topcontrol').css({bottom:"-100px"});
    }
});
jQuery('#topcontrol').click(function(){
    jQuery('html, body').animate({scrollTop: '0px'}, 800);
    return false;
});
/*------------------------- Navigation, affix and scrollspy --------------------------*/
//Navigation
if (window.matchMedia('(max-width: 767px)').matches) {
    //navigation
    $('#navigation').affix({
        offset: {              
          top: $('#navigation').offset().top,
        }
    });
}


// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

$(document).ready(function(e) { 
    /*tooltip*/
    $('[data-toggle=tooltip]').tooltip();       
            
    /*dropdown-menu*/
    $('#element').click(function(e) {
        e.stopPropagation();
    });
    
    /*popover*/
    $('[data-toggle="popover"]').popover({ html : true});
    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
});

jQuery(document).ready(function($) {    
/************************
****** Search Bar *******
*************************/
$(".search i.fa").click(function() {
    if ( $(this).hasClass('fa-search') ) {
        $(".search-field").fadeIn(400).find("input").focus();
        $(this).fadeOut(105, function() {
            $(this).siblings("i.fa-times").fadeIn(200);
        });
    } else { // Pressed the times icon
        $(".search-field").fadeOut(400);
        $(this).fadeOut(105, function() {
            $(this).siblings("i.fa-search").fadeIn(200);
        });
    }
}).hover(function() {
    $(this).animate({
        color:"#6ebff3"
    }, 400);
}, function() {
    $(this).animate({
        color: "#868686"
    }, 400);
});

$(document).keyup(function(e) {
    if ( e.keyCode == 27 ) {
        $(".navbar-form i.fa-times").click();
    }
});
    
    
}); // End of script