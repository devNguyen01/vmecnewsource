/**
 * Created with JetBrains PhpStorm.
 * User: Tinh
 * Date: 11/25/13
 * Time: 5:40 PM
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function(){
    /**begin sort lich nhan vien calendar_view*/
        var cal_curURL = $("#hdcurUrl").val();
        var hd_month = $("#hdmonth").val();
        var hd_year = $("#hdyear").val();
        var hd_user = $("#hduser").val();
        $("#sfmonth").bind("change",function(){
            var _valMonth = $(this).val();
            if(_valMonth){
                window.location.href=cal_curURL+'/?u='+hd_user+'&m='+_valMonth+'&y='+hd_year;
            }else{
                window.location.href=cal_curURL+'/?u='+hd_user+'&m='+hd_month+'&y='+hd_year;
            }
        });
        $("#sfyear").bind("change",function(){
            var _valYear = $(this).val();
            if(_valYear){
                window.location.href=cal_curURL+'/?u='+hd_user+'&m='+hd_month+'&y='+_valYear;
            }else{
                window.location.href=cal_curURL+'/?u='+hd_user+'&m='+hd_month+'&y='+hd_year;
            }
        });

        $("#sfuid").bind("change",function(){
            var _valUser = $(this).val();
            if(_valUser){
                window.location.href=cal_curURL+'/?u='+_valUser+'&m='+hd_month+'&y='+hd_year;
            }else{
                window.location.href=cal_curURL+'/?u='+hd_user+'&m='+hd_month+'&y='+hd_year;
            }
        });
    /**end sort lich nhan vien calendar_view*/

    /**begin popup iframe*/
    $(".popup_iframe").bind("click",function(e){
       var href_pu = $(this).attr("href");
        e.preventDefault();
        $('.show_popup_iframe').bPopup();
        $(".comment_popup_iframe").html('<iframe src="'+href_pu+'" width="100%" height="100%" frameborder="0"></iframe>');
    });
    /**end popup iframe*/

    /**zoom hinh anh*/
    $(".zoom_img").colorbox({rel:'zoom_img'});
    /**ngay sinh*/
    /**begin xu ly nut check tong*/
    var checkbox_count=$('.check_box').length; /**tong so check box*/
    var checkbox=$('.check_item_c2').length; /**tong so check box*/
    $(document).on("click",".check-all",function(){
        var checked=$('.check_item_c2:checked').length; /** so luong check box duoc check*/
        if(checked < checkbox) /**neu ma so check dc check nho hon tong so thi chua check all*/
        {
            $('.check_item_c2').attr('checked',true);
            $('.check_item_c2').parent("span").addClass("checked");
        }else
        {
            $('.check_item_c2').attr('checked',false);
            $('.check_item_c2').parent("span").removeClass("checked");
        }
    });
    /**end xu ly nut check tong*/

    /**begin xu ly nut check cho component*/
    $(document).on("click",".check_sum",function(){
        data_id = $(this).attr("data-id");
        var length_check = $(".check_item_"+data_id).length;
        var check = $(".check_item_"+data_id+":checked").length;
        if(check < length_check){
            $(".check_item_"+data_id).attr("checked",true);
            $(".check_item_"+data_id).parent("span").addClass("checked");
        }else{
            $(".check_item_"+data_id).attr("checked",false);
            $(".check_item_"+data_id).parent("span").removeClass("checked");
        }
        var count_check_sum = $(".check_box:checked").length;
        if(count_check_sum < checkbox_count){
            $(".check-all").attr("checked",false);
            $(".check-all").parent("span").removeClass("checked");
        }else{
            $(".check-all").attr("checked",true);
            $(".check-all").parent("span").addClass("checked");
        }
    });
    /**end xu ly nut check cho component*/

    /**begin xu ly tung check*/
    $(document).on("click",".check_box",function(){
        var once_id = $(this).attr("data-id");
        var once_check = $(".check_item_"+once_id+":checked").length;
        if(once_check==4){
            $(".check_sum_"+once_id).attr("checked",true);
            $(".check_sum_"+once_id).parent("span").addClass("checked");
        }else{
            $(".check_sum_"+once_id).attr("checked",false);
            $(".check_sum_"+once_id).parent("span").removeClass("checked");
        }

        var count_check_sum = $(".check_box:checked").length;
        if(count_check_sum < checkbox_count){
            $(".check-all").attr("checked",false);
            $(".check-all").parent("span").removeClass("checked");
        }else{
            $(".check-all").attr("checked",true);
            $(".check-all").parent("span").addClass("checked");
        }
    })
    /**end xu ly tung check*/



     /**check username co ton tai chua*/
    $("#user_email").bind("blur",function(){
        val_user = $(this).val();
        if(val_user){
            var data_check_user = "useremail="+val_user;
            $(".check_user_email").html("<img src='"+configs.base_public+"backend/images/loading.gif' alt='Loading'/>");
            $.ajax({
                type:"post",
                data:data_check_user,
                url:configs.admin_ajax+'ajbackend/check_user_email/',
                success:function(data){ 
                    if(data){
                        $(".check_user_email").html(data).show(400);
                        $(".check_user_email").prev("input").addClass("error");
                        return false;
                    }else{
                        $(".check_user_email").html("").hide(400);
                        $(".check_user_email").prev("input").removeClass("error");
                    }
                }
            })
        }
    })
    /**end checj username co ton tai chua*/


    /** start script toanvu **/
    $("#customer-company-content").slideUp();
    $("#customer-company-title").click(function(){
        $("#customer-personal-content").slideUp();
        $("#customer-company-content").slideDown();
        $("#customerForm").attr("value",2);
    });
    $("#customer-personal-title").click(function(){
        $("#customer-personal-content").slideDown();
        $("#customer-company-content").slideUp();
        $("#customerForm").attr("value",1);
    });

    /** end script toanvu **/

    /**begin cap nhat trang thai khi chi tiet lam viec*/
    $(document).on("click",".update_status_cus",function(){
        $(this).fadeOut(200);
        $(this).addClass("change_status_cus");
        $(this).removeClass("update_status_cus");
        $(this).html('<select class="form-control input-sm save_change_status_cus">' +
            '<option value="0">------</option>' +
            '<option value="1">Chưa liên hệ</option>' +
            '<option value="2">Đăng ký</option>' +
            '<option value="3">Đang liên lạc</option>' +
            '<option value="4">Đang làm việc</option>' +
            '<option value="5">Không thành công</option>' +
            '<option value="6">Thành công</option> ' +
            '</select>').fadeIn(200);
    });
    $(document).on("change",".save_change_status_cus",function(){
        _val = $(this).val();
        if(_val && isNumeric(_val) && _val>0){
            hdID = $("#hdID").val();
            var data_string = "status="+_val+"&hdID="+hdID;
            $.ajax({
                type:"post",
                data:data_string,
                url:configs.admin_url+'customer/ajupdateStatusCustomer/',
                success:function(data){
                    if(data){
                        $(".change_status_cus").html(data);
                        $(".change_status_cus").addClass("update_status_cus");
                    }
                }
            })
        }
    });
    /**end cap nhat trang thai khi chi tiet lam viec*/

    /**begin load data todolist
     * load du lieu bang ajax
     * co phan trang
     * */
    $(".aj_load_todolist").load(configs.admin_url+'customer/ajLoadCalendarPerson/').fadeIn(200);
        /**begin load co phan trang*/
        $(document).on("click",".aj_load_paging li",function(){
            _href = $(this).children("a").attr("href");
            $(".aj_load_todolist").load(_href).fadeIn(200);
            return false;
        });
        /**end load co phan trang*/
    /**end load data todolist*/

    /**begin load theo tung ngay*/
    $(document).on("click",".ajchange_cal",function(){
        _href = $(this).attr("data-href");
        $(".aj_load_todolist").load(_href).fadeIn(200);

        /**begin xu ly active*/
        $(".format_table tr td").removeClass("active_cal");
        $(this).addClass("active_cal");
        /**end xu ly active*/
    });
    /**end load theo tung ngay*/

    /**begin cap nhat trang thai todolist
     * cap nhat trang thai bang ajax
     * */
     $(document).on("click",".check_val",function(){
        _val_status = $(this).val();
         _val_id = $(this).attr("data-id");
         if(_val_status && _val_id >0){
             var obj = $(this);
             var data_string = "status="+_val_status+"&id="+_val_id;
             $.ajax({
                 type:"post",
                 data:data_string,
                 url:configs.admin_url+'customer/ajUpdateStatusCalendar/',
                 success:function(data){
                     if(data==1){
                         if(_val_status==1){
                             $(".check_val_"+_val_id).parent("li").removeClass("done");
                             obj.val(0);
                         }else{
                             $(".check_val_"+_val_id).parent("li").addClass("done");
                             obj.val(1);
                         }
                     }else{
                         alert("Please check again infomation !");
                     }
                 }
             });
         }
     });
     /**end cap nhat trang thai todolist*/


    /**begin kiem tra mat khau khi dang ky khach hang
     * 1.so sanh 2 mat khau co trung nhau hay khong
     * 2.xu ly khi thay doi mat khau thi nhap lai mat khau phai bang rong
     * 3. xu ly su kien submit luu
     * */
      $(document).on("blur","#co_password_re",function(){
         if(!isCompare($("#co_password"),$("#co_password_re"))){
             alert('Mật khẩu xác nhận không chính xác');
             return false;
         }
     });
    $(document).on("keyup","#co_password",function(){
        $("#co_password_re").val("");
    });
    $(document).on("click",".sf_click",function(){
        if(!isCompare($("#co_password"),$("#co_password_re"))){
            alert('Mật khẩu xác nhận không chính xác');
            return false;
        }
    })

     /**end kiem tra mat khau*/


     /**begin kiem tra ma code*/
     $("#co_code").bind("blur",function(){
        _val_co_code = $(this).val();
        _val_id = $("#hdID").val();        
        if(_val_co_code==""){
            alert("Bạn chưa nhập mã code !");
        }else{
            var data_string = "co_code="+_val_co_code+"&id="+_val_id;
             $.ajax({
                 type:"post",
                 data:data_string,
                 url:configs.admin_url+'customer/ajCoCode/',
                 success:function(data){
                    <!--
                        /**
                        * data = 0 => co ton tai
                        * data = 1 => khong ton tai
                        */
                    -->
                    if(data==0){
                        alert("Mã code tồn tại !");
                        $("#co_code").val("");
                    }                    
                 }
             });
        }
     })
     /**end kiem tra ma code*/

     /**thong ke bieu do theo nam*/
     $("#chart_year").bind("change",function(){
        var _val_y = $(this).val();
        if(_val_y){
          window.location.href = _val_y;
        }
     })
     /**end ke bieu do theo nam*/
 });

