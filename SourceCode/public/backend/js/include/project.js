/**
 * Created with JetBrains PhpStorm.
 * User: ManhHuy
 * Date: 9/12/13
 * Time: 9:31 AM
 * To change this template use File | Settings | File Templates.
 */

/**begin upload hinh anh*/
$(function () {
  'use strict';
  var url = configs.base_url+'public/backend/library/upload/project/files.php';
  /**begin picture*/
  $('#pro_picture').fileupload({
    url: url,
    dataType: 'json',
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
          $('<li/>').html(
              '<a class="img-upload zoom_img tip-bottom cboxElement" href="'+configs.base_file +'thumbnail/' + file.name+'">' +
                  '<img src="'+configs.base_file+'thumbnail/' + file.name+'" title="'+file.name+'" class="img-circle" />' +
                  '</a> '+
                  '<i class="fa fa-trash-o trash_file_upload" title="Xóa '+file.name+'" data-id="" data-name="'+file.name+'"></i>' +
                  '<input type="hidden" name="pro_picture[]" value="'+file.name+'" />'
          ).appendTo('#files_picture');
      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress_picture .bar').css(
        'width',
        progress + '%'
        );
    }
  });
  /**end picture*/
});
/**end upload hinh anh*/

$(document).ready(function(){
   $(document).on('click', '.removePic', function() {
        var filename = $(this).attr('data-name');
        var obj = $(this);
        if(filename)
        {
            datastring = 'filename='+filename;
            $.ajax({
                data:datastring,
                type:'post',
                url:configs.base_url+'project/removeFile/',
                success:function(data){
                    obj.parent('li').remove();
                }
            });
        }
    });

    $(".zoom_img").colorbox({rel:'zoom_img'});
    /**begin xu ly xoa file*/
    $(document).on("click",".trash_file_upload",function(){
        var trash_file_upload = $(this);
        var url_file_news = "public/frontend/uploads/files/"+configs.base_component+"/";
        var name_file = $(this).attr("data-name");
        var id_name_file = $(this).attr("data-id");
        var data_file_news = "name_file="+name_file+"&url="+url_file_news+"&id="+id_name_file;
        $.ajax({
            type:'post',
            data:data_file_news,
            url:configs.admin_url+'project/delete_picture/',
            success:function(data){
                trash_file_upload.parent("li").remove();
            }
        });
    });
    /**end xu ly xoa file*/
})
