/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.editorConfig = function(config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    // Toolbar configuration generated automatically by the editor based on config.toolbarGroups.
    /**begin tich hop ckfinder*/
    config.allowedContent = true;
    config.removeDialogTabs = 'image:upload;link:upload';
    // config.removeDialogTabs = 'image:advanced;link:advanced';

    // config.filebrowserBrowseUrl = configs.base_url+'public/backend/library/editor/kcfinder/browse.php?type=files';
    // config.filebrowserImageBrowseUrl = configs.base_url+'public/backend/library/editor/kcfinder/browse.php?type=images';
    // config.filebrowserFlashBrowseUrl = configs.base_url+'public/backend/library/editor/kcfinder/browse.php?type=flash';
    // config.filebrowserUploadUrl = configs.base_url+'public/backend/library/editor/kcfinder/upload.php?type=files';
    // config.filebrowserImageUploadUrl = configs.base_url+'public/backend/library/editor/kcfinder/upload.php?type=images';
    // config.filebrowserFlashUploadUrl = configs.base_url+'public/backend/library/editor/kcfinder/upload.php?type=flash';
    /**end tich hop ckfinder**/
    //config.filebrowserBrowseUrl = configs.base_url+'public/backend/library/editor/ckfinder/ckfinder.html';
    //config.filebrowserImageBrowseUrl = configs.base_url+'public/backend/library/editor/ckfinder/ckfinder.html?type=Images';
    //config.filebrowserFlashBrowseUrl = configs.base_url+'public/backend/library/editor/ckfinder/ckfinder.html?type=Flash';
    //config.filebrowserUploadUrl = configs.base_url+'public/backend/library/editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    //config.filebrowserImageUploadUrl = configs.base_url+'public/backend/library/editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    //config.filebrowserFlashUploadUrl = configs.base_url+'public/backend/library/editor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
    //file man
    var roxyFileman = configs.base_url+'public/backend/library/editor/fileman/?integration=ckeditor';
    config.filebrowserBrowseUrl = roxyFileman;
    config.filebrowserImageBrowseUrl = roxyFileman+'&type=image';
};